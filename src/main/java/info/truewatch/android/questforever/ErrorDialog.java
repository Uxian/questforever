/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class ErrorDialog {
	public static void showCredentialsNotSet(final Context parent, final YesNoNotification nfy) {
		final String message = parent.getString(R.string.credentialsNotSet);
		final AlertDialog.Builder builder = new AlertDialog.Builder(parent).setTitle("Alert").setMessage(message).setIcon(android.R.drawable.ic_dialog_alert);
		builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				nfy.answeredYes();
			}
		});

		builder.setNegativeButton(android.R.string.no, new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				nfy.answeredNo();
			}
		});

		builder.create().show();
	}
}
