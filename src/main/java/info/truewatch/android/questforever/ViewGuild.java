/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.exception.GameNotSupportedException;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Guild;
import info.truewatch.android.questforever.core.persistence.GuildMember;
import info.truewatch.android.questforever.core.persistence.Toon;
import info.truewatch.android.questforever.core.data.GuildRosterParser;
import info.truewatch.android.questforever.core.data.ParserFactory;
import info.truewatch.android.questforever.core.data.eq2.LegacyGuildException;
import info.truewatch.android.questforever.widget.ListAdapterFactory;

import java.text.MessageFormat;
import java.util.*;

public class ViewGuild extends Activity implements ProgressNotification, FilterChangeListener {

    private static final int MENU_LOAD_GUILD = 1;
    private static final int MENU_VIEW_PAGE = 2;
    private static final int MENU_APPLY_FILTER = 3;
    private static final int MENU_CLEAR_FILTER = 4;
    private static final int MENU_VIEW_DETAILS = 5;
    private static final int MENU_CREATE_SHORTCUT = 6;
    private static final int MENU_ADD_FAVOURITE = 7;

    private static final long A_WEEK = 1000 * 3600 * 24 * 7;

    private final Handler handler = new Handler();

    private TextView lastUpdate;
    private ListView memberList;

    private Guild guild;

    private boolean isEmpty = false;

    private List<CharacterFilter> filters = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewguild);

        memberList = (ListView) findViewById(R.id.memberList);
        lastUpdate = (TextView) findViewById(R.id.lastUpdate);

        memberList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                listClicked(position);
            }
        });

        memberList.setOnCreateContextMenuListener(this);
    }

    protected void listClicked(final int position) {
        final GuildMember entity = (GuildMember) memberList.getItemAtPosition(position);
        NavigationHelper.CharacterView.view(this, guild, entity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        final Guild entity = NavigationHelper.GuildView.fromIntent(this, getIntent());
        if (entity != null) {
            Config.LOG.debug("Successfully loaded guild from database");
            guild = entity;
            final String title = MessageFormat.format(getString(R.string.guildNameFormat), guild.getName(), guild.getServer());
            setTitle(title);
        } else {
            Config.LOG.debug("Created guild from data provided");
            final String title = MessageFormat.format(getString(R.string.unknownGuildFormat), getIntent().getLongExtra(NavigationHelper.GuildView.EXTRA_GUILD_ID, -1));
            setTitle(title);
        }
        updateDate();
        populateListFromDb();

        if (isEmpty || agesSinceUpdate()) {
            final int resource = neverUpdated() ? R.string.loadGuildFirstTime : R.string.loadGuildAges;
            final String question = MessageFormat.format(getString(resource), guild.getLastUpdateDate());
            final QuestionDialog dlg = new QuestionDialog(this, question);
            dlg.setListener(new QuestionDialog.Listener() {
                @Override
                public void questionAnswered(final boolean yesSelected) {
                    if (yesSelected) {
                        loadGuild();
                    }
                }
            });
            dlg.show();
        }
    }

    private boolean neverUpdated() {
        return isEmpty || (!guild.hasBeenUpdated());
    }

    private boolean agesSinceUpdate() {
        if (guild == null) {
            return false;
        }
        return (guild.getLastUpdateDate() == null || System.currentTimeMillis() - guild.getLastUpdateDate().getTime() > A_WEEK);
    }

    private void updateDate() {
        if (guild != null) {
            if (guild.getLastUpdateDate() == null) {
                lastUpdate.setText(R.string.neverUpdated);
            } else {
                final String last = MessageFormat.format(getString(R.string.lastUpdatedFormat), guild.getLastUpdateDate());
                lastUpdate.setText(last);
            }
        } else {
            lastUpdate.setText("");
        }
    }

    private void filterMembers(final List<GuildMember> members) {
        if (filters != null && filters.size() > 0) {
            final Iterator<GuildMember> it = members.iterator();
            while (it.hasNext()) {
                final GuildMember member = it.next();
                for (final CharacterFilter f : filters) {
                    if (!f.characterAllowed(member)) {
                        it.remove();
                        break;
                    }
                }
            }
        }
    }

    private void populateListFromDb() {
        if (guild != null) {
            List<GuildMember> members = OrmPersistence.getObjects(this, GuildMember.class, "guildId", guild.getId(), "game", guild.getGame());
            if (members == null) {
                // Make sure we have at least an empty list
                members = new ArrayList<GuildMember>();
            }
            filterMembers(members);
            Collections.sort(members);
            final ListAdapter adapter = ListAdapterFactory.getMemberListAdapter(this, members);
            Config.LOG.info("Populating UI list with " + members.size());
            memberList.setAdapter(adapter);
            isEmpty = members.size() == 0;
        }
    }

    protected void loadGuild() {
        try {
            final ProgressMonitor monitor = new ProgressMonitor(this, "Loading Guild", "Connecting", true);
            OrmPersistence.deleteObjects(this, GuildMember.class, "guildId", guild.getId(), "game", guild.getGame());
            final GuildRosterParser tsk;
            tsk = ParserFactory.getRosterParser(this, guild);
            tsk.setNotifier(new ProgressBroadcaster(monitor, this));
            new Thread(tsk).start();
        } catch (GameNotSupportedException e) {
            notifyError(e, this);
        }

    }

    @Override
    public void notifyError(final Throwable problem, final Object source) {
        if (problem instanceof LegacyGuildException) {
            ErrorHelper.instance().showErrorDialog(this, handler, "Guild not found.  If this guild was created in version 3 or before, please re-search and add it again", null, null);
        } else {
            final Map<String, String> data = new HashMap<String, String>();
            if (guild != null) {
                data.put("game", GameConstants.getName(guild.getGame()));
                data.put("server", guild.getServer());
                data.put("guild", guild.getName());
                data.put("id", "" + guild.getId());
            }
            ErrorHelper.instance().showErrorDialog(this, handler, problem, null, data);
        }
    }

    @Override
    public void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText, final Object source) {
        handler.post(new Runnable() {

            @Override
            public void run() {
                if (completed) {
                    populateListFromDb();
                    guild.refreshLastUpdate();
                    OrmPersistence.store(ViewGuild.this, guild);
                    updateDate();
                }
            }
        });
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
        menu.setQwertyMode(true);
        MenuItem item;

        item = menu.add(Menu.NONE, MENU_VIEW_DETAILS, Menu.NONE, R.string.viewCharacter);
        item.setAlphabeticShortcut('c');
        item.setIcon(android.R.drawable.ic_menu_info_details);

        item = menu.add(Menu.NONE, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
        item.setAlphabeticShortcut('f');
        item.setIcon(R.drawable.menu_icon_favourite);

        item = menu.add(Menu.NONE, MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
        item.setAlphabeticShortcut('s');
        item.setIcon(R.drawable.menu_icon_shortcut);

        item = menu.add(Menu.NONE, MENU_VIEW_PAGE, Menu.NONE, R.string.viewPage);
        item.setAlphabeticShortcut('w');
        item.setIcon(android.R.drawable.ic_menu_search);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuItem item;

        item = menu.add(Menu.NONE, MENU_LOAD_GUILD, Menu.NONE, R.string.reloadGuild);
        item.setAlphabeticShortcut('r');
        item.setIcon(android.R.drawable.ic_menu_revert);

        item = menu.add(Menu.NONE, MENU_APPLY_FILTER, Menu.NONE, R.string.filter);
        item.setAlphabeticShortcut('f');
        item.setIcon(android.R.drawable.ic_menu_view);

        item = menu.add(Menu.NONE, MENU_CLEAR_FILTER, Menu.NONE, R.string.clearFilter);
        item.setAlphabeticShortcut('c');
        item.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case MENU_VIEW_PAGE:
                viewMemberPage(info.position);
                break;
            case MENU_VIEW_DETAILS:
                viewMemberDetails(info.position);
                break;
            case MENU_ADD_FAVOURITE:
                addFavourite(info.position);
                break;
            case MENU_CREATE_SHORTCUT:
                createShortcut(info.position);
                break;
        }
        return super.onContextItemSelected(item);
    }

    private void createShortcut(final int position) {
        final GuildMember member = (GuildMember) memberList.getItemAtPosition(position);
        Config.LOG.info("Position " + position + ", member=" + member);
        if (member != null) {
            final Toon entity = member.asToon(guild);
            ShortcutHelper.createCharacterShortcut(this, entity);
        }
    }

    private void addFavourite(final int position) {
        final GuildMember member = (GuildMember) memberList.getItemAtPosition(position);
        Config.LOG.info("Position " + position + ", member=" + member);
        final Toon entity = new Toon();
        entity.setId(member.getId());
        entity.setGame(guild.getGame());
        entity.setName(member.getName());
        entity.setServer(guild.getServer());
        OrmPersistence.store(this, entity);
        Toast.makeText(this, R.string.addedFavourite, Toast.LENGTH_SHORT).show();
    }


    private void viewMemberPage(final int position) {
        final GuildMember member = (GuildMember) memberList.getItemAtPosition(position);
        Config.LOG.info("Position " + position + ", member=" + member);
        NavigationHelper.viewCharacterPage(this, guild, member);
    }

    private void viewMemberDetails(final int position) {
        final GuildMember member = (GuildMember) memberList.getItemAtPosition(position);
        Config.LOG.info("Position " + position + ", member=" + member);
        NavigationHelper.CharacterView.view(this, guild, member);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case MENU_LOAD_GUILD:
                loadGuild();
                break;
            case MENU_APPLY_FILTER:
                applyFilter();
                break;
            case MENU_CLEAR_FILTER:
                clearFilter();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void applyFilter() {
        final FilterDialog dlg = new FilterDialog(this, guild);
        dlg.addFilterChangeListener(this);
        dlg.show();
    }

    private void clearFilter() {
        filters = null;
        populateListFromDb();
    }

    @Override
    public void filterChanged(final List<CharacterFilter> filters) {
        this.filters = filters;
        populateListFromDb();
    }
}
