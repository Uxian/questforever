/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.TabActivity;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.core.data.EffectItem;
import info.truewatch.android.questforever.core.data.ValueField;
import info.truewatch.android.questforever.core.data.eq2.Eq2ItemLoader;
import info.truewatch.android.questforever.core.data.eq2.StatTransformer;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class ViewItemEq2 extends TabActivity implements ProgressNotification {

    private static final int MENU_ADD_FAVOURITE = 1;
    private static final int MENU_CREATE_SHORTCUT = 2;

    private Item theItem;

    private final Handler handler = new Handler();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_item_eq2);

        theItem = NavigationHelper.ItemView.fromIntent(this, getIntent());

        String title = "";
        if (theItem.getName() != null) {
            title = MessageFormat.format(getString(R.string.itemNameFormat), theItem.getName());
        } else {
            title = MessageFormat.format(getString(R.string.unknownItemFormat), theItem.getId());
        }
        setTitle(title);

        final TabHost host = getTabHost();
        host.addTab(host.newTabSpec("tabBasic").setIndicator(getString(R.string.basic)).setContent(R.id.item_tab_basic));
        //host.addTab(host.newTabSpec("tabChars").setIndicator(getString(R.string.chars)).setContent(R.id.characterPanel));
        //host.addTab(host.newTabSpec("tabItems").setIndicator(getString(R.string.items)).setContent(R.id.itemPanel));
        host.setCurrentTab(0);
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (theItem.getId() != -1) {
            loadItemEq2();
        }
    }


    private void loadItemEq2() {
        final ProgressMonitor m = new ProgressMonitor(this, "Loading Item", "Loading", false);
        final ProgressBroadcaster broad = new ProgressBroadcaster(m, this);
        Eq2ItemLoader loader = new Eq2ItemLoader(this, theItem);
        loader.setNotifier(broad);
        new Thread(loader).start();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (theItem != null && theItem.getId() != -1) {
            switch (item.getItemId()) {
                case MENU_ADD_FAVOURITE:
                    OrmPersistence.store(this, theItem);
                    Toast.makeText(this, R.string.addedFavourite, Toast.LENGTH_SHORT).show();
                    break;
                case MENU_CREATE_SHORTCUT:
                    ShortcutHelper.createItemShortcut(this, theItem);
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        menu.setQwertyMode(true);

        MenuItem item;

        item = menu.add(Menu.NONE, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
        item.setAlphabeticShortcut('f');
        item.setIcon(R.drawable.menu_icon_favourite);

        item = menu.add(Menu.NONE, MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
        item.setAlphabeticShortcut('s');
        item.setIcon(R.drawable.menu_icon_shortcut);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void notifyError(final Throwable problem, final Object source) {
        final Map<String, String> data = new HashMap<String, String>();
        if (theItem != null) {
            data.put("game", GameConstants.getName(theItem.getGame()));
            data.put("item", theItem.getName());
            data.put("itemId", "" + theItem.getId());
        }
        ErrorHelper.instance().showErrorDialog(this, handler, problem, null, data);
    }

    @Override
    public void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText, final Object source) {
        if (currentProgress == 2) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Eq2ItemLoader loader = (Eq2ItemLoader) source;
                    showItemStats(loader);
                }
            });
        }
        if (completed) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Eq2ItemLoader loader = (Eq2ItemLoader) source;
                    showIcon(loader);
                }
            });
        }
    }

    private void showIcon(Eq2ItemLoader loader) {
        ImageView icon = (ImageView) findViewById(R.id.item_icon);
        if (loader.getIcon() != null) {
            icon.setImageBitmap(loader.getIcon());

            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

            double ratio = (double) loader.getIcon().getWidth() / (double) loader.getIcon().getHeight();

            ViewGroup.LayoutParams params = icon.getLayoutParams();
            params.width = displaymetrics.widthPixels / 8;
            params.height = (int) (params.width / ratio);
            icon.setLayoutParams(params);
            icon.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }

    private void showItemStats(Eq2ItemLoader loader) {
        TabHost host = getTabHost();
        TextView name = (TextView) findViewById(R.id.item_name);
        TextView description = (TextView) findViewById(R.id.item_description);
        name.setText(loader.getName());
        description.setText(loader.getDescription());

        StringBuilder flags = new StringBuilder();
        for (String flag : loader.getFlags()) {
            flags.append(flag).append(" ");
        }
        ((TextView) findViewById(R.id.item_flags)).setText(flags.toString());

        FrameLayout tabs = (FrameLayout) findViewById(android.R.id.tabcontent);

        if (!loader.getModifiers().isEmpty()) {
            TableLayout statTable = createTable("damage", R.string.stats, tabs);
            boolean hadEntries = false;
            AtomicBoolean evenRow = new AtomicBoolean(false);
            int section = -1;

            for (ValueField value : loader.getModifiers()) {
                if (value.getSection() != section) {
                    addTitleRow(statTable, value.getSectionName(), hadEntries);
                    hadEntries = true;
                    evenRow.set(false);
                    section = value.getSection();
                }
                addTableRow(statTable, value.getName(), value.getValue(), evenRow);
            }
        }

        if (!loader.getAdornmentSlots().isEmpty() || !loader.getSlots().isEmpty() || !loader.getUsableClasses().isEmpty()) {
            TableLayout useTable = createTable("slots", R.string.usability, tabs);

            boolean hadEntries = false;

            if (!loader.getUsableClasses().isEmpty()) {
                AtomicBoolean evenRow = new AtomicBoolean(true);
                addTitleRow(useTable, "Classes", hadEntries);
                hadEntries = true;
                for (String className : loader.getUsableClasses().keySet()) {
                    addTableRow(useTable, StatTransformer.translateStat(className),
                            loader.getUsableClasses().get(className).toString(), evenRow);
                }
            }

            if (!loader.getSlots().isEmpty()) {
                AtomicBoolean evenRow = new AtomicBoolean(true);
                addTitleRow(useTable, "Slots", hadEntries);
                hadEntries = true;
                for (String slotName : loader.getSlots()) {
                    addTableRow(useTable, StatTransformer.translateStat(slotName), "", evenRow);
                }
            }

            if (!loader.getAdornmentSlots().isEmpty()) {
                AtomicBoolean evenRow = new AtomicBoolean(true);
                addTitleRow(useTable, "Adornment Slots", hadEntries);
                hadEntries = true;
                for (String slotName : loader.getAdornmentSlots()) {
                    addTableRow(useTable, StatTransformer.translateStat(slotName), "", evenRow);
                }
            }
        }

        if (!loader.getEffects().isEmpty()) {
            TableLayout effectsTable = createTable("effects", R.string.effects, tabs);
            AtomicBoolean evenRow = new AtomicBoolean(false);

            boolean hadEntries = false;

            for (EffectItem item : loader.getEffects()) {
                if (item.getIndentationLevel() == 0) {
                    addTitleRow(effectsTable, item.getText(), hadEntries);
                    hadEntries = true;
                    evenRow.set(true);
                } else {
                    addTableRow(effectsTable, item.getText(), evenRow, item.getIndentationLevel());
                }
            }
        }

        if (!loader.getRecipes().isEmpty()) {
            TableLayout recipeTable = createTable("recipes", R.string.recipes, tabs);
            AtomicBoolean evenRow = new AtomicBoolean(true);
            for (String recipeName : loader.getRecipes()) {
                addTableRow(recipeTable, recipeName, evenRow, 0);
            }
        }

        if (!loader.getAdornmentSlots().isEmpty()) {
        }
    }

    private void addTitleRow(TableLayout table, String headingText, boolean withSeparator) {
        if (withSeparator) {
            View lineView = new View(this);
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    2.0F, getResources().getDisplayMetrics());
            lineView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, height));
            lineView.setBackgroundColor(Color.rgb(255, 255, 255));
            table.addView(lineView);
        }

        final TableRow tr = new TableRow(this);
        final ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(param);
        tr.setBackgroundColor(android.R.color.black);
        final TextView tv1 = new TextView(this);

        SpannableString content = new SpannableString(headingText);
        content.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), 0, content.length(), 0);
        tv1.setText(content);
        tv1.setSingleLine(false);
        tv1.setPadding(2, 2, 2, 2);
        final TextView tv2 = new TextView(this);
        tv2.setText("");
        tv2.setPadding(2, 2, 2, 2);
        tr.addView(tv1);
        tr.addView(tv2);
        table.addView(tr);
    }

    private TableLayout createTable(String spec, int name, FrameLayout tabs) {
        final ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);

        ScrollView scroller = new ScrollView(this);
        scroller.setFadingEdgeLength(0);
        scroller.setMeasureAllChildren(false);
        scroller.setScrollBarStyle(LinearLayout.VERTICAL);

        LinearLayout linear = new LinearLayout(this);

        TableLayout table = new TableLayout(this);
        table.setShrinkAllColumns(true);

        scroller.addView(linear, param);
        linear.addView(table, param);
        tabs.addView(scroller, param);

        scroller.setVisibility(View.INVISIBLE);
        getTabHost().addTab(getTabHost().newTabSpec(spec).setIndicator(getString(name)).setContent(new TabCreator(scroller)));
        return table;
    }

    private void addTableRow(TableLayout table, String key, AtomicBoolean evenRow, int indentation) {
        final ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final TableRow tr = new TableRow(this);
        tr.setLayoutParams(param);
        if (evenRow.getAndSet(!evenRow.get())) {
            tr.setBackgroundColor(Color.rgb(32, 32, 32));
        } else {
            tr.setBackgroundColor(android.R.color.black);
        }
        final TextView tv1 = new TextView(this);
        tv1.setText(key);
        tv1.setPadding(2 + (20 * indentation), 2, 2, 2);
        tr.addView(tv1);
        table.addView(tr, param);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        tv1.setWidth(display.getWidth());
    }

    private void addTableRow(TableLayout table, String key, String value, AtomicBoolean evenRow) {
        final TableRow tr = new TableRow(this);
        final ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(param);
        if (evenRow.getAndSet(!evenRow.get())) {
            tr.setBackgroundColor(Color.rgb(32, 32, 32));
        } else {
            tr.setBackgroundColor(android.R.color.black);
        }
        final TextView tv1 = new TextView(this);
        tv1.setText(key);
        tv1.setPadding(2, 2, 5, 2);
        final TextView tv2 = new TextView(this);
        tv2.setText(value);
        tv2.setPadding(5, 2, 2, 2);
        tr.addView(tv1);
        tr.addView(tv2);
        table.addView(tr);
    }

    private class TabCreator implements TabHost.TabContentFactory {

        private final View theView;

        public TabCreator(View theView) {
            this.theView = theView;
        }

        @Override
        public View createTabContent(String s) {
            return theView;
        }
    }
}
