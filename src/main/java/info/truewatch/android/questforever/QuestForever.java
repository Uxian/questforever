/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import info.truewatch.android.questforever.core.exception.ReportViaGoogleException;

/**
 * Created with IntelliJ IDEA.
 * User: krupagj
 * Date: 20/10/12
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class QuestForever extends Application {

    private Thread.UncaughtExceptionHandler defaultHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        defaultHandler = Thread.getDefaultUncaughtExceptionHandler();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
    }

    private void handleUncaughtException(Thread thread, Throwable e) {
        if (e instanceof ReportViaGoogleException) {
            defaultHandler.uncaughtException(thread, e.getCause());
        } else {
            Bundle b = new Bundle();
            b.putSerializable(ReportError.ERROR, e);
            Intent intent = new Intent()
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .setClass(this, ReportError.class)
                    .putExtra(ReportError.ERROR, e);
            startActivity(intent);
        }
        System.exit(1);
    }
}
