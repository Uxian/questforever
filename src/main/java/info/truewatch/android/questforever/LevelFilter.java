/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.persistence.GuildMember;

public class LevelFilter implements CharacterFilter {

	private final int minLevel, maxLevel;
	private final ClassType type;

	public LevelFilter(int minLevel, int maxLevel, ClassType type) {
		this.minLevel = minLevel;
		this.maxLevel = maxLevel;
		this.type = type;
	}

	@Override
	public boolean characterAllowed(GuildMember member) {
		int levelToCompare = 0;
		switch (type) {
		case ADVENTURE:
		default:
			levelToCompare = member.getAdventureLevel();
			break;
		case ARTISAN:
			levelToCompare = member.getArtisanLevel();
			break;
		}

		return levelToCompare >= minLevel && levelToCompare <= maxLevel;
	}

}
