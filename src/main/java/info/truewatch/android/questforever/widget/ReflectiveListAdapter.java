/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.widget;

import info.truewatch.android.questforever.core.Config;

import java.lang.reflect.Method;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReflectiveListAdapter<T> extends BaseAdapter {

	private final Method methods[];
	private final int ids[];
	private final int resource;
	private final List<T> data;
	private final LayoutInflater inflater;

	public ReflectiveListAdapter(Context ctx, List<T> items, int resource, String methods[], int ids[], Class<T> clazz) {
		inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.resource = resource;
		this.methods = new Method[methods.length];
		this.data = items;
		for (int i=0; i<methods.length; ++i) {
			try {
				this.methods[i] = clazz.getDeclaredMethod(methods[i]);
			} catch (Exception ex) {
				this.methods[i] = null;
			}
		}
		this.ids = new int[ids.length];
        System.arraycopy(ids, 0, this.ids, 0, ids.length);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		T dataItem = data.get(position);
		View row = inflater.inflate(resource, null);

		for (int i=0; i<methods.length && i<ids.length; ++i) {
			TextView res = (TextView)row.findViewById(ids[i]);
			Object dataValue;
			try {
				dataValue = methods[i].invoke(dataItem);
				res.setText("" + dataValue);
			} catch (Exception e) {
				Config.LOG.error("Failed to reflect method " + methods[i] + " on data item " + dataItem, e);
				res.setText(e.getMessage());
			}

		}

		return row;
	}


}
