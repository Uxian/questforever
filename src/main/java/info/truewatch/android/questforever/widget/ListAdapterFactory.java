/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.widget;

import info.truewatch.android.questforever.core.persistence.*;

import java.util.List;

import android.content.Context;
import android.widget.ListAdapter;

/**
 * Factory to produce standard list adapters for each type of data item.
 * @author krupagj
 */
public class ListAdapterFactory {

    private static final String GET_NAME = "getName";
    private static final String GET_LOCATION = "getLocation";
    private static final String GET_DETAILS = "getDetails";

    public static ListAdapter getToonListAdapter(Context ctx, List<Toon> data) {
		return new ReflectiveListAdapter<Toon>(ctx, data, android.R.layout.simple_list_item_2,
				new String[]{GET_NAME, GET_LOCATION}, new int[]{android.R.id.text1, android.R.id.text2}, Toon.class);
	}

	public static ListAdapter getGuildListAdapter(Context ctx, List<Guild> data) {
		return new ReflectiveListAdapter<Guild>(ctx, data, android.R.layout.simple_list_item_2,
				new String[]{GET_NAME, GET_LOCATION}, new int[]{android.R.id.text1, android.R.id.text2}, Guild.class);
	}

	public static ListAdapter getMemberListAdapter(Context ctx, List<GuildMember> data) {
		return new ReflectiveListAdapter<GuildMember>(ctx, data, android.R.layout.simple_list_item_2,
				new String[]{GET_NAME, GET_DETAILS}, new int[]{android.R.id.text1, android.R.id.text2}, GuildMember.class);
	}

	public static ListAdapter getItemListAdapter(Context ctx, List<Item> data) {
		return new ReflectiveListAdapter<Item>(ctx, data, android.R.layout.simple_list_item_2,
				new String[]{GET_NAME, GET_DETAILS}, new int[]{android.R.id.text1, android.R.id.text2}, Item.class);
	}

    public static ListAdapter getServerListAdapter(Context ctx, List<Server> data) {
        return new ReflectiveListAdapter<Server>(ctx, data, android.R.layout.simple_list_item_2,
                new String[]{GET_NAME, GET_DETAILS}, new int[]{android.R.id.text1, android.R.id.text2}, Server.class);
    }
}
