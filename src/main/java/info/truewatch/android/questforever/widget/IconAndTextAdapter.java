/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.widget;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import info.truewatch.android.questforever.R;

public class IconAndTextAdapter<T extends EntryWithImage> extends BaseAdapter {

	private List<T> items = new ArrayList<T>();
	private final LayoutInflater layoutInflater;

	public IconAndTextAdapter(Context context) {
		layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void addItem(T item) {
		items.add(item);
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		EntryWithImage iai = items.get(position);
		View row = layoutInflater.inflate(R.layout.icon_list_entry, null);
		ImageView icon = (ImageView)row.findViewById(R.id.entryImage);
		TextView text = (TextView)row.findViewById(R.id.entryText);
		icon.setImageBitmap(iai.getBitmap());
		text.setText(iai.getText());
		return row;
	}

}
