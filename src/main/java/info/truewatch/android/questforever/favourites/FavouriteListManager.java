/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.favourites;

import info.truewatch.android.questforever.R;
import info.truewatch.android.questforever.core.Config;
import android.content.Context;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public abstract class FavouriteListManager<ITEM_CLASS> implements OnItemClickListener, OnCreateContextMenuListener {

	private Context context;
	private ListView listView;
	private int menuOffset;

	private static final int MENU_REMOVE = 1;
	private static final int MENU_CREATE_SHORTCUT = 2;
	private static final int MENU_VIEW_PAGE = 3;

	/**
	 * Create a new favourites menu listener
	 * @param listView The list view to manage
	 * @param menuOffset Menu offset.  Must be a multiple of 100
	 * @param context The context owning this list
	 */
	public FavouriteListManager(ListView listView, int menuOffset, Context context) {
		this.listView = listView;
		this.context = context;
		this.menuOffset = menuOffset;
		listView.setOnItemClickListener(this);
		listView.setOnCreateContextMenuListener(this);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		menu.setQwertyMode(true);

        addRemoveMenu(menu);
        addShortcutMenuItem(menu);

		onCreateExtraContextMenu(menu, v, menuInfo);
	}

    protected void addShortcutMenuItem(ContextMenu menu) {
        MenuItem item;
        item = menu.add(Menu.NONE, menuOffset+MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
        item.setAlphabeticShortcut('s');
    }

    protected void addRemoveMenu(ContextMenu menu) {
        MenuItem item;
        item = menu.add(Menu.NONE, menuOffset+MENU_REMOVE, Menu.NONE, R.string.remove);
        item.setAlphabeticShortcut('r');
        item.setIcon(android.R.drawable.ic_menu_delete);
    }

	protected void onCreateExtraContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		// no-op, override in sub-classes
	}

	/**
	 * Must be called as a delegate from the containing Activity
	 */
	@SuppressWarnings("unchecked")
	public void onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

		ITEM_CLASS dataItem = (ITEM_CLASS)listView.getItemAtPosition(info.position);
		Config.LOG.info("Position=" + info.position + ", item=" + dataItem);

		if (dataItem != null) {
			switch (item.getItemId() - menuOffset) {
			case MENU_REMOVE:
				remove(dataItem);
				break;

			case MENU_VIEW_PAGE:
				viewPage(dataItem);
				break;

			case MENU_CREATE_SHORTCUT:
				createShortcut(dataItem);
				break;
			}
		}
	}

	/**
	 * Must be called as a delegate from the containing Activity
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ITEM_CLASS dataItem = (ITEM_CLASS)listView.getItemAtPosition(position);
		Config.LOG.info("Position " + position + ", item=" + dataItem);
		if (dataItem != null) {
			listClicked(dataItem);
		}
	}

	public abstract void populateList();

	public int getMenuOffset() {
		return menuOffset;
	}

	protected abstract void remove(ITEM_CLASS item);
	protected abstract void viewPage(ITEM_CLASS item);
	protected abstract void createShortcut(ITEM_CLASS item);
	protected abstract void listClicked(ITEM_CLASS item);

	protected Context getContext() {
		return context;
	}

	protected ListView getListView() {
		return listView;
	}
}
