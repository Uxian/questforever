/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.favourites;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ListView;
import info.truewatch.android.questforever.NavigationHelper;
import info.truewatch.android.questforever.ProgressMonitor;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.exception.GameNotSupportedException;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.info.PreferenceConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Server;
import info.truewatch.android.questforever.core.data.ParserFactory;
import info.truewatch.android.questforever.core.data.ServerSearcher;
import info.truewatch.android.questforever.widget.ListAdapterFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServerListManager extends FavouriteListManager<Server> {

    public ServerListManager(ListView listView, int menuOffset, Context context) {
        super(listView, menuOffset, context);
    }

    @Override
    protected void createShortcut(Server entity) {
    }

    @Override
    protected void listClicked(Server entity) {
        populateList(true);
    }

    @Override
    public void populateList() {
        populateList(false);
    }

    public void populateList(boolean forceGetStatus) {
        List<Server> items = OrmPersistence.getObjects(getContext(), Server.class);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        Config.LOG.debug("Auto status: " + prefs.getBoolean(PreferenceConstants.AUTO_SERVER_STATUS, false));

        boolean getStatus = forceGetStatus || prefs.getBoolean(PreferenceConstants.AUTO_SERVER_STATUS, false);

        if (getStatus) {
            ServerSearcher searcher = null;
            try {
                searcher = ParserFactory.getServerSearcher(getContext(), GameConstants.EVERQUEST2, "");
                final Object waiter = new Object();
                final AtomicBoolean finished = new AtomicBoolean(false);
                final ProgressMonitor progressMonitor = new ProgressMonitor(getContext(), "Updating server status", "Connecting...", false);
                searcher.setNotifier(new ProgressNotification() {
                    @Override
                    public void notifyProgress(boolean completed, int currentProgress, int maxProgress, String progressText, Object source) {
                        progressMonitor.notifyProgress(completed, currentProgress, maxProgress, progressText, source);
                        if (completed) {
                            synchronized (waiter) {
                                finished.set(true);
                                waiter.notify();
                            }
                        }
                    }

                    @Override
                    public void notifyError(Throwable problem, Object source) {
                        progressMonitor.notifyError(problem, source);
                        synchronized (waiter) {
                            finished.set(true);
                            waiter.notify();
                        }
                    }
                });
                new Thread(searcher).start();
                synchronized (waiter) {
                    while (!finished.get()) {
                        try {
                            waiter.wait();
                        } catch (InterruptedException e) {
                            // No-op
                        }
                    }
                }
                copyServerStatus(searcher.getResults(), items);
            } catch (GameNotSupportedException e) {
                // No-op
            }
        }

        getListView().setAdapter(ListAdapterFactory.getServerListAdapter(getContext(), items));
    }

    private void copyServerStatus(List<Server> allServers, List<Server> selectedServers) {
        Map<String, Server> serverMap = new HashMap<String, Server>();
        if (allServers != null) {
            for (Server server : allServers) {
                Config.LOG.debug("Mapping server " + server.getName());
                serverMap.put(server.getName(), server);
            }
        }

        if (selectedServers != null) {
            for (Server toServer : selectedServers) {
                if (serverMap.containsKey(toServer.getName())) {
                    Config.LOG.debug("Copying server " + toServer.getName());
                    Server fromServer = serverMap.get(toServer.getName());
                    toServer.setStatus(fromServer.getStatus());
                    toServer.setAge(fromServer.getAge());
                }
            }
        }
    }

    @Override
    protected void remove(Server entity) {
        OrmPersistence.delete(getContext(), entity);
        populateList();
    }

    @Override
    protected void viewPage(Server entity) {
        NavigationHelper.viewServerPage(getContext(), entity);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setQwertyMode(true);
        addRemoveMenu(menu);
    }
}
