/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.favourites;

import info.truewatch.android.questforever.NavigationHelper;
import info.truewatch.android.questforever.ShortcutHelper;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Toon;
import info.truewatch.android.questforever.widget.ListAdapterFactory;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.ListAdapter;
import android.widget.ListView;

public class CharacterListManager extends FavouriteListManager<Toon> {

	public CharacterListManager(ListView listView, int menuOffset, Context context) {
		super(listView, menuOffset, context);
	}

	@Override
	protected void createShortcut(Toon entity) {
		ShortcutHelper.createCharacterShortcut(getContext(), entity);
	}

	@Override
	protected void listClicked(Toon entity) {
		NavigationHelper.CharacterView.view(getContext(), entity);
	}

	@Override
	public void populateList() {
		List<Toon> toons = new ArrayList<Toon>(OrmPersistence.getObjects(getContext(), Toon.class));
		ListAdapter adapter = ListAdapterFactory.getToonListAdapter(getContext(), toons);
		getListView().setAdapter(adapter);
	}

	@Override
	protected void remove(Toon entity) {
		OrmPersistence.delete(getContext(), entity);
		populateList();
	}

	@Override
	protected void viewPage(Toon entity) {
		NavigationHelper.viewCharacterPage(getContext(), entity);
	}
}
