/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.favourites;

import android.content.Context;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import info.truewatch.android.questforever.NavigationHelper;
import info.truewatch.android.questforever.ShortcutHelper;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Guild;
import info.truewatch.android.questforever.core.persistence.GuildMember;
import info.truewatch.android.questforever.widget.ListAdapterFactory;

import java.util.List;

public class GuildListManager extends FavouriteListManager<Guild> {

	public GuildListManager(final ListView listView, final int menuOffset, final Context owner) {
		super(listView, menuOffset, owner);
	}

	@Override
	protected void createShortcut(final Guild entity) {
		ShortcutHelper.createGuildShortcut(getContext(), entity);
	}

	@Override
	public void populateList() {
		final List<Guild> guilds = OrmPersistence.getObjects(getContext(), Guild.class);
		getListView().setAdapter(ListAdapterFactory.getGuildListAdapter(getContext(), guilds));
	}

	@Override
	protected void remove(final Guild entity) {
		OrmPersistence.deleteObjects(getContext(), GuildMember.class, "guildId", entity.getId(), "game", entity.getGame());
		OrmPersistence.delete(getContext(), entity);
		populateList();
	}

	@Override
	protected void viewPage(final Guild entity) {
		NavigationHelper.viewGuildPage(getContext(), entity);
	}

	@Override
	protected void listClicked(final Guild item) {
		NavigationHelper.GuildView.view(getContext(), item);
	}

	@Override
	protected void onCreateExtraContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
	}

	/**
	 * Must be called as a delegate from the containing Activity
	 */
	@Override
	public void onContextItemSelected(final MenuItem item) {
		final int menuOffset = getMenuOffset();
		final ListView listView = getListView();
		final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

		final Guild dataItem = (Guild) listView.getItemAtPosition(info.position);
		Config.LOG.info("Position=" + info.position + ", item=" + dataItem);

//		if (dataItem != null) {
//			switch (item.getItemId() - menuOffset) {
//			}
//		}

		super.onContextItemSelected(item);
	}
}
