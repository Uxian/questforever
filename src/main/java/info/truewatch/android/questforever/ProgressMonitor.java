/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import info.truewatch.android.questforever.core.ProgressNotification;

public class ProgressMonitor implements ProgressNotification {

	private ProgressDialog progressDialog;

	Handler handler = new Handler();

	public ProgressMonitor(Context ctx, String title, String initialMessage, boolean showBar) {
		progressDialog = new ProgressDialog(ctx);
		if (showBar) {
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		} else {
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		}
		progressDialog.setCancelable(false);
		progressDialog.setMax(10000);
		progressDialog.setProgress(0);
		progressDialog.setTitle(title);
		progressDialog.setMessage(initialMessage);
		progressDialog.show();
	}

	public ProgressMonitor(Context ctx, String title, String initialMessage) {
		this(ctx,title,initialMessage,false);
	}

	@Override
	public void notifyError(Throwable problem, Object source) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				progressDialog.dismiss();
			}
		});
	}

	@Override
	public void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText, Object source) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (completed) {
                    try {
					    progressDialog.dismiss();
                    } catch (IllegalArgumentException ex) {
                        // No-op
                    }
				} else {
					if (maxProgress != progressDialog.getMax()) {
						progressDialog.setMax(maxProgress);
					}
					progressDialog.setProgress(currentProgress);
					progressDialog.setMessage(progressText == null ? "" : progressText);
				}
			}
		});
	}
}
