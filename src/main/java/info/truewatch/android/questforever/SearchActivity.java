/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.info.GameConstants;
import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Spinner;

public class SearchActivity extends Activity {

	public SearchActivity() {
		super();
	}

	public int getGame() {
		Spinner spinner = (Spinner)findViewById(R.id.game_spinner);
		String gameName = spinner.getSelectedItem().toString();
		//if ("Everquest".equals(gameName)) {
		//	return GameConstants.EVERQUEST;
		//} else if ("Everquest II".equals(gameName)) {
		//	return GameConstants.EVERQUEST2;
		//}
		return GameConstants.EVERQUEST2;
	}

	@Override
	protected void onStart() {
		super.onStart();

		Spinner spinner = (Spinner)findViewById(R.id.game_spinner);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		String game = prefs.getString("preferredGame", "Everquest II");
		if (game.equals("Everquest II")) {
			spinner.setSelection(0);
		} else {
			spinner.setSelection(0);
		}

	}

}
