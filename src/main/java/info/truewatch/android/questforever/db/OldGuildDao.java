/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.db;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Guild;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class OldGuildDao {

	private static final String TABLE_NAME = "guild";

	private static final String[] COLUMNS = new String[]{"gid", "gname", "server", "lastUpdate"};

	private static List<OldGuildDao> getAll(SQLiteDatabase db) {
		LinkedList<OldGuildDao> retval = new LinkedList<OldGuildDao>();
		Cursor curs = null;
		try {
			curs = db.query(TABLE_NAME, COLUMNS, null, null, null, null, "server ASC, gname ASC");
			while (curs.move(1)) {
				OldGuildDao dao = constructFromCursor(curs);
				retval.add(dao);
			}
		} finally {
			if (curs != null) {
				curs.close();
			}
		}
		return retval;
	}

	static void createTables(SQLiteDatabase db) {
		Config.LOG.info("Creating database table");
		db.execSQL("CREATE TABLE "+TABLE_NAME+" (gid integer primary key, gname text not null, server text not null, lastUpdate long)");
	}

	static void upgradeTables(SQLiteDatabase db, int version) {
		Config.LOG.info("Upgrading guild tables to version " + version);
		switch (version) {
		case 2:
			db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD COLUMN lastUpdate integer(8)");
			break;
		}

	}

	static void migrateTables(SQLiteDatabase db, Context context) {
		// This upgrade must be to the latest version
		List<OldGuildDao> items = getAll(db);
		LinkedList<Guild> migrated = new LinkedList<Guild>();
		for (OldGuildDao oldItem: items) {
			Guild newItem = new Guild();
			newItem.setGame(GameConstants.EVERQUEST2);
			newItem.setId(oldItem.getId());
			newItem.setLastUpdateDate(oldItem.getLastUpdate());
			newItem.setName(oldItem.getName());
			newItem.setServer(oldItem.getServer());
			migrated.add(newItem);
		}
		OrmPersistence.storeAll(context, migrated);
		db.execSQL("DELETE FROM " + TABLE_NAME);
	}

	private static OldGuildDao constructFromCursor(Cursor curs) {
		OldGuildDao dao = new OldGuildDao();
		dao.setId(curs.getInt(0));
		dao.setName(curs.getString(1));
		dao.setServer(curs.getString(2));
		if (curs.isNull(3)) {
			dao.lastUpdate = null;
		} else {
			dao.lastUpdate = new Date(curs.getLong(3));
		}
		return dao;
	}

	private int id;
	private String name;
	private String server;
	private Date lastUpdate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Date getLastUpdate() {
		return new Date(lastUpdate.getTime());
	}

	public void refreshLastUpdate() {
		this.lastUpdate = new Date();
	}

	public String toString() {
		return name + " (" + server + ")";
	}
}
