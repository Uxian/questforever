/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.db;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.GuildMember;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class OldMemberDao {
	private static final String TABLE_NAME = "member";

	private long id;
	private int guildId;
	private String name;
	private String rank;
	private int level;
	private String adventureClass;
	private String artisanClass;
	private int artisanLevel;
	private String secondaryTradeskillClass;
	private int secondaryTradeskillLevel;

	static void createTables(SQLiteDatabase db) {
		db
				.execSQL("CREATE TABLE "
						+ TABLE_NAME
						+ " (charid integer primary key, guildid integer not null, chname text not null, rank text not null, level integer not null, "
						+ "adventureClass text not null, artisanClass text not null, artisanLevel integer not null, secondaryTradeskillClass text not null, "
						+ "secondaryTradeskillLevel integer not null)");
	}

	static void migrateTables(SQLiteDatabase db, Context context) {
		// This upgrade must be to the latest version
		List<OldMemberDao> members = getAll(db);
		LinkedList<GuildMember> migrated = new LinkedList<GuildMember>();
		for (OldMemberDao member: members) {
			GuildMember newMember = new GuildMember();
			newMember.setRank(member.getRank());
			newMember.setName(member.getName());
			newMember.setId(member.getId());
			newMember.setGuildId(member.getGuildId());
			newMember.setGame(GameConstants.EVERQUEST2);
			newMember.setArtisanLevel(member.getArtisanLevel());
			newMember.setArtisanClass(member.getArtisanClass());
			newMember.setAdventureLevel(member.getLevel());
			newMember.setAdventureClass(member.getAdventureClass());
			migrated.add(newMember);
		}
		OrmPersistence.storeAll(context, migrated);
		db.delete(TABLE_NAME, null, null);
	}

	static void upgradeTables(SQLiteDatabase db, int version) {
		Config.LOG.info("Upgrading guild tables to version " + version);

	}

	private static OldMemberDao getFromCursor(Cursor curs) {
		OldMemberDao dao = new OldMemberDao();
		dao.setId(curs.getLong(0));
		dao.setGuildId(curs.getInt(1));
		dao.setName(curs.getString(2));
		dao.setRank(curs.getString(3));
		dao.setLevel(curs.getInt(4));
		dao.setAdventureClass(curs.getString(5));
		dao.setArtisanClass(curs.getString(6));
		dao.setArtisanLevel(curs.getInt(7));
		dao.setSecondaryTradeskillClass(curs.getString(8));
		dao.setSecondaryTradeskillLevel(curs.getInt(9));
		return dao;
	}

	private static List<OldMemberDao> getAll(SQLiteDatabase db) {
		LinkedList<OldMemberDao> retval = new LinkedList<OldMemberDao>();
		Cursor curs = null;
		try {
			String[] cols = new String[]{"charid","guildid", "chname", "rank", "level", "adventureClass", "artisanClass", "artisanLevel",
					"secondaryTradeskillClass", "secondaryTradeskillLevel"};
			curs = db.query(TABLE_NAME, cols, null, null, null, null, null);
			while (curs.move(1)) {
				OldMemberDao dao = getFromCursor(curs);
				retval.add(dao);
			}
		} finally {
			if (curs != null) {
				curs.close();
			}
		}
		return retval;
	}

	public String toString() {
		return name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getAdventureClass() {
		return adventureClass;
	}

	public void setAdventureClass(String adventureClass) {
		this.adventureClass = adventureClass;
	}

	public String getArtisanClass() {
		return artisanClass;
	}

	public void setArtisanClass(String artisanClass) {
		this.artisanClass = artisanClass;
	}

	public int getArtisanLevel() {
		return artisanLevel;
	}

	public void setArtisanLevel(int artisanLevel) {
		this.artisanLevel = artisanLevel;
	}

	public String getSecondaryTradeskillClass() {
		return secondaryTradeskillClass;
	}

	public void setSecondaryTradeskillClass(String secondaryTradeskillClass) {
		this.secondaryTradeskillClass = secondaryTradeskillClass;
	}

	public int getSecondaryTradeskillLevel() {
		return secondaryTradeskillLevel;
	}

	public void setSecondaryTradeskillLevel(int secondaryTradeskillLevel) {
		this.secondaryTradeskillLevel = secondaryTradeskillLevel;
	}

	public void setGuildId(int guildId) {
		this.guildId = guildId;
	}

	public int getGuildId() {
		return guildId;
	}
}
