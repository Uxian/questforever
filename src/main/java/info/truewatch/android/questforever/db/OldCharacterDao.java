/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.db;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Toon;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class OldCharacterDao {

	private static final int INTRODUCED_VERSION = 3;

	private static final String TABLE_NAME = "favouriteChar";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	private String name;
	private String server;

	static void createTables(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_NAME + " (chname text not null, servername text not null, PRIMARY KEY(chname,servername))");
	}

	static void migrateTables(SQLiteDatabase db, Context context) {
		// This upgrade must be to the latest version
		List<OldCharacterDao> items = getAll(db);
		LinkedList<Toon> migrated = new LinkedList<Toon>();
		for (OldCharacterDao oldItem: items) {
			Toon newItem = new Toon();
			newItem.setGame(GameConstants.EVERQUEST2);
			newItem.setName(oldItem.getName());
			newItem.setServer(oldItem.getServer());
			migrated.add(newItem);
		}
		OrmPersistence.storeAll(context, migrated);
		db.delete(TABLE_NAME, null, null);
	}


	static void upgradeTables(SQLiteDatabase db, int version) {
		Config.LOG.info("Upgrading character tables to version " + version);
		switch (version) {
		case INTRODUCED_VERSION:
			createTables(db);
			break;
		}
	}

	private void setFromCursor(Cursor curs) {
		setName(curs.getString(0));
		setServer(curs.getString(1));
	}

	public static List<OldCharacterDao> getAll(SQLiteDatabase db) {
		LinkedList<OldCharacterDao> retval = new LinkedList<OldCharacterDao>();
		Cursor curs = null;
		try {
			String[] cols = new String[]{"chname","servername"};
			curs = db.query(TABLE_NAME, cols, null, null, null, null, "chname ASC, servername ASC");
			while (curs.move(1)) {
				OldCharacterDao dao = new OldCharacterDao();
				dao.setFromCursor(curs);
				retval.add(dao);
			}
		} finally {
			if (curs != null) {
				curs.close();
			}
		}
		return retval;
	}

	public String toString() {
		return name + " (" + server + ")";
	}
}
