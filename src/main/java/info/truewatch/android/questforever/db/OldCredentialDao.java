/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.db;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.info.PreferenceConstants;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

public class OldCredentialDao {

	private static final int INTRODUCED_VERSION = 5;

	private static final String TABLE_NAME = "eqplayers";
	private static final String[] COLUMNS = {"id", "username", "password"};

	private static final int COL_ID = 0;
	private static final int COL_USERNAME = 1;
	private static final int COL_PASSWORD = 2;

	private int id;
	private String username;
	private String password;

	static void createTables(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_NAME + " (id integer not null, username text not null, password text not null, PRIMARY KEY(id))");
	}

	static void upgradeTables(SQLiteDatabase db, int version) {
		Config.LOG.info("Upgrading character tables to version " + version);
		switch (version) {
		case INTRODUCED_VERSION:
			createTables(db);
			break;
		}
	}

	static void migrateTables(SQLiteDatabase db, Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		// This upgrade must be to the latest version
		List<OldCredentialDao> items = getAll(db);
		for (OldCredentialDao oldItem: items) {
			String username = oldItem.getUsername();
			String password = oldItem.getPassword();
			Editor editor = prefs.edit();
			editor.putString(PreferenceConstants.STATION_LOGIN, username);
			editor.putString(PreferenceConstants.STATION_PASSWORD, password);
			editor.commit();
		}
		db.delete(TABLE_NAME, null, null);
	}

	private void setFromCursor(Cursor curs) {

		setId(curs.getInt(COL_ID));
		setUsername(curs.getString(COL_USERNAME));
		String cryptedPassword = curs.getString(COL_PASSWORD);

		Crypto crypto = new Crypto(getData());

		if (!cryptedPassword.equals("")) {
			password = crypto.decrypt(cryptedPassword);
			//Log.d(Config.TAG, cryptedPassword + " -> " + password);
		} else {
			password = "";
		}
	}

	private String getData() {
		return username;
	}

	public static List<OldCredentialDao> getAll(SQLiteDatabase db) {
		LinkedList<OldCredentialDao> retval = new LinkedList<OldCredentialDao>();
		Cursor curs = null;
		try {
			curs = db.query(TABLE_NAME, COLUMNS, null, null, null, null, "username ASC");
			while (curs.move(1)) {
				OldCredentialDao dao = new OldCredentialDao();
				dao.setFromCursor(curs);
				retval.add(dao);
			}
		} finally {
			if (curs != null) {
				curs.close();
			}
		}
		return retval;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String toString() {
		return getClass().getName() + "@" + username;
	}
}
