/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.db;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Item;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class OldItemDao {

	private static final int INTRODUCED_VERSION = 4;

	private static final String TABLE_NAME = "favouriteItem";
	private static final String[] COLUMNS = new String[]{"itemid", "itemname"};

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String name;
	private int id;

	static void createTables(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_NAME + " (itemid integer primary key, itemname text not null)");
	}

	static void upgradeTables(SQLiteDatabase db, int version) {
		Config.LOG.info("Upgrading character tables to version " + version);
		switch (version) {
		case INTRODUCED_VERSION:
			createTables(db);
			break;
		}
	}

	static void migrateTables(SQLiteDatabase db, Context context) {
		// This upgrade must be to the latest version
		List<OldItemDao> items = getAll(db);
		LinkedList<Item> migrated = new LinkedList<Item>();
		for (OldItemDao oldItem: items) {
			Item newItem = new Item();
			newItem.setGame(GameConstants.EVERQUEST2);
			newItem.setName(oldItem.getName());
			newItem.setId(oldItem.getId());
			migrated.add(newItem);
		}
		OrmPersistence.storeAll(context, migrated);
		db.delete(TABLE_NAME, null, null);
	}

	private void setFromCursor(Cursor curs) {
		setId(curs.getInt(0));
		setName(curs.getString(1));
	}


	public static List<OldItemDao> getAll(SQLiteDatabase db) {
		LinkedList<OldItemDao> retval = new LinkedList<OldItemDao>();
		Cursor curs = null;
		try {
			curs = db.query(TABLE_NAME, COLUMNS, null, null, null, null, "itemname ASC, itemid ASC");
			while (curs.move(1)) {
				OldItemDao dao = new OldItemDao();
				dao.setFromCursor(curs);
				retval.add(dao);
			}
		} finally {
			if (curs != null) {
				curs.close();
			}
		}
		return retval;
	}

	public String toString() {
		return name;
	}
}
