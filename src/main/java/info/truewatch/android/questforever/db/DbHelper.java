/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.db;

import info.truewatch.android.questforever.core.Config;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

	public static final int EXIT_VERSION = 6;

	private static final int DATABASE_VERSION = 6;
	private static final String DATABASE_NAME = "QuestForever";
	private static DbHelper theInstance = null;

	public static DbHelper instance(Context ctx) {
		if (theInstance == null) {
			theInstance = new DbHelper(ctx,DATABASE_NAME, null, DATABASE_VERSION);
		}
		return theInstance;
	}

	public static DbHelper instance() {
		return theInstance;
	}

	private DbHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	public void migrateTables(Context context) {
		Config.LOG.info("Migrating tables to new database system");
		SQLiteDatabase db = getWritableDatabase();
		try {
			db.beginTransaction();
			OldGuildDao.migrateTables(db, context);
			OldMemberDao.migrateTables(db, context);
			OldCharacterDao.migrateTables(db, context);
			OldItemDao.migrateTables(db, context);
			OldCredentialDao.migrateTables(db, context);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
			db.close();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Config.LOG.info("Creating new database");
		try {
			db.beginTransaction();
			OldGuildDao.createTables(db);
			OldMemberDao.createTables(db);
			OldCharacterDao.createTables(db);
			OldItemDao.createTables(db);
			OldCredentialDao.createTables(db);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Config.LOG.info("Upgrading database from " + oldVersion + " to " + newVersion);
		for (int n=oldVersion+1; n<=newVersion; ++n) {
			Config.LOG.info("Upgrading database version " + n);
			try {
				db.beginTransaction();
				OldGuildDao.upgradeTables(db, n);
				OldMemberDao.upgradeTables(db, n);
				OldCharacterDao.upgradeTables(db, n);
				OldItemDao.upgradeTables(db, n);
				OldCredentialDao.upgradeTables(db, n);
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}
}
