/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.info.PreferenceConstants;
import info.truewatch.android.questforever.db.DbHelper;
import info.truewatch.android.questforever.favourites.*;
import android.app.TabActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;

public class SelectGuild extends TabActivity {

	private static final int MENU_SEARCH_GUILDS = 101;
	private static final int MENU_SEARCH_CHARACTERS = 102;
	private static final int MENU_SEARCH_ITEMS = 103;
	private static final int MENU_PREFERENCES = 104;

	private static final int GUILD_MANAGER_OFFSET = 200;
	private static final int CHARACTER_MANAGER_OFFSET = 300;
	private static final int ITEM_MANAGER_OFFSET = 400;
    private static final int SERVER_MANAGER_OFFSET = 500;

	private FavouriteListManager<?> guildManager, characterManager, itemManager, serverManager;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		setTitle(R.string.favouritesTitle);

		// Make sure data is migrated from previous version
		DbHelper.instance(this).migrateTables(this);
        // Remove any usernames and passwords from the preferences since they're no longer required
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getString(PreferenceConstants.STATION_LOGIN, null) != null
                || prefs.getString(PreferenceConstants.STATION_PASSWORD, null) != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(PreferenceConstants.STATION_LOGIN);
            editor.remove(PreferenceConstants.STATION_PASSWORD);
            editor.commit();
        }

		final TabHost host = getTabHost();
		host.addTab(host.newTabSpec("tabGuilds").setIndicator(getString(R.string.guilds)).setContent(R.id.guildPanel));
		host.addTab(host.newTabSpec("tabChars").setIndicator(getString(R.string.chars)).setContent(R.id.characterPanel));
		host.addTab(host.newTabSpec("tabItems").setIndicator(getString(R.string.items)).setContent(R.id.itemPanel));
        host.addTab(host.newTabSpec("tabServers").setIndicator(getString(R.string.servers)).setContent(R.id.serverPanel));
		host.setCurrentTab(0);

		guildManager = new GuildListManager((ListView)findViewById(R.id.guildList), GUILD_MANAGER_OFFSET, this);
		characterManager = new CharacterListManager((ListView)findViewById(R.id.characterList), CHARACTER_MANAGER_OFFSET, this);
		itemManager = new ItemListManager((ListView)findViewById(R.id.itemList), ITEM_MANAGER_OFFSET, this);
        serverManager = new ServerListManager((ListView)findViewById(R.id.serverList), SERVER_MANAGER_OFFSET, this);

		final Button searchButton = (Button)findViewById(R.id.searchButton);
		final Button prefsButton = (Button)findViewById(R.id.prefsButton);

        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                switch(host.getCurrentTab()) {
                    case 0:
                    default:
                        searchForGuilds();
                        break;
                    case 1:
                        searchForCharacters();
                        break;
                    case 2:
                        searchForItems();
                        break;
                    case 3:
                        searchForServers();
                }

            }
        });

		prefsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				NavigationHelper.preferences(SelectGuild.this);
			}
		});

		populateList();
	}

	private void searchForGuilds() {
		NavigationHelper.searchGuilds(this);
	}

	private void searchForCharacters() {
		NavigationHelper.searchCharacters(this);
	}

	private void searchForItems() {
		NavigationHelper.searchItems(this);
	}

    private void searchForServers() {
        NavigationHelper.searchServers(this);
    }

	@Override
	protected void onStart() {
		super.onStart();
		populateList();
	}

	private void populateList() {
		guildManager.populateList();
		characterManager.populateList();
		itemManager.populateList();
        serverManager.populateList();
	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
        final int idBase = (item.getItemId()/100)*100;
        switch (idBase) {
            case GUILD_MANAGER_OFFSET:
                Config.LOG.debug("Forwarding context menu to guild");
                guildManager.onContextItemSelected(item);
                break;
            case CHARACTER_MANAGER_OFFSET:
                Config.LOG.debug("Forwarding context menu to char");
                characterManager.onContextItemSelected(item);
                break;
            case ITEM_MANAGER_OFFSET:
                Config.LOG.debug("Forwarding context menu to item");
                itemManager.onContextItemSelected(item);
                break;
            case SERVER_MANAGER_OFFSET:
                Config.LOG.debug("Forwarding context menu to server");
                serverManager.onContextItemSelected(item);
        }
        return super.onContextItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		MenuItem item;
		item = menu.add(Menu.NONE, MENU_SEARCH_GUILDS, Menu.NONE, R.string.searchGuilds);
		item.setAlphabeticShortcut('g');
		item.setIcon(android.R.drawable.ic_menu_search);

		item = menu.add(Menu.NONE, MENU_SEARCH_CHARACTERS, Menu.NONE, R.string.searchCharacters);
		item.setAlphabeticShortcut('c');
		item.setIcon(android.R.drawable.ic_menu_search);

		item = menu.add(Menu.NONE, MENU_SEARCH_ITEMS, Menu.NONE, R.string.searchItems);
		item.setAlphabeticShortcut('i');
		item.setIcon(android.R.drawable.ic_menu_search);

		item = menu.add(Menu.NONE, MENU_PREFERENCES, Menu.NONE, R.string.preferences);
		item.setAlphabeticShortcut('p');
		item.setIcon(android.R.drawable.ic_menu_preferences);

		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case MENU_SEARCH_GUILDS:
			searchForGuilds();
			break;
		case MENU_SEARCH_CHARACTERS:
			searchForCharacters();
			break;
		case MENU_SEARCH_ITEMS:
			searchForItems();
			break;
		case MENU_PREFERENCES:
			NavigationHelper.preferences(this);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
