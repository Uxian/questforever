/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class QuestionDialog {

	private Listener listener = null;
	private final AlertDialog.Builder dlg;

	public QuestionDialog(Context ctx, String message) {
		dlg = new AlertDialog.Builder(ctx);
		dlg.setTitle(R.string.question);
		dlg.setIcon(R.drawable.ic_dialog_question);
		dlg.setMessage(message);
		dlg.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialogButtonClicked(true);
			}
		});
		dlg.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialogButtonClicked(false);
			}
		});
	}

	public QuestionDialog(Context ctx, int message) {
		this(ctx, ctx.getString(message));
	}

	public void show() {
		dlg.create().show();
	}

	public void setListener(Listener l) {
		listener = l;
	}

	private void dialogButtonClicked(boolean positive) {
		if (listener != null) {
			listener.questionAnswered(positive);
		}
	}

	public interface Listener {
		void questionAnswered(boolean yesSelected);
	}
}
