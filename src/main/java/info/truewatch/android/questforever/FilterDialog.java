/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Guild;
import info.truewatch.android.questforever.core.persistence.GuildMember;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class FilterDialog extends Dialog {

	private Button okButton;
	private Button cancelButton;

	private EditText nameSubstring;

	private Spinner adventureClassSelector;
	private EditText minAdventure, maxAdventure;

	private Spinner artisanClassSelector;
	private EditText minArtisan, maxArtisan;

	private Spinner rankSelector;

	private List<FilterChangeListener> listeners = new LinkedList<FilterChangeListener>();

	private final Guild guild;

	public FilterDialog(Context ctx, Guild guild) {
		super(ctx);
		this.guild = guild;
	}

	public void addFilterChangeListener(FilterChangeListener listener) {
		listeners.add(listener);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Context mContext = getContext();
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.filters, (ViewGroup) findViewById(R.id.LinearLayout01));
		setContentView(layout);
		setTitle(R.string.applyFilter);
		assignControls();

		okButton.setText(android.R.string.ok);
		cancelButton.setText(android.R.string.cancel);

		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onOk();
			}
		});

		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onCancel();
			}
		});

		View.OnKeyListener enterListener = new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					onOk();
					return true;
				}
				return false;
			}
		};
		nameSubstring.setOnKeyListener(enterListener);
		minAdventure.setOnKeyListener(enterListener);
		minArtisan.setOnKeyListener(enterListener);
		maxAdventure.setOnKeyListener(enterListener);
		maxArtisan.setOnKeyListener(enterListener);

		List<GuildMember> members = OrmPersistence.getObjects(getContext(), GuildMember.class, "guildId", guild.getId(), "game", guild.getGame());

		List<String> allClasses = getAllAdventureClasses(members);
		allClasses.add(0, getContext().getString(R.string.any));
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, allClasses);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adventureClassSelector.setAdapter(adapter);

		allClasses = getAllArtisanClasses(members);
		allClasses.add(0, getContext().getString(R.string.any));
		adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, allClasses);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		artisanClassSelector.setAdapter(adapter);

		allClasses = getAllRanks(members);
		allClasses.add(0, getContext().getString(R.string.any));
		adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, allClasses);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		rankSelector.setAdapter(adapter);
	}

	private List<String> getAllRanks(List<GuildMember> members) {
		TreeSet<String> retval = new TreeSet<String>();
		for (GuildMember member: members) {
			if (member.getRank() != null && member.getRank().length() > 0) {
				retval.add(member.getRank());
			}
		}
		return new ArrayList<String>(retval);
	}

	private List<String> getAllArtisanClasses(List<GuildMember> members) {
		TreeSet<String> retval = new TreeSet<String>();
		for (GuildMember member: members) {
			if (member.getArtisanClass() != null && member.getArtisanClass().length() > 0) {
				retval.add(member.getArtisanClass());
			}
		}
		return new ArrayList<String>(retval);
	}

	private List<String> getAllAdventureClasses(List<GuildMember> members) {
		TreeSet<String> retval = new TreeSet<String>();
		for (GuildMember member: members) {
			if (member.getAdventureClass() != null && member.getAdventureClass().length() > 0) {
				retval.add(member.getAdventureClass());
			}
		}
		return new ArrayList<String>(retval);
	}

	private void assignControls() {
		okButton = (Button)findViewById(R.id.okButton);
		cancelButton = (Button)findViewById(R.id.cancelButton);

		nameSubstring = (EditText)findViewById(R.id.nameSubstring);

		adventureClassSelector = (Spinner)findViewById(R.id.adventureClassSelector);
		minAdventure = (EditText)findViewById(R.id.minAdventure);
		maxAdventure = (EditText)findViewById(R.id.maxAdventure);

		artisanClassSelector = (Spinner)findViewById(R.id.artisanClassSelector);
		minArtisan = (EditText)findViewById(R.id.minArtisan);
		maxArtisan = (EditText)findViewById(R.id.maxArtisan);

		rankSelector = (Spinner)findViewById(R.id.rankSelector);
	}

	private void onOk() {
		List<CharacterFilter> filters = new LinkedList<CharacterFilter>();
		addFilters(filters);

		for (FilterChangeListener listener: listeners) {
			listener.filterChanged(filters);
		}

		dismiss();
	}

	private void onCancel() {
		dismiss();
	}

	private void addFilters(List<CharacterFilter> filters) {
		filterName(filters);
		filterAdventure(filters);
		filterArtisan(filters);
		filterRank(filters);
	}

	private void filterName(List<CharacterFilter> filters) {
		String name = nameSubstring.getText().toString();
		if (name != null && !name.equals("")) {
			filters.add(new CharacterNameFilter(name));
		}
	}

	private void filterAdventure(List<CharacterFilter> filters) {
		applyClassFilters(filters, adventureClassSelector, minAdventure, maxAdventure, ClassType.ADVENTURE);
	}

	private void filterArtisan(List<CharacterFilter> filters) {
		applyClassFilters(filters, artisanClassSelector, minArtisan, maxArtisan, ClassType.ARTISAN);
	}

	private void filterRank(List<CharacterFilter> filters) {
		String anyString = getContext().getString(R.string.any);
		String rank = rankSelector.getSelectedItem().toString();
		if (rank != null && !rank.equals("") && !rank.equals(anyString)) {
			filters.add(new RankFilter(rank));
		}
	}

	private void applyClassFilters(List<CharacterFilter> filters, Spinner clz, EditText min, EditText max, ClassType type) {
		String anyString = getContext().getString(R.string.any);
		String clazz = clz.getSelectedItem().toString();
		int minLevel = Integer.parseInt(min.getText().toString());
		int maxLevel = Integer.parseInt(max.getText().toString());

		if (!anyString.equals(clazz)) {
			filters.add(new ClassFilter(clazz, type));
		}

		filters.add(new LevelFilter(minLevel, maxLevel, type));

	}
}
