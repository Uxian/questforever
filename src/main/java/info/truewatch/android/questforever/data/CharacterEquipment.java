/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.data;

import android.graphics.Bitmap;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.widget.EntryWithImage;

public class CharacterEquipment implements EntryWithImage {

    private Bitmap image;
    private String name;
    private long id;

    public CharacterEquipment(Bitmap image, long id, String name) {
        super();
        this.id = id;
        this.image = image;
        this.name = name;
    }

    @Override
    public Bitmap getBitmap() {
        return image;
    }

    @Override
    public String getText() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Item toItemEntity(int game) {
        Item item = new Item();
        item.setGame(game);
        item.setName(getText());
        item.setId(id);
        return item;
    }
}
