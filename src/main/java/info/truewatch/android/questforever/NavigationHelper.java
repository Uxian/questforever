/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import info.truewatch.android.questforever.core.exception.InvalidIntentDataException;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.info.PreferenceConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.*;

/**
 * Class to aid in navigating between activities within the application
 * @author krupagj
 */
public class NavigationHelper {

	private static final String EXTRA_BASE = "info.truewatch.android.questforever";

    /**
	 * Class for helping to view guilds
	 * @author krupagj
	 */
	public static class GuildView {
		public static final String EXTRA_GUILD_ID = EXTRA_BASE + ".guild.id";
		public static final String EXTRA_GUILD_GAME = EXTRA_BASE + ".guild.game";
		public static final String EXTRA_GUILD_NAME = EXTRA_BASE + ".guild.name";
		public static final String EXTRA_GUILD_SERVER = EXTRA_BASE + ".guild.server";

		// GUILDS
		public static Intent getIntent(Context ctx, Guild guild, Class<?> clazz) {
			Intent i = getIntentForClass(ctx, clazz);
			i.putExtra(EXTRA_GUILD_ID, guild.getId());
			i.putExtra(EXTRA_GUILD_GAME, guild.getGame());
			i.putExtra(EXTRA_GUILD_NAME, guild.getName());
			i.putExtra(EXTRA_GUILD_SERVER, guild.getServer());
			return i;
		}

		public static Intent getIntent(Context ctx, Guild guild) {
			return getIntent(ctx, guild, ViewGuild.class);
		}

		public static Guild fromIntent(Context ctx, Intent i) {
            checkIntentExtras(i, EXTRA_GUILD_NAME, EXTRA_GUILD_SERVER);
			long id = i.getLongExtra(EXTRA_GUILD_ID, -1);
			int game = i.getIntExtra(EXTRA_GUILD_GAME, -1);
			String name = i.getStringExtra(EXTRA_GUILD_NAME);
			String server = i.getStringExtra(EXTRA_GUILD_SERVER);
			Guild theGuild = OrmPersistence.getObject(ctx, Guild.class, "id", id, "game", game);
			if (theGuild == null) {
				theGuild = new Guild();
				theGuild.setName(name);
				theGuild.setServer(server);
				theGuild.setId(id);
				theGuild.setGame(game);
			}
			return theGuild;
		}

		public static void view(Context ctx, Guild guild) {
			ctx.startActivity(getIntent(ctx, guild));
		}
	}

	public static class CharacterView {
		public static final String EXTRA_CHAR_ID = EXTRA_BASE + ".character.id";
		public static final String EXTRA_CHAR_GAME = EXTRA_BASE + ".character.game";
		public static final String EXTRA_CHAR_NAME = EXTRA_BASE + ".character.name";
		public static final String EXTRA_CHAR_SERVER = EXTRA_BASE + ".character.server";

		public static Intent getIntent(Context ctx, Toon toon) {
			Intent i = getIntentForClass(ctx, ViewCharacter.class);
			i.putExtra(EXTRA_CHAR_ID, toon.getId());
			i.putExtra(EXTRA_CHAR_GAME, toon.getGame());
			i.putExtra(EXTRA_CHAR_NAME, toon.getName());
			i.putExtra(EXTRA_CHAR_SERVER, toon.getServer());
			return i;
		}

		public static Intent getIntent(Context ctx, Guild guild, GuildMember toon) {
			return getIntent(ctx, toon.asToon(guild));
		}

		public static Toon fromIntent(Context ctx, Intent i) {
            checkIntentExtras(i, EXTRA_CHAR_NAME, EXTRA_CHAR_SERVER);
			int game = i.getIntExtra(EXTRA_CHAR_GAME, -1);
			String name = i.getStringExtra(EXTRA_CHAR_NAME);
			String server = i.getStringExtra(EXTRA_CHAR_SERVER);
			long id = i.getLongExtra(EXTRA_CHAR_ID, -1);

			Toon retval = null;
			if (game == GameConstants.EVERQUEST2) {
				retval = OrmPersistence.getObject(ctx, Toon.class, "game", game, "name", name, "server", server);
			} else {
				retval = OrmPersistence.getObject(ctx, Toon.class, "game", game, "id", id);
			}

			if (retval == null) {
				retval = new Toon();
				retval.setName(name);
				retval.setServer(server);
				retval.setId(id);
				retval.setGame(game);
			}

			return retval;
		}

		public static void view(Context ctx, Toon toon) {
			ctx.startActivity(getIntent(ctx, toon));
		}

		public static void view(Context ctx, Guild guild, GuildMember toon) {
			ctx.startActivity(getIntent(ctx, guild, toon));
		}

	}

	public static class ItemView {
		public static final String EXTRA_ITEM_ID = EXTRA_BASE + ".item.id";
		public static final String EXTRA_ITEM_NAME = EXTRA_BASE + "item.name";
		public static final String EXTRA_ITEM_GAME = EXTRA_BASE + ".item.game";

		public static Intent getIntent(Context ctx, Item item) {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
            final Intent i;
            if (item.getGame() == GameConstants.EVERQUEST2) {
                i = getIntentForClass(ctx, ViewItemEq2.class);
            } else {
			    i = getIntentForClass(ctx, ViewItem.class);
            }
		    i.putExtra(EXTRA_ITEM_ID, item.getId());
		    i.putExtra(EXTRA_ITEM_NAME, item.getName());
		    i.putExtra(EXTRA_ITEM_GAME, item.getGame());
		    return i;
		}

		public static Item fromIntent(Context ctx, Intent i) {
            checkIntentExtras(i, EXTRA_ITEM_NAME);
			int game = i.getIntExtra(EXTRA_ITEM_GAME, -1);
			long id = i.getLongExtra(EXTRA_ITEM_ID, -1);
			String name = i.getStringExtra(EXTRA_ITEM_NAME);

			Item retval = OrmPersistence.getObject(ctx, Item.class, "game", game, "id", id);

			if (retval == null) {
				retval = new Item();
				retval.setName(name);
				retval.setId(id);
				retval.setGame(game);
			}

			return retval;
		}

		public static void view(Context ctx, Item item) {
			ctx.startActivity(getIntent(ctx, item));
		}
	}

	// LEGACY
	public static void searchGuilds(Context ctx) {
		Intent i = getIntentForClass(ctx, SearchGuild.class);
		ctx.startActivity(i);
	}

	public static void searchCharacters(Context ctx) {
		Intent i = getIntentForClass(ctx, SearchCharacter.class);
		ctx.startActivity(i);
	}

	public static void searchItems(Context ctx) {
		Intent i = getIntentForClass(ctx, SearchItem.class);
		ctx.startActivity(i);
	}


    public static void searchServers(Context ctx) {
        Intent i = getIntentForClass(ctx, SearchServer.class);
        ctx.startActivity(i);
    }

	public static void listGuilds(Context ctx) {
		Intent i = getIntentForClass(ctx, SelectGuild.class);
		ctx.startActivity(i);
	}

	public static void launchUri(Context ctx, Uri uri) {
		Intent i = new Intent(Intent.ACTION_VIEW, uri);
		ctx.startActivity(i);
	}

	public static void launchUri(Context ctx, String uriString) {
		launchUri(ctx, Uri.parse(uriString));
	}

	public static void viewCharacterPage(Context ctx, Toon toon) {
		if (toon.getGame() == GameConstants.EVERQUEST2) {
			Uri uri = Uri.parse("http://eq2players.station.sony.com/" + toon.getServer() +"/" + toon.getName() + "/");
			launchUri(ctx, uri);
		} else {
			Uri uri = Uri.parse("http://eqplayers.station.sony.com/character_profile.vm?characterId=" + toon.getId());
			launchUri(ctx, uri);
		}
	}

	public static void viewCharacterPage(Context ctx, Guild guild, GuildMember toon) {
		if (toon.getGame() == GameConstants.EVERQUEST2) {
			Uri uri = Uri.parse("http://eq2players.station.sony.com/" + guild.getServer() +"/" + toon.getName() + "/");
			launchUri(ctx, uri);
		} else {
			Uri uri = Uri.parse("http://eqplayers.station.sony.com/character_profile.vm?characterId=" + toon.getId());
			launchUri(ctx, uri);
		}
	}

	public static void viewItemPage(Context ctx, Item entity) {
		String uri = null;
		switch (entity.getGame()) {
		case GameConstants.EVERQUEST2:
			uri = "http://eq2players.station.sony.com/Item/" + entity.getId();
			break;
		}
		launchUri(ctx, uri);
	}

	public static void viewGuildPage(Context ctx, Guild guild) {
		String uri = null;
		switch (guild.getGame()) {
		case GameConstants.EVERQUEST2:
			uri = "http://eq2players.station.sony.com/Guild/" + guild.getId() + "/";
			break;
		}

		launchUri(ctx, uri);
	}

    public static void viewServerPage(Context ctx, Server entity) {
        String uri = null;
        switch (entity.getGame()) {
            case GameConstants.EVERQUEST2:
                uri = "http://www.everquest2.com/";
                break;
        }

        launchUri(ctx, uri);
    }

    public static void preferences(Context ctx) {
		ctx.startActivity(getIntentForClass(ctx, PreferencesScreen.class));
	}

	private static Intent getIntentForClass(Context ctx, Class<?> clazz) {
		return new Intent(ctx, clazz);
	}

    private static void checkIntentExtras(Intent intent, String... extras) {
        if (extras == null || intent == null) {
            throw new InvalidIntentDataException();
        }

        for (String extra: extras) {
            if (!intent.hasExtra(extra)) {
                throw new InvalidIntentDataException();
            }
        }
    }
}
