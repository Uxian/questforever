/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PresenceAdapter extends BaseAdapter {

	private final List<String> characters = new ArrayList<String>();

	private final LayoutInflater inflater;

	public PresenceAdapter(Context context) {
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public synchronized void characterAvailable(String characterName) {
		if (!characters.contains(characterName)) {
			characters.add(characterName);
			Collections.sort(characters);
			notifyDataSetChanged();
		}
	}

	public synchronized void characterUnavailable(String characterName) {
		characters.remove(characterName);
		notifyDataSetChanged();
	}

	public synchronized void clear() {
		characters.clear();
		notifyDataSetChanged();
	}

	@Override
	public synchronized int getCount() {
		return characters.size();
	}

	@Override
	public synchronized Object getItem(int location) {
		return characters.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public synchronized View getView(int position, View convertView, ViewGroup parent) {
		String characterName = characters.get(position);
		View row = inflater.inflate(android.R.layout.simple_list_item_1, null);
		TextView text1 = (TextView)row.findViewById(android.R.id.text1);
		text1.setText(characterName);
		return row;
	}
}
