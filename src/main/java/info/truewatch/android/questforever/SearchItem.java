/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.exception.GameNotSupportedException;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.widget.ListAdapterFactory;
import info.truewatch.android.questforever.core.data.ItemSearcher;
import info.truewatch.android.questforever.core.data.ParserFactory;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class SearchItem extends SearchActivity implements ProgressNotification {

	private static final int MENU_ADD_FAVOURITE = 1;
	private static final int MENU_CREATE_SHORTCUT = 2;

	EditText itemName;
	ListView itemList;
	Button searchButton, addButton;

	Handler handler = new Handler();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		setTitle(R.string.searchItemsTitle);

		searchButton = (Button) findViewById(R.id.SearchButton);
		itemName = (EditText)findViewById(R.id.SearchName);
		itemList = (ListView)findViewById(R.id.ResultList);

		searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				onClickSearch();
			}
		});

		itemList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(final AdapterView<?> parent, final View view, final int position,	final long id) {
				onClickList(position);
			}

		});

		itemName.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					// Perform action on key press
					onClickSearch();
					return true;
				}
				return false;
			}
		});

		itemList.setOnCreateContextMenuListener(this);
	}

	private void onClickList(final int position) {
		final Item entity = (Item)itemList.getItemAtPosition(position);
		NavigationHelper.ItemView.view(this, entity);
	}

	private void onClickSearch() {
        final ItemSearcher searcher;
        try {
            searcher = ParserFactory.getItemSearcher(this, getGame(), itemName.getText().toString());
            final ProgressMonitor mon = new ProgressMonitor(this, "Search for Items", "Connecting");
            searcher.setNotifier(new ProgressBroadcaster(mon, this));
            new Thread(searcher).start();
        } catch (GameNotSupportedException e) {
            notifyError(e, null);
        }
    }

	@Override
	public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
		menu.setQwertyMode(true);

		MenuItem item;

		item = menu.add(Menu.NONE, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
		item.setAlphabeticShortcut('f');
		item.setIcon(R.drawable.menu_icon_favourite);

		item = menu.add(Menu.NONE, MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
		item.setAlphabeticShortcut('s');
		item.setIcon(R.drawable.menu_icon_shortcut);

	}

	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

		switch (item.getItemId()) {
		case MENU_ADD_FAVOURITE:
			addFavourite(info.position);
			break;
		case MENU_CREATE_SHORTCUT:
			createShortcut(info.position);
			break;
		}

		return super.onContextItemSelected(item);
	}

	private void addFavourite(final int position) {
		final Item entity = (Item)itemList.getItemAtPosition(position);
		Config.LOG.debug("Position " + position + ", item=" + entity);
		OrmPersistence.store(this, entity);
		Toast.makeText(this, R.string.addedFavourite, Toast.LENGTH_SHORT).show();
	}

	private void createShortcut(final int position) {
		final Item entity = (Item)itemList.getItemAtPosition(position);
		Config.LOG.debug("Position " + position + ", item=" + entity);
		if (entity != null) {
			ShortcutHelper.createItemShortcut(this, entity);
		}
	}

	@Override
	public void notifyError(final Throwable problem, final Object source) {
		final Map<String,String> data = new HashMap<String, String>();
		data.put("query", itemName.getText().toString());
		ErrorHelper.instance().showErrorDialog(this, handler, problem, null, data);
	}

	@Override
	public void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText, final Object source) {
		if (completed) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					final ItemSearcher srch = (ItemSearcher)source;
					itemList.setAdapter(ListAdapterFactory.getItemListAdapter(SearchItem.this, srch.getResults()));
				}
			});
		}
	}
}
