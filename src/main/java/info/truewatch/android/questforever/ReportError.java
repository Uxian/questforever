/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.Activity;
import android.os.Handler;
import info.truewatch.android.questforever.core.Config;

public class ReportError extends Activity {

	public static String ERROR = "ERROR";

	Throwable error;

	Handler handler = new Handler();

	@Override
	protected void onStart() {
		super.onStart();
		error = (Throwable) getIntent().getSerializableExtra(ERROR);
		notifyError(error, this);
	}

	public void notifyError(final Throwable problem, final Object source) {
		Config.LOG.error("In global error dialog thingy", problem);
		ErrorHelper.instance().showErrorDialog(this, handler, problem, null, null);
	}
}
