/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.persistence.GuildMember;

/**
 * Filter that allows selection of characters by class
 * @author krupagj
 */
public class ClassFilter implements CharacterFilter {

	private final String className;
	private final ClassType type;

	/**
	 * Construct a filter for the specified class
	 * @param className name of the class
	 * @param type type of class to filter when a character is allowed multiple classes (adventure, artisan, secondary...)
	 */
	public ClassFilter(String className, ClassType type) {
		this.className = className;
		this.type = type;
	}

	@Override
	public boolean characterAllowed(GuildMember member) {
		switch (type) {
		case ADVENTURE:
		default:
			return className.equals(member.getAdventureClass());
		case ARTISAN:
			return className.equals(member.getArtisanClass());
		}
	}

}
