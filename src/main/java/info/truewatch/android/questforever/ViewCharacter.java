/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.TabActivity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ViewGroup.LayoutParams;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.exception.GameNotSupportedException;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.core.persistence.Toon;
import info.truewatch.android.questforever.core.data.CharacterLoader;
import info.truewatch.android.questforever.core.data.ParserFactory;
import info.truewatch.android.questforever.core.data.ValueField;
import info.truewatch.android.questforever.data.CharacterEquipment;
import info.truewatch.android.questforever.widget.IconAndTextAdapter;
import info.truewatch.android.questforever.widget.ListAdapterFactory;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewCharacter extends TabActivity implements ProgressNotification {

    private static final int MENU_ADD_FAVOURITE = 1;
    private static final int MENU_CREATE_SHORTCUT = 2;

    private static final int MENU_GROUP_CHARACTER = 1;
    private static final int MENU_GROUP_ITEM = 2;

    private final Handler handler = new Handler();

    private Toon theCharacter;
    private ListView equipmentList;
    private ListView characterList;

    String title = "";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.chardetails);

        final TabHost host = getTabHost();
        host.addTab(host.newTabSpec("tabAttributes").setIndicator(getString(R.string.basic)).setContent(R.id.attributeScroller));
        host.addTab(host.newTabSpec("tabStats").setIndicator(getString(R.string.stats)).setContent(R.id.statsScroller));
        host.addTab(host.newTabSpec("tabDoll").setIndicator(getString(R.string.paperdoll)).setContent(R.id.paperDoll));
        host.addTab(host.newTabSpec("tabAlts").setIndicator(getString(R.string.alts)).setContent(R.id.altList));
        host.addTab(host.newTabSpec("tabEquip").setIndicator(getString(R.string.equipment)).setContent(R.id.equipmentList));
        host.setCurrentTab(0);

        characterList = (ListView) findViewById(R.id.altList);
        characterList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                characterClicked(position);
            }
        });
        characterList.setOnCreateContextMenuListener(new AltMenu());

        equipmentList = (ListView) findViewById(R.id.equipmentList);
        equipmentList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                equipmentClicked(position);
            }
        });
        equipmentList.setOnCreateContextMenuListener(new EquipmentMenu());

        theCharacter = NavigationHelper.CharacterView.fromIntent(this, getIntent());

        if (theCharacter != null) {
            title = MessageFormat.format(getString(R.string.characterNameFormat), theCharacter.getName());
            setTitle(title);
        } else {
            title = MessageFormat.format(getString(R.string.unknownCharFormat), "" + getIntent().getStringExtra(NavigationHelper.CharacterView.EXTRA_CHAR_NAME));
            setTitle(title);
        }

        loadCharacter();
    }

    private void loadCharacter() {
        if (theCharacter != null) {
            try {
                final CharacterLoader task;
                task = ParserFactory.getCharacterLoader(this, theCharacter);

                task.setNotifier(this);
                setProgressBarIndeterminateVisibility(true);
                new Thread(task).start();
            } catch (GameNotSupportedException e) {
                notifyError(e, theCharacter);
            }

        }
    }

    @Override
    public void notifyError(final Throwable problem, final Object source) {
        final Map<String, String> data = new HashMap<String, String>();
        if (theCharacter != null) {
            data.put("game", GameConstants.getName(theCharacter.getGame()));
            data.put("server", theCharacter.getServer());
            data.put("character", theCharacter.getName());
            data.put("characterId", "" + theCharacter.getId());
        }
        ErrorHelper.instance().showErrorDialog(this, handler, problem, null, data);
    }

    @Override
    public void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText, final Object source) {
        handler.post(new Runnable() {

            @Override
            public void run() {
                if (source instanceof CharacterLoader) {
                    showData((CharacterLoader) source, currentProgress);
                }

                if (completed) {
                    setTitle(title);
                    setProgressBarIndeterminateVisibility(false);
                } else {
                    setTitle(title + " - " + progressText);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuItem item = null;

        item = menu.add(Menu.NONE, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
        item.setAlphabeticShortcut('f');
        item.setIcon(R.drawable.menu_icon_favourite);

        item = menu.add(Menu.NONE, MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
        item.setAlphabeticShortcut('s');
        item.setIcon(R.drawable.menu_icon_shortcut);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ADD_FAVOURITE:
                OrmPersistence.store(this, theCharacter);
                Toast.makeText(this, R.string.addedFavourite, Toast.LENGTH_SHORT).show();
                break;
            case MENU_CREATE_SHORTCUT:
                ShortcutHelper.createCharacterShortcut(this, theCharacter);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void equipmentClicked(final int position) {
        final CharacterEquipment entity = (CharacterEquipment) equipmentList.getItemAtPosition(position);
        if (entity != null) {
            NavigationHelper.ItemView.view(this, entity.toItemEntity(theCharacter.getGame()));
        }
    }

    private void showData(final CharacterLoader loader, final int progressMilestone) {
        final TableLayout tableAtt = (TableLayout) findViewById(R.id.attributeTable);

        switch (progressMilestone) {
            case CharacterLoader.ATTRIBUTES_AVAILABLE: {
                final Collection<ValueField> attributes = loader.getAttributes();
                int lastSection = -1;
                boolean greyRow = true;
                for (ValueField valueField : attributes) {
                    if (valueField.getSection() > lastSection) {
                        lastSection = addHeadingRow(tableAtt, valueField, lastSection > -1);
                        greyRow = true;
                    }
                    addDataRow(tableAtt, valueField, greyRow);
                    greyRow = !greyRow;
                }
            }
            break;

            case CharacterLoader.STATS_AVAILABLE: {
                final TableLayout tableStat = (TableLayout) findViewById(R.id.statsTable);
                final Collection<ValueField> stats = loader.getStats();
                int lastSection = -1;
                boolean greyRow = true;
                for (final ValueField valueField : stats) {
                    if (valueField.getSection() > lastSection) {
                        lastSection = addHeadingRow(tableStat, valueField, lastSection > -1);
                        greyRow = true;
                    }
                    Config.LOG.info("Showing: " + valueField.getName());
                    addDataRow(tableStat, valueField, greyRow);
                    greyRow = !greyRow;
                }
            }
            break;

            case CharacterLoader.BIO_AVAILABLE: {
                final TextView bio = (TextView) findViewById(R.id.bioText);
                bio.setText(loader.getBiography());
            }
            break;

            case CharacterLoader.PAPERDOLL_AVAILABLE: {
                final ImageView doll = (ImageView) findViewById(R.id.paperDoll);
                final ImageView portrait = (ImageView) findViewById(R.id.portrait_image);

                if (loader.getPaperDoll() != null) {
                    doll.setImageBitmap(loader.getPaperDoll());
                } else {
                    Config.LOG.warn("Bitmap was null");
                }

                if (loader.getPortrait() != null) {
                    portrait.setImageBitmap(loader.getPortrait());

                    DisplayMetrics displaymetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                    double ratio = (double) loader.getPortrait().getWidth() / (double) loader.getPortrait().getHeight();

                    LayoutParams params = portrait.getLayoutParams();
                    params.width = displaymetrics.widthPixels / 5;
                    params.height = (int) (params.width / ratio);
                    portrait.setLayoutParams(params);
                    portrait.setScaleType(ImageView.ScaleType.FIT_XY);
                }
            }
            break;

            case CharacterLoader.ALTS_AVAILABLE: {
                final List<Toon> alts = loader.getAlts();
                characterList.setAdapter(ListAdapterFactory.getToonListAdapter(this, alts));
            }
            break;

            case CharacterLoader.EQUIPMENT_AVAILABLE: {
                final IconAndTextAdapter<CharacterEquipment> adapter = new IconAndTextAdapter<CharacterEquipment>(this);
                for (int n = 0; n < loader.getEquipmentCount(); ++n) {
                    final Bitmap bmp = loader.getEquipmentImage(n);
                    if (bmp != null) {
                        final String name = loader.getEquipmentName(n);
                        if (name != null) {
                            final CharacterEquipment entity = new CharacterEquipment(bmp, loader.getEquipmentId(n), name);
                            adapter.addItem(entity);
                        }
                    }
                }

                equipmentList.setAdapter(adapter);
            }
            break;

        }
    }

    private void addDataRow(TableLayout tableStat, ValueField valueField, boolean greyRow) {
        final TableRow tr = new TableRow(this);
        final LayoutParams param = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(param);
        if (greyRow) {
            tr.setBackgroundColor(Color.rgb(32, 32, 32));
        } else {
            tr.setBackgroundColor(android.R.color.black);
        }
        final TextView tv1 = new TextView(this);
        tv1.setText(valueField.getName());
        tv1.setPadding(2, 2, 2, 2);
        final TextView tv2 = new TextView(this);
        if (valueField.getMaxValue() == null) {
            tv2.setText(valueField.getValue());
        } else {
            tv2.setText(valueField.getValue() + "/" + valueField.getMaxValue());
        }
        tv2.setPadding(2, 2, 2, 2);
        tr.addView(tv1);
        tr.addView(tv2);
        tableStat.addView(tr);
    }

    private int addHeadingRow(TableLayout tableStat, ValueField valueField, boolean withSeparator) {
        final int lastSection = valueField.getSection();

//        <View android:layout_width="fill_parent" android:layout_height="2dip"
//        android:background="#FFFFFFFF"/>

        if (withSeparator) {
            View lineView = new View(this);
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    2.0F, getResources().getDisplayMetrics());
            lineView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, height));
            lineView.setBackgroundColor(Color.rgb(255, 255, 255));
            tableStat.addView(lineView);
        }

        final TableRow tr = new TableRow(this);
        final LayoutParams param = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        tr.setLayoutParams(param);
        tr.setBackgroundColor(android.R.color.black);
        final TextView tv1 = new TextView(this);

        SpannableString content = new SpannableString(valueField.getSectionName());
        content.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), 0, content.length(), 0);
        tv1.setText(content);

        tv1.setPadding(2, 2, 2, 2);
        final TextView tv2 = new TextView(this);
        tv2.setText("");
        tv2.setPadding(2, 2, 2, 2);
        tr.addView(tv1);
        tr.addView(tv2);
        tableStat.addView(tr);
        return lastSection;
    }

    static class AltMenu implements View.OnCreateContextMenuListener {
        @Override
        public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
            menu.setQwertyMode(true);

            MenuItem item;

            item = menu.add(MENU_GROUP_CHARACTER, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
            item.setAlphabeticShortcut('f');
            item.setIcon(R.drawable.menu_icon_favourite);

            item = menu.add(MENU_GROUP_CHARACTER, MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
            item.setAlphabeticShortcut('s');
            item.setIcon(R.drawable.menu_icon_shortcut);

        }
    }

    static class EquipmentMenu implements View.OnCreateContextMenuListener {
        @Override
        public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
            menu.setQwertyMode(true);

            MenuItem item;

            item = menu.add(MENU_GROUP_ITEM, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
            item.setAlphabeticShortcut('f');
            item.setIcon(R.drawable.menu_icon_favourite);

            item = menu.add(MENU_GROUP_ITEM, MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
            item.setAlphabeticShortcut('s');
            item.setIcon(R.drawable.menu_icon_shortcut);
        }
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case MENU_ADD_FAVOURITE:
                addFavourite(item.getGroupId(), info.position);
                break;
            case MENU_CREATE_SHORTCUT:
                createShortcut(item.getGroupId(), info.position);
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void addFavourite(final int group, final int position) {
        if (group == MENU_GROUP_CHARACTER) {
            final Toon entity = (Toon) characterList.getItemAtPosition(position);
            Config.LOG.debug("Position " + position + ", character=" + entity);
            OrmPersistence.store(this, entity);
        } else {
            final CharacterEquipment entity = (CharacterEquipment) equipmentList.getItemAtPosition(position);
            Config.LOG.debug("Position " + position + ", item=" + entity);
            final Item item = entity.toItemEntity(theCharacter.getGame());
            OrmPersistence.store(this, item);

        }
        Toast.makeText(this, R.string.addedFavourite, Toast.LENGTH_SHORT).show();
    }

    private void createShortcut(final int group, final int position) {
        if (group == MENU_GROUP_CHARACTER) {
            final Toon entity = (Toon) characterList.getItemAtPosition(position);
            Config.LOG.debug("Position " + position + ", character=" + entity);
            if (entity != null) {
                ShortcutHelper.createCharacterShortcut(this, entity);
            }
        } else {
            final CharacterEquipment entity = (CharacterEquipment) equipmentList.getItemAtPosition(position);
            if (entity != null) {
                final Item newEntity = entity.toItemEntity(theCharacter.getGame());
                ShortcutHelper.createItemShortcut(this, newEntity);
            }
        }
    }

    private void characterClicked(final int position) {
        final Toon entity = (Toon) characterList.getItemAtPosition(position);
        NavigationHelper.CharacterView.view(this, entity);
    }

}
