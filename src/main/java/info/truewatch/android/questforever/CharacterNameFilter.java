/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.persistence.GuildMember;

/**
 * Filter used for searching characters by name
 * @author krupagj
 */
public class CharacterNameFilter implements CharacterFilter {

	private final String nameSubstring;

	/**
	 * Construct a filter using the specified substring
	 * @param nameSubstring sub-string that all results returned must contain.  Can be empty to return all results.
	 */
	public CharacterNameFilter(String nameSubstring) {
		this.nameSubstring = nameSubstring;
	}

	@Override
	public boolean characterAllowed(GuildMember member) {
		return member.getName().toLowerCase().contains(nameSubstring.toLowerCase());
	}

}
