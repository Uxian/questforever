/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.persistence.Guild;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.core.persistence.Toon;
import android.content.Context;
import android.content.Intent;

public class ShortcutHelper {

	private static void installShortcut(Context ctx, Intent shortcutIntent, String name) {
		shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		final Intent intent = new Intent();
		intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		// Sets the custom shortcut's title
		intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
		// Set the custom shortcut icon
		intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(ctx, R.drawable.ic_app));
		// add the shortcut
		intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		ctx.sendBroadcast(intent);
	}

	public static void createGuildShortcut(Context ctx, Guild entity) {
		final Intent shortcutIntent = NavigationHelper.GuildView.getIntent(ctx, entity);
		installShortcut(ctx, shortcutIntent, entity.getName() + " / " + entity.getServer());
	}

	public static void createCharacterShortcut(Context ctx, Toon entity) {
		final Intent shortcutIntent = NavigationHelper.CharacterView.getIntent(ctx, entity);
		installShortcut(ctx, shortcutIntent, entity.getName() + " / " + entity.getServer());
	}

	public static void createItemShortcut(Context ctx, Item entity) {
		final Intent shortcutIntent = NavigationHelper.ItemView.getIntent(ctx, entity);
		installShortcut(ctx, shortcutIntent, entity.getName());
	}

}
