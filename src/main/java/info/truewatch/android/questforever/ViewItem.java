/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.info.PreferenceConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.core.data.eq2.Eq2ItemLoaderHtml;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class ViewItem extends Activity implements ProgressNotification {

    private static final int MENU_ADD_FAVOURITE = 1;
    private static final int MENU_CREATE_SHORTCUT = 2;

    private Item theItem;
    private String uri;

    private final Handler handler = new Handler();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewitem);

        theItem = NavigationHelper.ItemView.fromIntent(this, getIntent());

        String title = "";
        if (theItem.getName() != null) {
            Config.LOG.debug("Item URI is " + uri);
            title = MessageFormat.format(getString(R.string.itemNameFormat), theItem.getName());
        } else {
            title = MessageFormat.format(getString(R.string.unknownItemFormat), theItem.getId());
        }
        setTitle(title);

        if (theItem.getGame() == GameConstants.EVERQUEST2) {
            uri = "http://eq2players.station.sony.com/item/" + theItem.getId();
        } else {
            uri = "http://eqplayers.station.sony.com/item_details.vm?itemId=" + theItem.getId();
        }

    }


    @Override
    protected void onStart() {
        super.onStart();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (theItem.getId() != -1) {
            if (theItem.getGame() == GameConstants.EVERQUEST2) {
                loadItemEq2();
            }
        }
    }

    private void loadItemEq2() {
        final ProgressMonitor m = new ProgressMonitor(this, "Loading Item", "Loading", false);
        final ProgressBroadcaster broad = new ProgressBroadcaster(m, this);

        final Runnable r = new Runnable() {
            @Override
            public void run() {
                doLoadItemEq2(broad);
            }
        };

        new Thread(r).start();
    }

    private void doLoadItemEq2(final ProgressNotification nfy) {
        try {
            final HttpSession session = new HttpSession(this);
            session.setNotifier(nfy);
            String html = Eq2ItemLoaderHtml.loadItemDetails(theItem, session);

            Config.LOG.info(html);

            nfy.notifyProgress(true, 1, 1, null, html);
        } catch (final Exception ex) {
            Config.LOG.error("Error", ex);
            nfy.notifyError(ex, this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (theItem != null && theItem.getId() != -1) {
            switch (item.getItemId()) {
                case MENU_ADD_FAVOURITE:
                    OrmPersistence.store(this, theItem);
                    Toast.makeText(this, R.string.addedFavourite, Toast.LENGTH_SHORT).show();
                    break;
                case MENU_CREATE_SHORTCUT:
                    ShortcutHelper.createItemShortcut(this, theItem);
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        menu.setQwertyMode(true);

        MenuItem item;

        item = menu.add(Menu.NONE, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
        item.setAlphabeticShortcut('f');
        item.setIcon(R.drawable.menu_icon_favourite);

        item = menu.add(Menu.NONE, MENU_CREATE_SHORTCUT, Menu.NONE, R.string.createShortcut);
        item.setAlphabeticShortcut('s');
        item.setIcon(R.drawable.menu_icon_shortcut);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void notifyError(final Throwable problem, final Object source) {
        final Map<String, String> data = new HashMap<String, String>();
        if (theItem != null) {
            data.put("game", GameConstants.getName(theItem.getGame()));
            data.put("item", theItem.getName());
            data.put("itemId", "" + theItem.getId());
        }
        ErrorHelper.instance().showErrorDialog(this, handler, problem, null, data);
    }

    @Override
    public void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText, final Object source) {
        if (completed) {
            final String html = (String) source;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    final WebView htmlView = (WebView) findViewById(R.id.itemView);
                    htmlView.loadDataWithBaseURL(uri, html, "text/html", "utf-8", null);
                }
            });
        }
    }
}
