/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import info.truewatch.android.questforever.core.ProgressNotification;

import java.util.LinkedList;
import java.util.List;

public class ProgressBroadcaster implements ProgressNotification {

	private List<ProgressNotification> targetList = new LinkedList<ProgressNotification>();

	public ProgressBroadcaster(ProgressNotification... targets) {
		for (ProgressNotification target: targets) {
			targetList.add(target);
		}
	}

	public void addTarget(ProgressNotification nfy) {
		targetList.add(nfy);
	}

	public void removeTarget(ProgressNotification nfy) {
		targetList.remove(nfy);
	}

	@Override
	public void notifyError(Throwable problem, Object source) {
		for (ProgressNotification nfy: targetList) {
			nfy.notifyError(problem, source);
		}
	}

	@Override
	public void notifyProgress(boolean completed, int currentProgress, int maxProgress, String progressText, Object source) {
		for (ProgressNotification nfy: targetList) {
			nfy.notifyProgress(completed, currentProgress, maxProgress, progressText, source);
		}
	}

}
