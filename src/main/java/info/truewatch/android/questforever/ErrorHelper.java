/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.Toast;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.exception.DataServerConnectException;
import info.truewatch.android.questforever.core.exception.NonReportableException;
import info.truewatch.android.questforever.core.exception.NonReportableRuntimeException;
import info.truewatch.android.questforever.core.exception.ReportViaGoogleException;
import org.apache.http.NoHttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Map;

public class ErrorHelper {

    private static ErrorHelper instance = null;

    private ErrorHelper() {
    }

    public static ErrorHelper instance() {
        if (instance == null) {
            instance = new ErrorHelper();
        }
        return instance;
    }

    public void showErrorDialog(final Context context, final Handler handler, final Throwable error, final Runnable retryMethod, final Map<String, String> extraData) {
        runInHandler(handler, new ErrorDialogRunner(context, translateException(error, context), retryMethod, error, extraData));
    }

    public void showChatSystemError(final Context context, final Handler handler, final Exception problem, final Runnable retryMethod, final Map<String, String> extraData) {
        final String message = context.getString(R.string.chat_system_error, problem.getClass().getSimpleName(), problem.getMessage());
        runInHandler(handler, new ErrorDialogRunner(context, message, retryMethod, problem, extraData));
    }

    public void showErrorDialog(final Context context, final Handler handler, final String error, final Runnable retryMethod, final Map<String, String> extraData) {
        runInHandler(handler, new ErrorDialogRunner(context, error, retryMethod, null, extraData));
    }

    private void runInHandler(final Handler handler, final Runnable task) {
        if (handler == null) {
            task.run();
        } else {
            handler.post(task);
        }
    }

    private String translateException(final Throwable error, final Context context) {
        String errorText = transformException(error).getMessage();

        if (error instanceof RuntimeException || error instanceof Error) {
            errorText = context.getString(R.string.unknownError);
        } else if (error instanceof WebErrorException) {
            errorText = context.getString(R.string.webSiteError);
        }

        return errorText;
    }

    private static Throwable transformException(Throwable original) {
        if (original instanceof ConnectException
                || original instanceof SocketTimeoutException
                || original instanceof SocketException
                || original instanceof HttpHostConnectException
                || original instanceof ConnectTimeoutException
                || original instanceof FileNotFoundException
                || original instanceof NoHttpResponseException) {
            return new DataServerConnectException(original);
        } else {
            return original;
        }
    }

    static class ErrorDialogRunner implements Runnable {

        private final Context context;
        private final String errorMessage;
        private final Runnable retryCommand;
        private final Throwable error;
        private final Map<String, String> extraData;

        public ErrorDialogRunner(final Context context, final String errorMessage, final Runnable retryCommand, final Throwable error, final Map<String, String> extraData) {
            super();
            this.context = context;
            this.errorMessage = errorMessage;
            this.retryCommand = retryCommand;
            this.error = transformException(error) instanceof NonReportableRuntimeException || transformException(error) instanceof NonReportableException ? null : error;
            this.extraData = extraData;
        }

        @Override
        public void run() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle("Error").setMessage(errorMessage).setIcon(R.drawable.ic_app);
            if (retryCommand != null && error != null) {
                builder.setSingleChoiceItems(new String[]{"Report", "Retry"}, 0, new OnListItemSelected());
                builder.setNeutralButton(R.string.reportBasic, new OnReportGoogleClicked());
                builder.setNegativeButton(android.R.string.cancel, null);
            } else if (error != null) {
                builder.setPositiveButton(R.string.reportError, new OnReportClicked());
                builder.setNeutralButton(R.string.reportBasic, new OnReportGoogleClicked());
                builder.setNegativeButton(android.R.string.cancel, null);
            } else if (retryCommand != null) {
                builder.setPositiveButton(R.string.reportError, new OnRetryClicked());
                builder.setNegativeButton(android.R.string.cancel, null);
            } else {
                builder.setNegativeButton(android.R.string.ok, null);
            }

            try {
                builder.create().show();
            } catch (WindowManager.BadTokenException ex) {
                // No-op
            }
        }

        private class OnListItemSelected implements DialogInterface.OnClickListener {
            @Override
            public void onClick(final DialogInterface dlg, final int item) {
                if (item == 0 && error != null) {
                    reportError();
                } else if (retryCommand != null) {
                    retryCommand.run();
                }
                dlg.dismiss();
            }
        }

        private class OnRetryClicked implements DialogInterface.OnClickListener {
            @Override
            public void onClick(final DialogInterface dlg, final int item) {
                if (retryCommand != null) {
                    retryCommand.run();
                }
            }
        }

        private class OnReportClicked implements DialogInterface.OnClickListener {
            @Override
            public void onClick(final DialogInterface dlg, final int item) {
                if (error != null) {
                    reportError();
                }
            }
        }

        private class OnReportGoogleClicked implements DialogInterface.OnClickListener {
            @Override
            public void onClick(final DialogInterface dlg, final int item) {
                throw new ReportViaGoogleException(error);
            }
        }

        private void reportError() {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"uxian.tw@gmail.com"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "Quest Forever Error Report");
            intent.putExtra(Intent.EXTRA_TEXT, constructErrorText()); // do this so some email clients don't complain about empty body.
            context.startActivity(intent);
        }

        private String constructErrorText() {
            StringBuffer buffer = new StringBuffer();

            try {
                PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                buffer.append("App: ").append(info.packageName).append('\n');
                buffer.append("Version: ").append(info.versionName).append('\n');
                buffer.append("Version Code: ").append(info.versionCode).append('\n');
                buffer.append("Error: ").append(errorMessage).append('\n');
            } catch (PackageManager.NameNotFoundException e) {
                // No-op
            }

            buffer.append("OS: ").append(Build.VERSION.SDK).append('\n');
            buffer.append("Device: ").append(Build.DEVICE).append('\n');
            buffer.append("Manufacturer: ").append(Build.MANUFACTURER).append('\n');
            buffer.append("Model: ").append(Build.MODEL).append('\n');
            buffer.append("Product: ").append(Build.PRODUCT).append('\n');

            if (error != null) {
                StringWriter sw = new StringWriter();
                error.printStackTrace(new PrintWriter(sw));
                buffer.append("Stack Trace: ").append('\n').append(sw.toString()).append("\n\n");
            }

            String retval = buffer.toString();
            Config.LOG.info(retval);
            return retval;
        }
    }

}
