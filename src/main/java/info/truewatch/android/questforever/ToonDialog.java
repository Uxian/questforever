/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class ToonDialog {

	private List<String> toons;

	private final SimpleNotification nfy;

	AlertDialog dialog;

	private String selection;

	public ToonDialog(Context ctx, List<String> data, SimpleNotification nfy) {
		this.nfy = nfy;
		toons = data;

		if (data.size() > 0) {
			selection = data.get(0);
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(R.string.selectToon);

		builder.setSingleChoiceItems(data.toArray(new String[0]), 0, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				selection = toons.get(which);
			}
		});

		builder.setNegativeButton(android.R.string.cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				onCancel(dialog);
			}
		});

		builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				onOk(dialog);
			}
		});

		dialog = builder.create();
	}

	public void show() {
		dialog.show();
	}

	protected void onOk(DialogInterface dialog) {
		if (nfy != null) {
			HashMap<String,String> dataMap = new HashMap<String,String>();
			dataMap.put("name", selection);
			nfy.onEvent(dataMap);
		}
		dialog.dismiss();
	}

	private void onCancel(DialogInterface dialog) {
		if (nfy != null) {
			nfy.onEvent(null);
        }
		dialog.dismiss();
	}
}
