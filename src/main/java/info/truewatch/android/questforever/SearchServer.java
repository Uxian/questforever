/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever;

import android.os.Bundle;
import android.os.Handler;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.exception.GameNotSupportedException;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.Server;
import info.truewatch.android.questforever.core.data.ParserFactory;
import info.truewatch.android.questforever.core.data.ServerSearcher;
import info.truewatch.android.questforever.widget.ListAdapterFactory;

import java.util.HashMap;
import java.util.Map;

public class SearchServer extends SearchActivity implements ProgressNotification {

    private static final int MENU_ADD_FAVOURITE = 1;

    EditText serverName;
    ListView serverList;
    Button searchButton, addButton;

    Handler handler = new Handler();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        setTitle(R.string.searchServersTitle);

        searchButton = (Button) findViewById(R.id.SearchButton);
        serverName = (EditText) findViewById(R.id.SearchName);
        serverList = (ListView) findViewById(R.id.ResultList);

        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                onClickSearch();
            }
        });

        serverList.setOnCreateContextMenuListener(this);

        serverList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                onClickList(position);
            }

        });

        serverName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    onClickSearch();
                    return true;
                }
                return false;
            }
        });
    }

    private void addFavourite(final int position) {
        Server entity = (Server) serverList.getItemAtPosition(position);
        final Server originalServer = OrmPersistence.getObject(this, Server.class, "name", entity.getName(), "game", entity.getGame());
        if (originalServer != null) {
            originalServer.setName(entity.getName());
            entity = originalServer;
        }
        OrmPersistence.store(this, entity);
        Toast.makeText(this, R.string.addedFavourite, Toast.LENGTH_SHORT).show();
    }

    private void onClickList(final int position) {
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
        menu.setQwertyMode(true);

        MenuItem item;

        item = menu.add(Menu.NONE, MENU_ADD_FAVOURITE, Menu.NONE, R.string.addFavourite);
        item.setAlphabeticShortcut('f');
        item.setIcon(R.drawable.menu_icon_favourite);
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case MENU_ADD_FAVOURITE:
                addFavourite(info.position);
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void onClickSearch() {
        final ServerSearcher searcher;
        try {
            searcher = ParserFactory.getServerSearcher(this, getGame(), serverName.getText().toString());
            final ProgressMonitor mon = new ProgressMonitor(this, "Search for Servers", "Connecting");
            searcher.setNotifier(new ProgressBroadcaster(mon, this));
            new Thread(searcher).start();
        } catch (GameNotSupportedException e) {
            notifyError(e, null);
        }
    }

    @Override
    public void notifyError(final Throwable problem, final Object source) {
        final Map<String, String> data = new HashMap<String, String>();
        data.put("query", serverName.getText().toString());
        ErrorHelper.instance().showErrorDialog(SearchServer.this, handler, problem, null, data);
    }

    @Override
    public void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText, final Object source) {
        if (completed) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    final ServerSearcher srch = (ServerSearcher) source;
                    serverList.setAdapter(ListAdapterFactory.getServerListAdapter(SearchServer.this, srch.getResults()));
                }
            });
        }
    }

}
