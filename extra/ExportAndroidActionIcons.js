var j, sourceDoc, targetFile;

var destFolder = null;
// Get the destination to save the files
destFolder = Folder.selectDialog( 'Select the folder where you want to save the exported files.', 'E:/projects/QuestForever/app-modern/res' );

if (destFolder != null) {
    sourceDoc = app.activeDocument; // returns the document object

	for (dpi = 0; dpi < 4; ++dpi) {
	
		var size = 0;
		var dpiname = '';
	
		if (dpi == 0) {
			size = 16;
			dpiname = 'ldpi';
		} else if (dpi == 1) {
			size = 24;
			dpiname = 'mdpi';
		} else if (dpi == 2) {
			size = 36;
			dpiname = 'hdpi';
		} else if (dpi == 3) {
			size = 48;
			dpiname = 'xhdpi';
		}
		
		targetFile = getNewName(sourceDoc, destFolder + '/drawable-' + dpiname);

		// set PNG export options
		var opt = new ExportOptionsPNG24();
		opt.antiAliasing = true;
		opt.transparency = true;
		opt.artBoardClipping = true;
		opt.verticalScale = size / sourceDoc.height * 100;
		opt.horizontalScale = size / sourceDoc.width * 100;
		// Export
		sourceDoc.exportFile(targetFile, ExportType.PNG24, opt);
	}
  alert( 'Files are saved as PNG24 in ' + destFolder );
}

function getNewName(sourceDoc, destFolder) {
  var docName = sourceDoc.name;
  var ext = '.png'; // new extension for png file
  var newName = "";

  // if name has no dot (and hence no extension,
  // just append the extension
  if (docName.indexOf('.') < 0) {
    newName = docName + ext;
  } else {
    var dot = docName.lastIndexOf('.');
    newName += docName.substring(0, dot);
    newName += ext;
  }

  // Create a file object to save the png
  saveInFile = new File( destFolder + '/' + newName );
  return saveInFile;
}