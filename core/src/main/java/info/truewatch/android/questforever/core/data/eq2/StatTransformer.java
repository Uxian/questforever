/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import info.truewatch.android.questforever.core.data.SearchUtilities;

import java.io.IOException;
import java.util.Properties;

public class StatTransformer {

    private static Properties statTranslation = null;

    public static synchronized String translateStat(String original) {
        if (statTranslation == null) {
            try {
                info.truewatch.android.questforever.core.Config.LOG.debug("Loading statistics mapper");
                statTranslation = new Properties();
                statTranslation.load(StatTransformer.class.getResourceAsStream("/statmap.properties"));
            } catch (IOException ex) {
                info.truewatch.android.questforever.core.Config.LOG.error("Error reading statistics map", ex);
            }
        }

        String translated = statTranslation.getProperty(original, original);

        if (original.equals(translated)) {
            translated = SearchUtilities.capitalise(original);
        }
        return translated;
    }

}
