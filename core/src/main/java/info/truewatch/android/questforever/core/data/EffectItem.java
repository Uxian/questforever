/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data;

/**
 * Created by IntelliJ IDEA.
 * User: krupagj
 * Date: 05/02/12
 * Time: 19:59
 * To change this template use File | Settings | File Templates.
 */
public class EffectItem {
    
    private final int indentationLevel;
    private final String text;

    public EffectItem(int indentationLevel, String text) {
        this.indentationLevel = indentationLevel;
        this.text = text;
    }

    public int getIndentationLevel() {
        return indentationLevel;
    }

    public String getText() {
        return text;
    }
}
