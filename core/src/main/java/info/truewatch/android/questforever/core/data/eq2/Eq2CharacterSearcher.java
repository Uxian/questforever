/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import android.content.Context;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.persistence.Toon;
import info.truewatch.android.questforever.core.data.CharacterSearcher;
import info.truewatch.android.questforever.core.data.SearchUtilities;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Eq2CharacterSearcher implements Runnable, CharacterSearcher {

    public static final String SEARCH_TOON_FIELDS = "id,name,guild.rank,type,tradeskills,locationdata";
    private ProgressNotification notifier;
    private final String searchString;
    private List<Toon> results;

    private final Context context;

    public Eq2CharacterSearcher(final Context context, final String searchString) {
        this.searchString = searchString;
        this.context = context;
    }

    public static Toon jsonToSearchToon(JSONObject foundToon) throws JSONException {
        JSONObject nameObject = foundToon.getJSONObject("name");
        StringBuilder name = new StringBuilder(nameObject.getString("first"));
        if (!nameObject.isNull("last") && !"".equals(nameObject.getString("last"))) {
            name.append(" ").append(nameObject.getString("last"));
        }

        final int level = Integer.parseInt(foundToon.getJSONObject("type").getString("level"));
        final String server = foundToon.getJSONObject("locationdata").getString("world");
        String race = foundToon.getJSONObject("type").getString("race");
        String charClass = foundToon.getJSONObject("type").getString("class");
        final long id = Long.parseLong(foundToon.getString("id"));

        final Toon toon = new Toon();
        toon.setId(id);
        toon.setName(name.toString());
        toon.setServer(server);
        toon.setGame(GameConstants.EVERQUEST2);
        toon.setCharacterClass(charClass);
        toon.setLevel(level);
        toon.setRace(race);
        return toon;
    }

    @Override
    public void run() {
        try {
            results = findCharacters();
            notifyProgress(true, 3, 3, "Finished");
        } catch (final Exception e) {
            notifyError(e);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterSearcher#setNotifier(info.truewatch.android.questforever.core.ProgressNotification)
      */
    @Override
    public void setNotifier(final info.truewatch.android.questforever.core.ProgressNotification notifier) {
        this.notifier = notifier;
    }

    private void notifyError(final Throwable problem) {
        if (notifier != null) {
            notifier.notifyError(problem, this);
        }
    }

    private void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText) {
        if (notifier != null) {
            notifier.notifyProgress(completed, currentProgress, maxProgress,
                    progressText, this);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterSearcher#findCharacters()
      */
    @Override
    @SuppressWarnings("unchecked")
    public List<Toon> findCharacters() throws IOException, JSONException {

        info.truewatch.android.questforever.core.Config.LOG.info("Starting search");

        final LinkedList<Toon> retval = new LinkedList<Toon>();
        String peopleUrl = JSONUrlBuilder.characterUrl(null, 100,
                JSONUrlBuilder.PARAM_SHOW, SEARCH_TOON_FIELDS,
                JSONUrlBuilder.PARAM_SORT, "displayname",
                "displayname", "^" + SearchUtilities.capitalise(searchString));

        final HttpSession session = new HttpSession(context);
        notifyProgress(false, 0, 100, "Connecting");

        try {
            JSONArray characters = session.loadJSonDocument(peopleUrl).getJSONArray("character_list");

            notifyProgress(false, 0, 2, "Connecting");

            for (int n = 0; n < characters.length(); ++n) {
                JSONObject foundToon = characters.getJSONObject(n);
                retval.add(jsonToSearchToon(foundToon));
            }
            return retval;
        } catch (JSONException ex) {
            info.truewatch.android.questforever.core.Config.LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    /* (non-Javadoc)
    * @see info.truewatch.android.questforever.core.xml.eq2.CharacterSearcher#getResults()
    */
    @Override
    public List<Toon> getResults() {
        return results;
    }

}
