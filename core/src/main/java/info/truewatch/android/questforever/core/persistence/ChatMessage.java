/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.persistence;

import info.truewatch.android.questforever.core.orm.annotation.Column;
import info.truewatch.android.questforever.core.orm.annotation.Entity;

@Entity(tableName="message", primaryKey={"timestamp"})
public class ChatMessage implements Comparable<ChatMessage> {

	public static final int TYPE_RECEIVED = 0;
	public static final int TYPE_PRIVATE = 1;

	private Long timestamp;
	private String server;
	private String guild;
	private String from;
	private String to;
	private String text;
	private String type;

	@Override
	public int compareTo(final ChatMessage another) {
		return timestamp.compareTo(another.timestamp);
	}

	@Column(name="timestamp", notNull=true, version=1)
	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(final Long timestamp) {
		this.timestamp = timestamp;
	}

	@Column(name="sender", notNull=true, version=1)
	public String getFrom() {
		return from;
	}

	public void setFrom(final String from) {
		this.from = from;
	}

	@Column(name="recipient", notNull=false, version=1)
	public String getTo() {
		return to;
	}

	public void setTo(final String to) {
		this.to = to;
	}

	@Column(name="text", notNull=true, version=1)
	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@Column(name="guild", notNull=true, version=1)
	public String getGuild() {
		return guild;
	}

	public void setGuild(final String guild) {
		this.guild = guild;
	}

	@Column(name="server", notNull=true, version=1)
	public String getServer() {
		return server;
	}

	public void setServer(final String server) {
		this.server = server;
	}

	@Column(name="msgtype", notNull=true, version=1)
	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}
}
