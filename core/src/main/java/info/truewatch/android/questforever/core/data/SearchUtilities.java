/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data;

import android.text.TextUtils;

/**
 * Utility class that processes strings into formats that are more friendly for the SOE data systems that
 * prefers non-case-sensitive query strings.
 */
public class SearchUtilities {
    public static String capitalise(String original) {
        if (original == null || TextUtils.isEmpty(original)) {
            return original;
        } else {
            String translated;
            StringBuilder str = new StringBuilder(original);
            str.replace(0, 1, original.substring(0, 1).toUpperCase());
            translated = str.toString();
            return translated;
        }
    }
}
