/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.persistence;

import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.annotation.Column;
import info.truewatch.android.questforever.core.orm.annotation.Entity;

import java.util.Date;

@Entity(tableName="guild", primaryKey={"game", "id"})
public class Guild implements Comparable<Guild> {

	private String name;
	private String server;
	private long id;
	private int game;
	private long lastUpdated;

	public Guild() {
	}

	/**
	 * Convert from old DB format
	 * @param name guild name
	 * @param server server name
	 * @param id guild ID
	 */
	public Guild(String name, String server, int id) {
		this.name = name;
		this.server = server;
		this.id = id;
		this.game = GameConstants.EVERQUEST2;
		this.lastUpdated = -1;
	}

	@Column(name="name", version=1, notNull=true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="server", version=1, notNull=true)
	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	@Column(name="id", type="LONG", version=1, notNull=true)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name="game", type="INTEGER", version=1, notNull=true)
	public int getGame() {
		return game;
	}

	public void setGame(int game) {
		this.game = game;
	}

	@Column(name="lastUpdate", type="LONG", version=1)
	public long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public boolean hasBeenUpdated() {
		return lastUpdated > -1;
	}

	public Date getLastUpdateDate() {
		if (lastUpdated < 0) {
			return null;
		}
		return new Date(lastUpdated);
	}

	public void setLastUpdateDate(Date date) {
		if (date != null) {
			this.lastUpdated = date.getTime();
		} else {
			this.lastUpdated = -1;
		}
	}

	public void refreshLastUpdate() {
		this.lastUpdated = new Date().getTime();
	}

	public String toString() {
		return name + " (" + server + ")";
	}

	public String getLocation() {
		return server + ", " + GameConstants.getName(game);
	}

	public int compareTo(Guild other) {
		int diff = name.compareTo(other.name);
		if (diff == 0) {
			diff = server.compareTo(other.server);
		}
		if (diff == 0) {
			diff = game - other.game;
		}
		return diff;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Guild) {
			Guild other = (Guild)o;
			return name.equals(other.name) && server.equals(other.server) && game == other.game;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return name.hashCode() | server.hashCode() | game;
	}


}
