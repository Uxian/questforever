/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.orm;

import info.truewatch.android.questforever.core.orm.annotation.Column;

public class ColumnData {
	public String getterName;
	public String setterName;
	public String columnName;
	public int version;
	public String type;
	public boolean primaryKey;
	public boolean notNull;

	public static ColumnData fromAnnotation(String fieldName, Column column) {
		ColumnData data = new ColumnData();
		if (fieldName.startsWith("get")) {
			data.getterName = fieldName;
			data.setterName = fieldName.replaceFirst("get", "set");
		} else if (fieldName.startsWith("set")) {
			data.setterName = fieldName;
			data.getterName = fieldName.replaceFirst("set", "get");
		}

		data.columnName = column.name();
		data.version = column.version();
		data.type = column.type();
		data.notNull = column.notNull();
		return data;
	}
}
