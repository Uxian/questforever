/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package info.truewatch.android.questforever.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import info.truewatch.android.questforever.core.Config;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Utility class to load a bitmap from the WWW
 * @author krupagj
 */
public class BitmapLoader {

	/**
	 * Load the specified bitmap from the Internet
	 * @param urlString URL from which to load the bitmap
	 * @return the bitmap object
	 * @throws IOException if the remote file can't be accessed
	 */
	public static Bitmap load(Context context, String urlString) throws IOException {
		URL url = new URL(urlString);
		return load(context, url);
	}

	/**
	 * Load the specified bitmap from the Internet
	 * @param url URL from which to load the bitmap
	 * @return the bitmap object
	 * @throws IOException if the remote file can't be accessed
	 */
	public static Bitmap load(Context context, URL url) throws IOException {
		HttpClient client = Config.createHttpClient(context);

		HttpGet get = new HttpGet(url.toString());
		HttpResponse resp = client.execute(get);

		Config.LOG.info("Got response code: " + resp.getStatusLine().getStatusCode() + " for bitmap " + url.toString());

		HttpEntity entity = resp.getEntity();
		if (entity != null) {
			try {
				Config.LOG.debug("Size: " + entity.getContentLength());
				InputStream is = entity.getContent();
                return BitmapFactory.decodeStream(is);
            } catch (OutOfMemoryError err) {
                return null;
            } finally {
				entity.consumeContent();
			}
		}

		return null;
	}
}
