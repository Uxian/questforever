/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by IntelliJ IDEA.
 * User: krupagj
 * Date: 16/12/11
 * Time: 18:28
 * To change this template use File | Settings | File Templates.
 */
public class ValueField implements Comparable<ValueField> {

    private final String name;
    private final String value;
    private final String sectionName;
    private String maxValue;
    private final int section;

    public ValueField(String name, String value, int section, String sectionName) {
        this.name = name;
        this.value = formatNumeric(value);
        this.section = section;
        this.sectionName = sectionName;
    }

    private static String formatNumeric(String value) {
        if (value.matches("^[0-9]+$")) {
            long amount = Long.parseLong(value);
            NumberFormat format = DecimalFormat.getNumberInstance();
            format.setGroupingUsed(true);
            return format.format(amount);
        }
        return value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public int getSection() {
        return section;
    }

    public String getSectionName() {
        return sectionName;
    }

    @Override
    public int compareTo(ValueField other) {
        int comparison = section - other.section;
        if (comparison == 0) {
            comparison = name.compareTo(other.name);
        }
        return comparison;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValueField that = (ValueField) o;

        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
