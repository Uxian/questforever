/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Configuration data for the application.
 * @author krupagj
 */
public class Config {

	/**
	 * Standard tag to use in all log messages for ease of filtering
	 */
	private static final String TAG = "QuestForever";

	/**
	 * Logger to use in the application
	 */
	public static final Logger LOG = LoggerFactory.getLogger(TAG);

	/**
	 * Get the currently configured proxy server on the Android system
	 * @return details of the proxy server or null if proxy is disabled
	 */
	public static HttpHost getProxy(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		HttpHost theProxy = null;
		String proxyHost = android.net.Proxy.getDefaultHost();

		if (prefs.getBoolean("disableProxy", false)) {
			LOG.warn("Proxy bypass is in effect");
			theProxy = null;
		} else if (proxyHost != null) {
			int proxyPort = android.net.Proxy.getDefaultPort();
			LOG.info("Proxy is " + proxyHost + ":" + proxyPort);
			theProxy = new HttpHost(proxyHost, proxyPort);
		} else {
			LOG.info("No proxy configured");
			theProxy = null;
		}

		return theProxy;
	}

	/**
	 * Construct a new HTTP client using the currently configured proxy server
	 * @return a clean HTTP client
	 */
	public static HttpClient createHttpClient(Context context) {
		HttpClient client = new DefaultHttpClient();
		HttpHost proxy = getProxy(context);
		if (proxy != null) {
			client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		}
		return client;
	}
}
