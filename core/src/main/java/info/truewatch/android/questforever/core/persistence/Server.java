/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.persistence;

import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.annotation.Column;
import info.truewatch.android.questforever.core.orm.annotation.Entity;

import java.util.Queue;

@Entity(tableName="server", primaryKey={"name", "game"})
public class Server {

    private String name;
    private int game;
    private String category;

    private String age;
    private String status;

    @Column(name="name", notNull=true, version=1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="game", type="INTEGER", version=1, notNull=true)
    public int getGame() {
        return game;
    }

    public void setGame(int game) {
        this.game = game;
    }

    @Column(name="category", notNull=true, version=1)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDetails() {
        StringBuilder builder = new StringBuilder();
        if (getStatus() != null && getAge() != null) {
            builder.append("Status: ").append(getStatus()).append(", ");
            builder.append("Last Check: ").append(getAge()).append(", ");
        } else {
            builder.append("No status available, ");
        }
        builder.append('(').append(getCategory()).append(')');
        return builder.toString();
    }
}
