/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.persistence;

import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.annotation.Column;
import info.truewatch.android.questforever.core.orm.annotation.Entity;

@Entity(tableName="toon", primaryKey={"name", "game", "server"})
public class Toon {

	private String name;
	private String server;
	private int game;
	private long id;

	private String characterClass;
	private int level;
	private String race;

	public Toon() {
	}

	/**
	 * Upgrade from version 1/2 data
	 * @param name character name
	 * @param server server name
	 */
	public Toon(String name, String server) {
		this.name = name;
		this.server = server;
		this.game = GameConstants.EVERQUEST2;
	}

	@Column(name="name", notNull=true, version=1)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="server", notNull=true, version=1)
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}

	@Column(name="game", notNull=true, type="INTEGER", version=1)
	public int getGame() {
		return game;
	}
	public void setGame(int game) {
		this.game = game;
	}

	public String getLocation() {

		StringBuffer buf = new StringBuffer();

		if (level > 0) {
			buf.append(level).append(" ");
		}

		if (race != null) {
			buf.append(race).append(" ");
		}

		if (characterClass != null) {
			buf.append(characterClass).append(", ");
		}

		buf.append(server).append(", ").append(GameConstants.getName(getGame()));
		return buf.toString();
	}

	@Column(name="id", type="LONG", version=1)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCharacterClass() {
		return characterClass;
	}

	public void setCharacterClass(String characterClass) {
		this.characterClass = characterClass;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

    @Override
    public String toString() {
        return name;
    }
}
