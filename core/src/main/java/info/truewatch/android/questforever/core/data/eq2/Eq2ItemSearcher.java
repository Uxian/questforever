/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import android.content.Context;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.core.data.ItemSearcher;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Eq2ItemSearcher implements Runnable, ItemSearcher {

    private ProgressNotification notifier;
    private final String searchString;
    private List<Item> results;

    private final Context context;

    public Eq2ItemSearcher(final Context context, final String searchString) {
        this.searchString = searchString;
        this.context = context;
    }

    @Override
    public void run() {
        try {
            results = findItems();
            notifyProgress(true, 3, 3, "Finished");
        } catch (final Exception e) {
            notifyError(e);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.ItemSearcher#setNotifier(info.truewatch.android.questforever.core.ProgressNotification)
      */
    @Override
    public void setNotifier(final ProgressNotification notifier) {
        this.notifier = notifier;
    }

    private void notifyError(final Throwable problem) {
        if (notifier != null) {
            notifier.notifyError(problem, this);
        }
    }

    private void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText) {
        if (notifier != null) {
            notifier.notifyProgress(completed, currentProgress, maxProgress,
                    progressText, this);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.ItemSearcher#findItems()
      */
    @Override
    @SuppressWarnings("unchecked")
    public List<Item> findItems() throws IOException, JSONException {

        Config.LOG.info("Starting search");

        final LinkedList<Item> retval = new LinkedList<Item>();
        String itemsUrl = JSONUrlBuilder.itemUrl(null, 1000,
                JSONUrlBuilder.PARAM_SHOW, "id,displayname",
                JSONUrlBuilder.PARAM_SORT, "displayname",
                "displayname", "i/" + searchString + "/");

        final HttpSession session = new HttpSession(context);
        notifyProgress(false, 0, 100, "Connecting");

        try {
            JSONArray items = session.loadJSonDocument(itemsUrl).getJSONArray("item_list");

            notifyProgress(false, 0, 2, "Connecting");

            for (int n = 0; n < items.length(); ++n) {
                JSONObject item = items.getJSONObject(n);
                info.truewatch.android.questforever.core.Config.LOG.debug("Found a row");

                final String name = item.getString("displayname");

                final long id = Long.parseLong(item.getString("id"));

                final Item itemEntity = new Item();
                itemEntity.setGame(GameConstants.EVERQUEST2);
                itemEntity.setName(name);
                itemEntity.setId(id);
                retval.add(itemEntity);
            }
        } catch (JSONException ex) {
            info.truewatch.android.questforever.core.Config.LOG.error(ex.getMessage(), ex);
            throw ex;
        }
        return retval;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.ItemSearcher#getResults()
      */
    @Override
    public List<Item> getResults() {
        return results;
    }

}
