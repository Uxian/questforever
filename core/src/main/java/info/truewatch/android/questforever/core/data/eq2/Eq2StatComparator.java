/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import info.truewatch.android.questforever.core.Config;

import java.io.IOException;
import java.util.Comparator;
import java.util.Properties;

public class Eq2StatComparator implements Comparator<String> {

	private static Eq2StatComparator theInstance = null;

	public static Eq2StatComparator instance() {
		if (theInstance == null) {
			theInstance = new Eq2StatComparator();
		}
		return theInstance;
	}

	private final Properties properties = new Properties();

	private Eq2StatComparator() {
		try {
			properties.load(Eq2StatComparator.class.getResourceAsStream("/eq2stats.order.properties"));
		} catch (IOException ex) {
			Config.LOG.warn("Failed to load ordering info", ex);
		}
	}

	@Override
	public int compare(String item1, String item2) {
		Object obj1 = properties.get(item1);
		Object obj2 = properties.get(item2);

		if (obj1 != null && obj2 != null) {
			int value1 = Integer.parseInt(obj1.toString());
			int value2 = Integer.parseInt(obj2.toString());

			Config.LOG.info(item1 + "=" + value1 + ", " + item2 + "=" + value2);

			if (value1 != value2) {
				info.truewatch.android.questforever.core.Config.LOG.info("Return=" + (value1-value2));
				return value1 - value2;
			}
		}

		return item1.compareTo(item2);
	}

}
