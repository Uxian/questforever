/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.orm;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.orm.annotation.Column;
import info.truewatch.android.questforever.core.orm.annotation.Entity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Orm extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "QuestForever_ORM";

	private static final int CURRENT_VERSION = 2;

	private static final String CLASSES_TABLE = "classList";
	private static final String CLASSNAME_FIELD = "className";

	private final Map<String,EntityData> metaData = new HashMap<String, EntityData>();

	public Orm(final Context context) {
		super(context, DATABASE_NAME, null, CURRENT_VERSION);
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + CLASSES_TABLE + " ("+CLASSNAME_FIELD+" TEXT PRIMARY KEY)");
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
		final Cursor curs = db.query(CLASSES_TABLE, null, null, null, null, null, null);
		while (curs.move(1)) {
			final String className = curs.getString(0);
			constructMetaDataForClass(className);
		}
	}

	private void constructMetaDataForClass(final String className) {
		try {
			final Class<?> clazz = Class.forName(className);
			constructMetaDataForClass(clazz);
		} catch (final Exception ex) {
			Config.LOG.error("Error creating metadata for class " + className, ex);
		}
	}

	void constructMetaDataForExistingTables() {
		final SQLiteDatabase db = getReadableDatabase();
		try {
			final Cursor curs = db.query(CLASSES_TABLE, new String[]{CLASSNAME_FIELD}, null, null, null, null, null);
			while (curs.move(1)) {
				final String className = curs.getColumnName(0);
				Config.LOG.debug("Generating metadata for class " + className);
				constructMetaDataForClass(className);
			}
			curs.close();
		} finally {
			db.close();
		}
	}

	EntityData constructMetaDataForClass(final Class<?> clazz) {
		EntityData entityData = null;
		final Entity entity = clazz.getAnnotation(Entity.class);
		if (entity != null) {
			entityData = new EntityData();
			entityData.tableName = entity.tableName();
			entityData.primaryKey = entity.primaryKey();

			final Method methods[] = clazz.getMethods();
			for (final Method method: methods) {
				if (method.isAnnotationPresent(Column.class)) {
					final Column column = method.getAnnotation(Column.class);
					final ColumnData data = ColumnData.fromAnnotation(method.getName(), column);
					entityData.fields.put(data.columnName, data);
				}
			}
			metaData.put(clazz.getName(), entityData);
		} else {
			Config.LOG.warn("Attempt to generate metadata for class " + clazz + " which isn't an entity");
		}
		return entityData;
	}

	private static Method findMethod(final Class<?> clazz, final String name) {
		for (final Method method: clazz.getMethods()) {
			if (name.equals(method.getName())) {
				return method;
			}
		}
		return null;
	}

	private Object[] getPrimaryKeyConditions(final Object obj) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		final Class<?> clazz = obj.getClass();
		final EntityData data = metaData.get(clazz.getName());

		final Object[] retval = new Object[data.primaryKey.length*2];

		int index = 0;

		for (final String key: data.primaryKey) {
			final ColumnData col = data.fields.get(key);
			final Method m = clazz.getMethod(col.getterName, (Class<?>[])null);
			final Object item = m.invoke(obj, (Object[])null);
			retval[index++] = key;
			retval[index++] = item;
		}
		return retval;
	}

	private static class Where {
		String template;
		String[] values;
	}

	private static Where createWhere(final Object... conditions) {
		final StringBuffer conditionTemplate = new StringBuffer();
		final String[] conditionValues = new String[conditions.length/2];

		final Where retval = new Where();

		for (int n=0; n<conditions.length; n+=2) {
			if (n>0) {
				conditionTemplate.append(" AND ");
			}
			conditionTemplate.append(conditions[n]).append("=?");
			conditionValues[n/2] = conditions[n+1].toString();
		}

		retval.template = conditionTemplate.toString();
		retval.values = conditionValues;
		return retval;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getObjects(final Class<?> clazz, final SQLiteDatabase db, final boolean single, final Object... conditions) throws SecurityException, NoSuchMethodException, IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException {
		final EntityData data = getOrCreateMetaData(clazz, db);

		final Where where = createWhere(conditions);

		final String[] fields = data.getFieldNames();

		final Cursor curs = db.query(data.tableName, fields, where.template, where.values, null, null, null);

		final List<T> retval = new LinkedList<T>();

		try {
			while (curs.move(1)) {
				final T newObject = (T)clazz.newInstance();

				for (int colNumber=0; colNumber<fields.length; ++colNumber) {
					final ColumnData col = data.fields.get(fields[colNumber]);
					final Method m = findMethod(clazz, col.setterName);
					final Class<?> dataType = m.getParameterTypes()[0];

					if (curs.isNull(colNumber)) {
						//Config.LOG.debug("Invoking method " + m + " with null");
						m.invoke(newObject, new Object[]{null});
					} else if (dataType==Long.class || dataType==Long.TYPE) {
						//Config.LOG.debug("Invoking method " + m + " with long");
						m.invoke(newObject, new Object[]{curs.getLong(colNumber)});
					} else if (dataType==Integer.class || dataType==Integer.TYPE) {
						//Config.LOG.debug("Invoking method " + m + " with integer");
						m.invoke(newObject, new Object[]{curs.getInt(colNumber)});
					} else {
						//Config.LOG.debug("Invoking method " + m + " with string");
						m.invoke(newObject, new Object[]{curs.getString(colNumber)});
					}
				}
				retval.add(newObject);
				if (single) {
					break;
				}
			}
		} finally {
			curs.close();
		}

		return retval;
	}

	<T> void deleteObjects(final Class<T> clazz, final SQLiteDatabase db, final Object... conditions) {
		final EntityData data = getOrCreateMetaData(clazz, db);

		final Where where = createWhere(conditions);
		db.delete(data.tableName, where.template, where.values);
	}

	private <T> EntityData getOrCreateMetaData(final Class<T> clazz, final SQLiteDatabase db) {
		EntityData data = metaData.get(clazz.getName());

		if (data == null) {
			data = constructMetaDataForClass(clazz);


			final Cursor curs = db.query("sqlite_master", new String[]{"name"}, "type=? AND name=?", new String[]{"table", data.tableName}, null, null, null);
			final boolean makeTable = !curs.move(1);

			if (makeTable) {
				Config.LOG.info("We need to create a new table for class " + clazz);
				final String sql = getCreationSqlForClass(clazz);
				Config.LOG.info(sql);
				db.execSQL(sql);
			} else {
				Config.LOG.info("Table " + data.tableName + " already exists");
				// TODO: Maybe upgrade table
			}
		}
		return data;
	}

	<T> void deleteObject(final T object, final SQLiteDatabase db) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		getOrCreateMetaData(object.getClass(), db);
		final Object[] conditions = getPrimaryKeyConditions(object);
		deleteObjects(object.getClass(), db, conditions);
	}

	<T> void storeObject(final T obj, final SQLiteDatabase db) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException, InstantiationException {
		final Class<?> clazz = obj.getClass();
		EntityData data = metaData.get(obj.getClass().getName());

		if (data == null) {
			data = constructMetaDataForClass(clazz);
			Config.LOG.info("We need to create a new table for class " + clazz);
			final String sql = getCreationSqlForClass(clazz);
			Config.LOG.info(sql);
			db.execSQL(sql);
		}

		final ContentValues cv = new ContentValues();

		for (final ColumnData col: data.fields.values()) {
			final Method m = clazz.getMethod(col.getterName, (Class<?>[])null);
			final Object item = m.invoke(obj, (Object[])null);

			if (item == null) {
				cv.putNull(col.columnName);
			} else if (item instanceof Long) {
				cv.put(col.columnName, (Long)item);
			} else if (item instanceof Integer) {
				cv.put(col.columnName, (Integer)item);
			} else if (item instanceof Boolean) {
				cv.put(col.columnName, (Boolean)item);
			} else {
				cv.put(col.columnName, item.toString());
			}
		}

		final Object[] conditions = getPrimaryKeyConditions(obj);

		final List<T> items = getObjects(obj.getClass(), db, true, conditions);
		if (items.size() == 1) {
			Config.LOG.debug("Updating existing object");
			final Where where = createWhere(conditions);
			db.update(data.tableName, cv, where.template, where.values);
		} else {
			Config.LOG.debug("Creating new object");
			db.insert(data.tableName, null, cv);
		}
	}

	String getCreationSqlForClass(final Class<?> clazz) {
		final EntityData data = metaData.get(clazz.getName());

		final StringBuffer sql = new StringBuffer("CREATE TABLE ").append(data.tableName).append(" (");
		int counter = 0;
		for (final String columnName: new TreeSet<String>(data.fields.keySet())) {
			if (counter++ > 0) {
				sql.append(", ");
			}
			final ColumnData colData = data.fields.get(columnName);
			sql.append(columnName).append(" ").append(colData.type);
			if (colData.notNull) {
				sql.append(" NOT NULL");
			}
		}
		sql.append(", PRIMARY KEY(");
		counter = 0;
		for (final String keyField: data.primaryKey) {
			if (counter++ > 0) {
				sql.append(", ");
			}
			sql.append(keyField);
		}
		sql.append("))");

		return sql.toString();
	}
}
