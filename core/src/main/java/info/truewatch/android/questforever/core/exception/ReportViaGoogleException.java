package info.truewatch.android.questforever.core.exception;

/**
 * Created by krupagj on 09/08/15.
 */
public class ReportViaGoogleException extends RuntimeException {

    public ReportViaGoogleException(Throwable throwable) {
        super(throwable);
    }
}
