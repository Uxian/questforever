/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.persistence.Item;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by IntelliJ IDEA.
 * User: krupagj
 * Date: 03/03/12
 * Time: 20:28
 * To change this template use File | Settings | File Templates.
 */
public class Eq2ItemLoaderHtml {
    static final String ADORNMENTS_SLOTS = "adornmentslot_list";

    public static String loadItemDetails(Item theItem, HttpSession session) throws IOException, JSONException {
        try {
            String itemUrl = JSONUrlBuilder.itemUrl(theItem.getId(), 1);
            Config.LOG.debug(itemUrl);
            JSONArray matchingItems = session.loadJSonDocument(itemUrl).getJSONArray("item_list");
            JSONObject item = matchingItems.getJSONObject(0);

            StringBuilder html = new StringBuilder();
            html.append("<html><head><title>").append(item.getString("displayname")).append("</title></head>");
            html.append("<body bgcolor=\"black\" style=\"color: white\">");
            html.append("<img style=\"float:left\" src=\"").append(JSONUrlBuilder.itemImageUrl(item.getString("iconid"))).append("\" />");
            html.append("<h2>").append(item.getString("displayname")).append("</h2>");

            JSONObject typeinfo = item.isNull("typeinfo") ? null : item.getJSONObject("typeinfo");
            html.append("<p>");
            if (!item.isNull("tier")) {
                html.append(item.getString("tier")).append(" ");
            }
            if (typeinfo != null && !typeinfo.isNull("wieldstyle")) {
                html.append(typeinfo.getString("wieldstyle")).append(" ");
            }
            html.append(item.getString("type"));
            if (typeinfo != null && !typeinfo.isNull("knowledgedesc")) {
                html.append(" (").append(typeinfo.getString("knowledgedesc")).append(")");
            }
            if (typeinfo != null && !typeinfo.isNull("skill")) {
                html.append(" (").append(typeinfo.getString("skill")).append(")");
            }
            html.append("</p>");

            html.append("<hr style=\"float:none; clear: both\" />");

            if (!item.isNull("slot_list") && item.get("slot_list") instanceof JSONArray) {
                html.append("<h3>Slots</h3>");
                html.append("<ul>");
                JSONArray slots = item.getJSONArray("slot_list");
                for (int n = 0; n < slots.length(); ++n) {
                    String slotName = slots.getJSONObject(n).getString("name");
                    html.append("<li>").append(slotName).append("</li>");
                }
                html.append("</ul>");
            }

            Map<String, String> statDetails = new TreeMap<String, String>();
            if (typeinfo != null && !typeinfo.isNull("damagerating")) {
                statDetails.put("Damage Rating", typeinfo.getString("damagerating"));
            }
            if (typeinfo != null && !typeinfo.isNull("damagetype")) {
                statDetails.put("Damage Type", typeinfo.getString("damagetype"));
            }
            if (typeinfo != null && !typeinfo.isNull("maxdamage")) {
                statDetails.put("Damage", typeinfo.getString("mindamage") + " to " + typeinfo.getString("maxdamage"));
            }
            if (typeinfo != null && !typeinfo.isNull("delay")) {
                statDetails.put("Delay", typeinfo.getString("delay"));
            }
            if (typeinfo != null && !typeinfo.isNull("physicaldamageabsorption")) {
                statDetails.put("Mitigation", typeinfo.getString("physicaldamageabsorption"));
            }
            if (!item.isNull("maxstacksize") && !"1".equals(item.getString("maxstacksize"))) {
                statDetails.put("Max Stack Size", item.getString("maxstacksize"));
            }

            if (!item.isNull("modifiers")) {
                Object modifiers = item.get("modifiers");
                if (modifiers instanceof JSONArray) {
                    handleModifiersArray(html, (JSONArray) modifiers);
                } else if (modifiers instanceof JSONObject) {
                    handleModifiersObject(html, (JSONObject) modifiers);
                }
            }

            if (!statDetails.isEmpty()) {
                html.append("<h3>Stats</h3>").append("<ul>");
                for (String stat : statDetails.keySet()) {
                    html.append("<li>").append(stat).append(": ").append(statDetails.get(stat)).append("</li>");
                }
                html.append("</ul>");
            }

            if (!item.isNull("effect_list")) {
                html.append("<h3>Effects</h3>");
                html.append("<ul>");
                JSONArray effects = item.getJSONArray("effect_list");
                for (int n = 0; n < effects.length(); ++n) {
                    String effect = effects.getJSONObject(n).getString("description");
                    int indentation = Integer.parseInt(effects.getJSONObject(n).getString("indentation"));
                    for (int x = 0; x < indentation; ++x) {
                        html.append("<ul>");
                    }
                    html.append("<li>").append(effect).append("</li>");
                    for (int x = 0; x < indentation; ++x) {
                        html.append("</ul>");
                    }
                }
                html.append("</ul>");
            }


            if (typeinfo != null && !typeinfo.isNull("classes")) {
                html.append("<h3>Usable by</h3>");
                html.append("<ul>");
                Object classesAnon = typeinfo.get("classes");
                if (classesAnon instanceof JSONObject) {
                    JSONObject classes = (JSONObject)classesAnon;
                    JSONArray allClasses = classes.names();
                    for (int n = 0; n < allClasses.length(); ++n) {
                        String className = allClasses.getString(n);
                        JSONObject details = classes.getJSONObject(className);
                        html.append("<li>").append(className).append(" (level ").append(details.getString("level")).append(")</li>");
                    }
                } else if (classesAnon instanceof JSONArray) {
                    JSONArray allClasses = (JSONArray) classesAnon;
                    for (int n = 0; n < allClasses.length(); ++n) {
                        JSONObject details = allClasses.getJSONObject(n);
                        html.append("<li>").append(details.getString("displayname")).append(" (level ").append(details.getString("level")).append(")</li>");
                    }
                }
                html.append("</ul>");
            }

            if (!item.isNull(ADORNMENTS_SLOTS)) {
                html.append("<h3>Adornment Slots</h3>");
                html.append("<ul>");
                Object slots = item.get(ADORNMENTS_SLOTS);
                if (slots instanceof JSONArray) {
                    JSONArray adornmentsslots = (JSONArray) slots;
                    for (int n = 0; n < adornmentsslots.length(); ++n) {
                        JSONObject adornmentslot = adornmentsslots.getJSONObject(n);
                        html.append("<li>").append(adornmentslot.getString("color")).append("</li>");
                    }
                }

                html.append("</ul>");
            }

            if (typeinfo != null && !typeinfo.isNull("recipe_list")) {
                html.append("<h3>Recipes</h3>").append("<ul>");
                JSONArray recipes = typeinfo.getJSONArray("recipe_list");
                for (int n = 0; n < recipes.length(); ++n) {
                    html.append("<li>").append(recipes.getJSONObject(n).getString("name")).append("</li>");
                }
                html.append("</ul>");
            }

            html.append("</body></html>");

            return html.toString();
        } catch (JSONException ex) {
            Config.LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    private static void handleModifiersObject(StringBuilder html, JSONObject modifiers) throws JSONException {
        JSONArray modnames = modifiers.names();
        if (modnames != null) {
            html.append("<h3>Modifiers</h3>");
            html.append("<ul>");
            for (int n = 0; n < modnames.length(); ++n) {
                Object modifierObject = modifiers.get(modnames.getString(n));
                if (modifierObject instanceof JSONObject) {
                    html.append("<li>");
                    JSONObject modifier = ((JSONObject) modifiers).getJSONObject(modnames.getString(n));
                    String modType = modifier.getString("type");
                    if ("modifyproperty".equals(modType)) {
                        String description;
                        if (modifier.has("desc")) {
                            description = modifier.getString("desc");
                        } else if (modifier.has("displayname")) {
                            description = modifier.getString("displayname");
                        } else {
                            description = "???";
                        }
                        html.append(StatTransformer.translateStat(description)).append(": ");
                        html.append(modifier.getString("value"));
                    } else {
                        html.append(StatTransformer.translateStat(modnames.getString(n))).append(": ");
                        html.append(modifier.getString("value"));
                    }
                    html.append("</li>");
                } else if (modifierObject instanceof JSONArray) {
                    JSONArray modSubArray = (JSONArray) modifierObject;
                    for (int m = 0; m < modSubArray.length(); ++m) {
                        html.append("<li>");
                        JSONObject modItem = modSubArray.getJSONObject(m);
                        html.append(StatTransformer.translateStat(modItem.getString("displayname"))).append(": ");
                        html.append(modItem.getString("value"));
                        html.append("</li>");
                    }
                }
            }
            html.append("</ul>");
        }
    }

    private static void handleModifiersArray(StringBuilder html, JSONArray modifiers) throws JSONException {
        if (modifiers.length() > 0) {
            html.append("<h3>Modifiers</h3>");
            html.append("<ul>");
            for (int m = 0; m < modifiers.length(); ++m) {
                html.append("<li>");
                JSONObject modItem = modifiers.getJSONObject(m);
                html.append(StatTransformer.translateStat(modItem.getString("displayname"))).append(": ");
                html.append(modItem.getString("value"));
                html.append("</li>");
            }
            html.append("</ul>");
        }
    }
}
