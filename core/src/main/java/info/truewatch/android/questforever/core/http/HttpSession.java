/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.http;

import android.content.Context;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

public class HttpSession {

	private ProgressNotification notifier = null;
	private String progressPrefix = null;

	private final HttpClient client;

	public HttpSession(final Context context) {
		client = Config.createHttpClient(context);
	}

    public JSONObject loadJSonDocument(final String url) throws IOException, JSONException {
        notifyProgress("Connecting");
        final HttpGet get = new HttpGet(url);
        final HttpResponse resp = client.execute(get);

        Config.LOG.info("Got response code: " + resp.getStatusLine().getStatusCode());
        notifyProgress("Reading data");

        if (resp.getEntity() != null) {
            try {
                final HttpEntity entity = resp.getEntity();
                Config.LOG.debug("Size: " + entity.getContentLength());
                final InputStream is = entity.getContent();
                String data = readFully(is);
                Config.LOG.debug(data);
                JSONTokener jsonTokener = new JSONTokener(data);
                Object something = jsonTokener.nextValue();
                if (something instanceof JSONObject) {
                    return (JSONObject) something;
                } else {
                    throw new FileNotFoundException("" + something);
                }
            } finally {
                resp.getEntity().consumeContent();
            }
        }

        throw new FileNotFoundException(resp.getStatusLine().getReasonPhrase());        
    }
    
    private String readFully(InputStream is) throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String data;
        while ((data = reader.readLine()) != null) {
            builder.append(data);
        }
        return builder.toString();
    }

	private void notifyProgress(final String update) {
		if (notifier != null) {
			if (progressPrefix == null) {
				notifier.notifyProgress(false, 0, 0, update, this);
			} else {
				notifier.notifyProgress(false, 0, 0, progressPrefix + ": " + update, this);
			}
		}
	}

	public void setNotifier(final ProgressNotification notifier) {
		this.notifier = notifier;
	}

	public void setProgressPrefix(final String progressPrefix) {
		this.progressPrefix = progressPrefix;
	}
}
