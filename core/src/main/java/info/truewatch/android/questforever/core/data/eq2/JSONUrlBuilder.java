/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import java.net.URLEncoder;

/**
 * Created by IntelliJ IDEA.
 * User: krupagj
 * Date: 15/02/12
 * Time: 21:08
 * To change this template use File | Settings | File Templates.
 */
public class JSONUrlBuilder {
    private static final String URL_BASE = "http://census.daybreakgames.com/s:questforever/";
    private static final String IMG_BASE = URL_BASE + "img/eq2/";
    private static final String JSON_BASE = URL_BASE + "json/";
    private static final String GET_URL_BASE = JSON_BASE + "get/eq2/";
    private static final String SERVER_URL = JSON_BASE + "status/eq2/";
    
    public static final String PARAM_SHOW = "c:show";
    public static final String PARAM_SORT = "c:sort";
    public static final String PARAM_ATTACHMENTS = "c:attachments";
    public static final String PARAM_RESOLVE = "c:resolve";

    public static String characterUrl(Object id, int limit, String... params) {
        return getUrl("character", id, limit, params);
    }

    public static String itemUrl(Object id, int limit, String... params) {
        return getUrl("item", id, limit, params);
    }
    
    public static String itemImageUrl(Object id) {
        return String.format(IMG_BASE + "icons/%s/item/", id);
    }

    public static String characterImageUrl(Object id, String type) {
        return String.format(IMG_BASE + "character/%s/%s", id, type);
    }
    
    public static String guildUrl(Object id, int limit, String... params) {
        return getUrl("guild", id, limit, params);
    }

    public static String factionUrl(Object id, int limit, String... params) {
        return getUrl("faction", id, limit, params);
    }

    public static String serverUrl() {
        return SERVER_URL;
    }

    private static String getUrl(String searchItem, Object id, int limit, String... params) {
        StringBuilder urlBuilder = new StringBuilder(GET_URL_BASE);
        urlBuilder.append(searchItem).append("/");
        if (id != null) {
            urlBuilder.append(URLEncoder.encode(id.toString()));
        }
        urlBuilder.append("?c:limit=").append(limit);

        for (int n=0; n<params.length-1; n += 2) {
            urlBuilder.append('&');
            urlBuilder.append(URLEncoder.encode(params[n])).append('=').append(URLEncoder.encode(params[n+1]));
        }
        return urlBuilder.toString();
    }
}
