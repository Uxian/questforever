/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import android.content.Context;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.OrmPersistence;
import info.truewatch.android.questforever.core.persistence.GuildMember;
import info.truewatch.android.questforever.core.data.GuildRosterParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Eq2GuildCsvParser implements Runnable, GuildRosterParser {

    private info.truewatch.android.questforever.core.ProgressNotification notifier;

    private final long guildId;
    private final Context context;

    public Eq2GuildCsvParser(final Context ctx, final long id) {
        guildId = id;
        this.context = ctx;
    }

    @Override
    public void run() {
        try {
            loadCSV();
        } catch (final Exception ex) {
            notifyError(ex);
        }
    }

    @Override
    public void loadCSV() throws IOException, JSONException, LegacyGuildException {
        String guildUrl = JSONUrlBuilder.guildUrl(guildId, 1, JSONUrlBuilder.PARAM_SHOW, "rank_list");
        Config.LOG.debug(guildUrl);

        String peopleUrl = JSONUrlBuilder.characterUrl(null, 1000,
                JSONUrlBuilder.PARAM_SHOW, "id,name,guild.rank,type,tradeskills",
                JSONUrlBuilder.PARAM_SORT, "displayname",
                "guild.id", "" + guildId);
        Config.LOG.debug(peopleUrl);

        final HttpSession session = new HttpSession(context);
        notifyProgress(false, 0, 100, "Connecting");

        try {
            Map<String, String> rankMap = new HashMap<String, String>();
            JSONArray guilds = session.loadJSonDocument(guildUrl).getJSONArray("guild_list");
            if (guilds.length() < 1) {
                throw new LegacyGuildException("Guild not found");
            }
            JSONArray ranks = guilds.getJSONObject(0).getJSONArray("rank_list");
            for (int n = 0; n < ranks.length(); ++n) {
                JSONObject rank = ranks.getJSONObject(n);
                rankMap.put(rank.getString("id"), rank.getString("name"));
            }


            JSONArray characters = session.loadJSonDocument(peopleUrl).getJSONArray("character_list");
            for (int n = 0; n < characters.length(); ++n) {
                JSONObject toon = characters.getJSONObject(n);
                GuildMember person = new GuildMember();

                JSONObject nameObject = toon.getJSONObject("name");
                StringBuilder name = new StringBuilder(nameObject.getString("first"));
                if (!nameObject.isNull("last") && !"".equals(nameObject.getString("last"))) {
                    name.append(" ").append(nameObject.getString("last"));
                }

                person.setName(name.toString());
                notifyProgress(false, n, characters.length(), "Reading data (" + person.getName() + ")");

                person.setId(Long.parseLong(toon.getString("id")));
                person.setGuildId(guildId);
                if (!toon.isNull("guild")) {
                    person.setRank(rankMap.get(toon.getJSONObject("guild").getString("rank")));
                } else {
                    person.setRank("?");
                }
                person.setAdventureLevel(Integer.parseInt(toon.getJSONObject("type").getString("level")));
                person.setAdventureClass(toon.getJSONObject("type").getString("class"));
                JSONObject ts = toon.getJSONObject("tradeskills");
                if (ts != null) {
                    JSONArray names = ts.names();
                    if (names.length() > 0) {
                        person.setArtisanClass(names.getString(0));
                        person.setArtisanLevel(parseInt(ts.getJSONObject(names.getString(0)).getString("level")));
                    }
                }
                person.setGame(GameConstants.EVERQUEST2);

                OrmPersistence.store(context, person);
            }
            notifyProgress(true, 0, 0, null);
        } catch (JSONException ex) {
            info.truewatch.android.questforever.core.Config.LOG.error(ex.getMessage(), ex);
            throw ex;
        }


    }

    private int parseInt(final String strValue) {
        int value;
        try {
            value = Integer.parseInt(strValue);
        } catch (final NumberFormatException ex) {
            info.truewatch.android.questforever.core.Config.LOG.warn("Failed to parse \"" + strValue + "\", " + ex.getMessage());
            value = -2;
        }
        return value;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.GuildRosterParser#setNotifier(info.truewatch.android.questforever.core.ProgressNotification)
      */
    @Override
    public void setNotifier(final info.truewatch.android.questforever.core.ProgressNotification notifier) {
        this.notifier = notifier;
    }

    private void notifyError(final Throwable problem) {
        if (notifier != null) {
            notifier.notifyError(problem, this);
        }
    }

    private void notifyProgress(final boolean completed, final int currentProgress,
                                final int maxProgress, final String progressText) {
        if (notifier != null) {
            notifier.notifyProgress(completed, currentProgress, maxProgress,
                    progressText, this);
        }
    }
}
