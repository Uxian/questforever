/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.persistence;

import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.orm.annotation.Column;
import info.truewatch.android.questforever.core.orm.annotation.Entity;

@Entity(tableName="item", primaryKey={"id", "game"})
public class Item {

	private int game;
	private String name;
	private long id;

	@Column(name="game", type="INTEGER", version=1, notNull=true)
	public int getGame() {
		return game;
	}
	public void setGame(int game) {
		this.game = game;
	}

	@Column(name="name", notNull=true, version=1)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="id", notNull=true, type="LONG", version=1)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getDetails() {
		return GameConstants.getName(game);
	}

    @Override
    public String toString() {
        return name;
    }
}
