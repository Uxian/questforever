/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import android.content.Context;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.persistence.Server;
import info.truewatch.android.questforever.core.data.ServerSearcher;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Eq2ServerSearcher implements Runnable, ServerSearcher {

    private info.truewatch.android.questforever.core.ProgressNotification notifier;
    private final String searchString;
    private List<Server> results;

    private final Context context;

    public Eq2ServerSearcher(final Context context, final String searchString) {
        this.searchString = searchString;
        this.context = context;
    }

    @Override
    public void run() {
        try {
            results = findServers();
            notifyProgress(true, 3, 3, "Finished");
        } catch (final Exception e) {
            notifyError(e);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.GuildSearcher#setNotifier(info.truewatch.android.questforever.core.ProgressNotification)
      */
    @Override
    public void setNotifier(final info.truewatch.android.questforever.core.ProgressNotification notifier) {
        this.notifier = notifier;
    }

    private void notifyError(final Throwable problem) {
        if (notifier != null) {
            notifier.notifyError(problem, this);
        }
    }

    private void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText) {
        if (notifier != null) {
            notifier.notifyProgress(completed, currentProgress, maxProgress,
                    progressText, this);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.GuildSearcher#findGuilds()
      */
    @Override
    public List<Server> findServers() throws IOException, URISyntaxException, JSONException {

        Config.LOG.info("Starting search");

        final LinkedList<Server> retval = new LinkedList<Server>();
        String searchUrl = JSONUrlBuilder.serverUrl();

        final HttpSession session = new HttpSession(context);

        notifyProgress(false, 1, 2, "Reading data");

        JSONObject root = session.loadJSonDocument(searchUrl).getJSONObject("eq2");

        try {
            JSONArray categories = root.names();

            for (int catIndex=0; catIndex< categories.length(); ++catIndex) {
                String category = categories.getString(catIndex);
                JSONObject servers = root.getJSONObject(category);

                JSONArray serverList = servers.names();
                for (int serverIndex = 0; serverIndex < serverList.length(); ++serverIndex) {
                    String serverName = serverList.getString(serverIndex);
                    JSONObject server = servers.getJSONObject(serverName);

                    Server entity = new Server();
                    entity.setCategory(category);
                    entity.setGame(GameConstants.EVERQUEST2);
                    entity.setName(serverName);
                    entity.setStatus(server.getString("status"));
                    entity.setAge(server.getString("age"));

                    if (entity.getName().toLowerCase().contains(searchString.toLowerCase())) {
                        retval.add(entity);
                    }
                }
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage());
        }

        Collections.sort(retval, new Comparator<Server>() {
            @Override
            public int compare(Server server1, Server server2) {
                int comparison = server1.getName().compareToIgnoreCase(server2.getName());
                return comparison;
            }
        });

        return retval;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.GuildSearcher#getResults()
      */
    @Override
    public List<Server> getResults() {
        return results;
    }

}
