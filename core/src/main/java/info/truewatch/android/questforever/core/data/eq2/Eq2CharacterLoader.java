/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import info.truewatch.android.questforever.core.BitmapLoader;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.persistence.Toon;
import info.truewatch.android.questforever.core.data.CharacterLoader;
import info.truewatch.android.questforever.core.data.ValueField;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Eq2CharacterLoader implements Runnable, CharacterLoader {

    private static final int ATTR_SECTION_CLASS = 0;
    public static final String ATTR_SECTION_CLASS_NAME = "Class/Level:";
    private static final int ATTR_SECTION_MISC = 1;
    public static final String ATTR_SECTION_MISC_NAME = "Misc:";


    private static final int STAT_SECTION_ENERGY = 0;
    private static final int STAT_SECTION_BASIC = 1;
    private static final int STAT_SECTION_COMBAT = 2;
    private static final int STAT_SECTION_TRADESKILL = 3;
    private static final int STAT_SECTION_MISC = 4;
    private static final int STAT_SECTION_SKILLS = 5;
    private static final int STAT_SECTION_LANGUAGES = 6;
    private static final int STAT_SECTION_FACTIONS =7;


    public static final String STAT_SECTION_ENERGY_NAME = "Energy:";
    public static final String STAT_SECTION_SKILLS_NAME = "Skills:";
    public static final String STAT_SECTION_BASIC_NAME = "Basic:";
    public static final String STAT_SECTION_COMBAT_NAME = "Combat:";
    public static final String STAT_SECTION_TRADESKILL_NAME = "Tradeskill:";
    public static final String STAT_SECTION_MISC_NAME = "Misc:";
    public static final String STAT_SECTION_LANGUAGES_NAME = "Languages:";
    public static final String STAT_SECTION_FACTIONS_NAME = "Factions:";

    private ProgressNotification notifier;

    private final Toon theCharacter;
    private Collection<ValueField> attributes = new HashSet<ValueField>();
    private Collection<ValueField> statistics = new HashSet<ValueField>();
    private String biography = "";
    private Bitmap paperDoll = null;
    private Bitmap portrait;

    private static HashMap<Integer,String> factionLookup = new HashMap<Integer, String>();

    List<Bitmap> equipmentImages = new ArrayList<Bitmap>(24);
    List<String> equipmentNames = new ArrayList<String>(24);
    List<Long> equipmentIds = new ArrayList<Long>(24);
    List<Toon> alts = new ArrayList<Toon>(24);

    private static final int FAKE_MAX_VALUE = 7;

    private final Context context;

    public Eq2CharacterLoader(final Context context, final Toon theCharacter) {
        this.theCharacter = theCharacter;
        this.context = context;
    }

    @Override
    public void run() {
        try {
            loadCharacter();
        } catch (final Exception e) {
            Config.LOG.error("Error parsing", e);
            notifyError(e);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#setNotifier(info.truewatch.android.questforever.core.ProgressNotification)
      */
    @Override
    public void setNotifier(final ProgressNotification notifier) {
        this.notifier = notifier;
    }

    private void notifyError(final Throwable problem) {
        if (notifier != null) {
            notifier.notifyError(problem, this);
        }
    }

    private void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText) {
        if (notifier != null) {
            notifier.notifyProgress(completed, currentProgress, maxProgress,
                    progressText, this);
        }
    }

    private String lookupFaction(HttpSession session, int id) throws IOException, JSONException {
        if (!factionLookup.containsKey(id)) {
            String factionListUrl = JSONUrlBuilder.factionUrl(id, 1);
            JSONArray factions = session.loadJSonDocument(factionListUrl).getJSONArray("faction_list");
            for (int n=0; n<factions.length(); ++n) {
                JSONObject faction = factions.getJSONObject(n);
                if (faction.has("id") && faction.has("name")) {
                    factionLookup.put(faction.getInt("id"), faction.getString("name"));
                }
            }
        }
        return factionLookup.get(id);
    }

    @SuppressWarnings("unchecked")
    private void loadCharacter() throws IOException, URISyntaxException {
        notifyProgress(false, INTERMEDIATE_UPDATE, FAKE_MAX_VALUE, "Reading");
        String peopleUrl = JSONUrlBuilder.characterUrl(theCharacter.getId(), 1, JSONUrlBuilder.PARAM_ATTACHMENTS, "all");
        String factionURL = JSONUrlBuilder.characterUrl(theCharacter.getId(),0,JSONUrlBuilder.PARAM_SHOW, "faction_list",
                JSONUrlBuilder.PARAM_RESOLVE, "factions(name,value)");
        Config.LOG.debug(peopleUrl);
        Config.LOG.debug(factionURL);

        final HttpSession session = new HttpSession(context);
        notifyProgress(false, 0, 100, "Connecting");

        try {
            notifyProgress(false, 0, 2, "Connecting");
            JSONArray characters = session.loadJSonDocument(peopleUrl).getJSONArray("character_list");
            JSONArray faction = session.loadJSonDocument(factionURL).getJSONArray("character_list");
            if (characters.length() == 0) {
                throw new FileNotFoundException("Character not found");
            } else {
                JSONObject foundToon = characters.getJSONObject(0);
                JSONObject foundFaction = faction.getJSONObject(0);
                JSONObject nameObject = foundToon.getJSONObject("name");
                StringBuilder name = new StringBuilder(nameObject.getString("first"));
                if (!nameObject.isNull("last") && !"".equals(nameObject.getString("last"))) {
                    name.append(" ").append(nameObject.getString("last"));
                }

                notifyProgress(false, INTERMEDIATE_UPDATE, FAKE_MAX_VALUE, "Basic");

                if (!foundToon.isNull("type")) {
                    JSONObject type = foundToon.getJSONObject("type");
                    addSingleItem("level", "Adventure Level", ATTR_SECTION_CLASS, ATTR_SECTION_CLASS_NAME, type, attributes);
                    addSingleItem("class", "Adventure Class", ATTR_SECTION_CLASS, ATTR_SECTION_CLASS_NAME, type, attributes);
                    addSingleItem("deity", "Deity", ATTR_SECTION_MISC, ATTR_SECTION_MISC_NAME, type, attributes);
                    addSingleItem("gender", "Gender", ATTR_SECTION_MISC, ATTR_SECTION_MISC_NAME, type, attributes);
                    addSingleItem("race", "Race", ATTR_SECTION_MISC, ATTR_SECTION_MISC_NAME, type, attributes);

                    long birthTimestamp = Long.parseLong(type.getString("birthdate_utc")) * 1000;
                    Date birthDate = new Date(birthTimestamp);
                    DateFormat df = new SimpleDateFormat("dd MMM, yyyy");
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                    attributes.add(new ValueField("Creation Date", df.format(birthDate), ATTR_SECTION_MISC, ATTR_SECTION_MISC_NAME));
                }

                if (!foundToon.isNull("tradeskills")) {
                    JSONObject ts = foundToon.getJSONObject("tradeskills");
                    addSingleItem("level", "Tradeskill Level", ATTR_SECTION_CLASS, ATTR_SECTION_CLASS_NAME, ts, attributes);
                    addSingleItem("class", "Tradeskill Class", ATTR_SECTION_CLASS, ATTR_SECTION_CLASS_NAME, ts, attributes);
                }

                addNestedItem(new String[]{"quests", "complete"}, "Quests Completed", ATTR_SECTION_MISC, ATTR_SECTION_MISC_NAME, foundToon, attributes);
                addNestedItem(new String[]{"quests", "active"}, "Quests Active", ATTR_SECTION_MISC, ATTR_SECTION_MISC_NAME, foundToon, attributes);

                notifyProgress(false, ATTRIBUTES_AVAILABLE, FAKE_MAX_VALUE, "Bio");

                Object bioHolder = foundToon.get("bio");
                if (bioHolder instanceof String) {
                    biography = Html.fromHtml((String) bioHolder).toString().replace("<br>", "\n");
                }

                notifyProgress(false, BIO_AVAILABLE, FAKE_MAX_VALUE, "Stats");

                JSONObject stats = foundToon.getJSONObject("stats");
                JSONArray statNames = stats.names();
                for (int n = 0; n < statNames.length(); ++n) {
                    String statName = statNames.getString(n);
                    Object obj = stats.get(statName);
                    String statValue = null;
                    if (obj instanceof JSONObject) {
                        if (!((JSONObject) obj).isNull("effective")) {
                            statValue = stats.getJSONObject(statName).getString("effective");
                            ValueField valueField = new ValueField(StatTransformer.translateStat(statName), statValue,
                                    STAT_SECTION_BASIC, STAT_SECTION_BASIC_NAME);
                            statistics.add(valueField);
                        }
                        if (!((JSONObject) obj).isNull("max")) {
                            statValue = stats.getJSONObject(statName).getString("max");
                            ValueField valueField = new ValueField(StatTransformer.translateStat(statName), statValue,
                                    STAT_SECTION_ENERGY, STAT_SECTION_ENERGY_NAME);
                            statistics.add(valueField);
                        }
                    } else if (obj instanceof Number) {
                        statValue = obj.toString();
                        ValueField valueField = new ValueField(StatTransformer.translateStat(statName), statValue,
                                STAT_SECTION_MISC, STAT_SECTION_MISC_NAME);
                        statistics.add(valueField);
                    }
                }

                addStatNameBlock("tradeskill", STAT_SECTION_TRADESKILL, STAT_SECTION_TRADESKILL_NAME, stats, statistics);
                addStatNameBlock("weapon", STAT_SECTION_COMBAT, STAT_SECTION_COMBAT_NAME, stats, statistics);
                addStatNameBlock("defense", STAT_SECTION_COMBAT, STAT_SECTION_COMBAT_NAME, stats, statistics);
                addStatNameBlock("combat", STAT_SECTION_COMBAT, STAT_SECTION_COMBAT_NAME, stats, statistics);
                addStatNameBlock("ability", STAT_SECTION_COMBAT, STAT_SECTION_COMBAT_NAME, stats, statistics);

                Object skillsAnon = foundToon.get("skills");
                if (skillsAnon instanceof JSONArray) {
                    JSONArray skills = (JSONArray) skillsAnon;
                    for (int n = 0; n < skills.length(); ++n) {
                        JSONObject skill = skills.getJSONObject(n);
                        String maxValue = null;
                        if (!skill.isNull("maxvalue")) {
                            maxValue = skill.getString("maxvalue");
                        }
                        String earnedvalue = skill.getString("earnedvalue");
                        if (!"0".equals(maxValue) && !"0".equals(earnedvalue)) {
                            ValueField valueField = new ValueField(StatTransformer.translateStat(skill.getString("name")),
                                    earnedvalue, STAT_SECTION_SKILLS, STAT_SECTION_SKILLS_NAME);
                            valueField.setMaxValue(maxValue);
                            statistics.add(valueField);
                        }
                    }
                } else if (skillsAnon instanceof JSONObject) {
                    JSONObject skills = (JSONObject) skillsAnon;
                    JSONArray names = skills.names();
                    for (int n = 0; n < names.length(); ++n) {
                        String skillName = names.getString(n);
                        JSONObject skill = skills.getJSONObject(skillName);
                        String maxValue = null;
                        if (!skill.isNull("maxvalue")) {
                            maxValue = skill.getString("maxvalue");
                        }
                        String earnedvalue = skill.getString("earnedvalue");
                        if (!"0".equals(maxValue) && !"0".equals(earnedvalue)) {
                            ValueField valueField = new ValueField(StatTransformer.translateStat(skillName),
                                    earnedvalue, STAT_SECTION_SKILLS, STAT_SECTION_SKILLS_NAME);
                            valueField.setMaxValue(maxValue);
                            statistics.add(valueField);
                        }
                    }
                }

                Object langObject = foundToon.get("language_list");
                if (langObject instanceof JSONArray) {
                    JSONArray languages = (JSONArray) langObject;
                    for (int n = 0; n < languages.length(); ++n) {
                        ValueField language = new ValueField(languages.getJSONObject(n).getString("name"), "",
                                STAT_SECTION_LANGUAGES, STAT_SECTION_LANGUAGES_NAME);
                        statistics.add(language);
                    }
                }
                notifyProgress(false, BIO_AVAILABLE, FAKE_MAX_VALUE, "Factions");
                Object factionObject = foundFaction.get("faction_list");
                if (factionObject instanceof JSONArray) {
                    JSONArray factions = (JSONArray) factionObject;
                    for (int n = 0; n < factions.length(); ++n) {
                        //get the faction name

                        final JSONObject factionBase = factions.getJSONObject(n);
                        if (factionBase.has("value")) {
                            String factionName = null;
                            if (factionBase.has("name")) {
                                factionName = factionBase.getString("name");
                                // Cache me if you can!
                                factionLookup.put(factionBase.getInt("id"), factionName);
                            } else if (factionBase.has("id")) {
                                Config.LOG.error("Missing name in faction entry: " + factionBase.toString(2));
                                factionName = lookupFaction(session, factionBase.getInt("id"));
                            } else {
                                Config.LOG.error("Invalid faction entry: " + factionBase.toString(2));
                            }

                            if (factionName != null) {
                                ValueField Faction = new ValueField(factionName, factionBase.getString("value"),
                                        STAT_SECTION_FACTIONS, STAT_SECTION_FACTIONS_NAME);
                                statistics.add(Faction);
                            }
                        } else {
                            Config.LOG.error("Error in Faction data: " + factionBase.toString(2));
                        }
                    }
                }


                notifyProgress(false, STATS_AVAILABLE, FAKE_MAX_VALUE, "Paperdoll");

                final String headShotUrl = JSONUrlBuilder.characterImageUrl(theCharacter.getId(), "headshot");
                final String paperDollUrl = JSONUrlBuilder.characterImageUrl(theCharacter.getId(), "paperdoll");

                portrait = BitmapLoader.load(context, headShotUrl);
                paperDoll = BitmapLoader.load(context, paperDollUrl);

                notifyProgress(false, PAPERDOLL_AVAILABLE, FAKE_MAX_VALUE, "Alts");

                if (!foundToon.isNull("account")) {
                    JSONObject account = foundToon.getJSONObject("account");
                    if (!account.isNull("link_id")) {
                        String accountLink = account.getString("link_id");
                        extractAlts(accountLink, session);
                    }
                }

                notifyProgress(false, ALTS_AVAILABLE, FAKE_MAX_VALUE, "Equipment 1");

                int itemCount = 1;
                JSONArray equipment = foundToon.getJSONArray("equipmentslot_list");
                final int totalItems = equipment.length();
                for (int n = 0; n < equipment.length(); ++n) {
                    JSONObject slot = equipment.getJSONObject(n);
                    Config.LOG.debug(slot.toString());
                    if (!slot.isNull("item")) {
                        JSONObject item = slot.getJSONObject("item");

                        String itemUrl = JSONUrlBuilder.itemUrl(item.getString("id"), 1,
                                JSONUrlBuilder.PARAM_SHOW, "displayname,iconid");
                        Config.LOG.debug(itemUrl);
                        JSONArray matchedItems = session.loadJSonDocument(itemUrl).getJSONArray("item_list");
                        if (matchedItems != null && matchedItems.length() > 0) {
                            JSONObject itemResult = matchedItems.getJSONObject(0);

                            equipmentNames.add(itemResult.getString("displayname"));
                            equipmentIds.add(Long.parseLong(item.getString("id")));
                            String imageUrl = JSONUrlBuilder.itemImageUrl(itemResult.getString("iconid"));
                            equipmentImages.add(BitmapLoader.load(context, imageUrl));
                        }
                    }
                    notifyProgress(false, EQUIPMENT_AVAILABLE, FAKE_MAX_VALUE, "Equipment " + (++itemCount) + "/" + totalItems);
                }
            }

            notifyProgress(true, COMPLETED, FAKE_MAX_VALUE, "Finished");
        } catch (JSONException ex) {
            throw new IllegalArgumentException(ex.getMessage(), ex);
        }

    }

    private void addSingleItem(String statName, String printName, int sectionId, String sectionName, JSONObject baseObject,
                               Collection<ValueField> collection) throws JSONException {
        if (!baseObject.isNull(statName)) {
            String value = baseObject.getString(statName);
            collection.add(new ValueField(printName, value, sectionId, sectionName));
        }
    }

    private void addNestedItem(String[] names, String printName, int sectionId, String sectionName, JSONObject baseObject,
                               Collection<ValueField> collection) throws JSONException {
        JSONObject item = baseObject;

        for (int n=0; n<names.length-1; ++n) {
            if (item.isNull(names[n])) {
                return;
            } else {
                item = item.getJSONObject(names[n]);
            }
        }

        if (!item.isNull(names[names.length-1])) {
            String value = item.getString(names[names.length-1]);
            collection.add(new ValueField(printName, value, sectionId, sectionName));
        }
    }


    private void addStatNameBlock(String sectionName, int sectionId, String sectionHeading, JSONObject stats,
                                  Collection<ValueField> collection) throws JSONException {
        if (!stats.isNull(sectionName)) {
            JSONObject trades = stats.getJSONObject(sectionName);
            JSONArray tradeArray = trades.names();
            for (int n = 0; n < tradeArray.length(); ++n) {
                String tradeName = tradeArray.getString(n);
                String value = trades.getString(tradeName);
                if (!"-1".equals(value) && !"-1.0".equals(value) && !"0".equals(value) && !"0.0".equals(value)) {
                    ValueField valueField = new ValueField(StatTransformer.translateStat(tradeName), value,
                            sectionId, sectionHeading);
                    collection.add(valueField);
                }
            }
        }
    }

    private void extractAlts(String accountLink, HttpSession session) throws IOException, JSONException {
        String characterListUrl = JSONUrlBuilder.characterUrl(null, 100,
                "account.link_id", accountLink,
                JSONUrlBuilder.PARAM_SHOW, Eq2CharacterSearcher.SEARCH_TOON_FIELDS,
                JSONUrlBuilder.PARAM_SORT, "displayname");
        JSONArray characters = session.loadJSonDocument(characterListUrl).getJSONArray("character_list");
        for (int n = 0; n < characters.length(); ++n) {
            JSONObject foundToon = characters.getJSONObject(n);
            Toon alt = Eq2CharacterSearcher.jsonToSearchToon(foundToon);
            if (alt.getId() != theCharacter.getId()) {
                alts.add(alt);
            }
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getEquipmentCount()
      */
    @Override
    public int getEquipmentCount() {
        return equipmentImages.size();
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getEquipmentImage(int)
      */
    @Override
    public Bitmap getEquipmentImage(final int index) {
        if (index < equipmentImages.size()) {
            return equipmentImages.get(index);
        } else {
            return null;
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getEquipmentName(int)
      */
    @Override
    public String getEquipmentName(final int slot) {
        return equipmentNames.get(slot);
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getEquipmentId(int)
      */
    @Override
    public long getEquipmentId(final int slot) {
        return equipmentIds.get(slot);
    }


    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getAlts()
      */
    @Override
    public List<Toon> getAlts() {
        return alts;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getAttributes()
      */
    @Override
    public Collection<ValueField> getAttributes() {
        List<ValueField> retval = new ArrayList<ValueField>(attributes);
        Collections.sort(retval);
        return retval;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getStats()
      */
    @Override
    public Collection<ValueField> getStats() {
        List<ValueField> retval = new ArrayList<ValueField>(statistics);
        Collections.sort(retval);
        return retval;
    }
    @Override
    public Collection<ValueField> getFactions() {
        List<ValueField> retval = new ArrayList<ValueField>(statistics);
        Collections.sort(retval);
        return retval;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getBiography()
      */
    @Override
    public String getBiography() {
        return biography;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#getPaperDoll()
      */
    @Override
    public Bitmap getPaperDoll() {
        return paperDoll;
    }

    public Bitmap getPortrait() {
        return portrait;
    }
}
