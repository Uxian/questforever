package info.truewatch.android.questforever.core.data.eq2;

/**
 * Created by IntelliJ IDEA.
 * User: krupagj
 * Date: 24/12/11
 * Time: 14:34
 * To change this template use File | Settings | File Templates.
 */
public class LegacyGuildException extends Exception {
    public LegacyGuildException(String detailMessage) {
        super(detailMessage);
    }
}
