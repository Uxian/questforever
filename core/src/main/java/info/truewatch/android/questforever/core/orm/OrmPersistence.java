/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.orm;

import info.truewatch.android.questforever.core.Config;

import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class OrmPersistence {

    private static Orm theOrm = null;
    private static final String FAILED_TO_PERSIST_OBJECT = "Failed to persist object";

    private static synchronized Orm getOrm(Context context) {
		if (theOrm == null) {
			theOrm = new Orm(context);
		}
		return theOrm;
	}

	public static <T> List<T> getObjects(Context ctx, Class<T> clazz, Object... conditions) {
		Orm orm = getOrm(ctx);
		SQLiteDatabase db = orm.getReadableDatabase();
		List<T> retval = null;

		try {
			retval = orm.getObjects(clazz, db, false, conditions);
		} catch (Exception ex) {
			Config.LOG.error("Error retrieving data", ex);
		} finally {
			db.close();
		}

		return retval;
	}

	public static <T> T getObject(Context ctx, Class<T> clazz, Object... conditions) {
		Orm orm = getOrm(ctx);
		SQLiteDatabase db = orm.getReadableDatabase();
		T retval = null;

		try {
			List<T> data = orm.getObjects(clazz, db, true, conditions);
			if (data.size() >= 1) {
				retval = data.get(0);
			}
		} catch (Exception ex) {
			Config.LOG.error("Error retrieving data", ex);
		} finally {
			db.close();
		}

		return retval;
	}

	public static void store(Context context, Object object) {
		Orm orm = getOrm(context);
		SQLiteDatabase db = orm.getWritableDatabase();

		try {
			db.beginTransaction();
			orm.storeObject(object, db);
			db.setTransactionSuccessful();
		} catch (Exception ex) {
			Config.LOG.error(FAILED_TO_PERSIST_OBJECT, ex);
		} finally {
			db.endTransaction();
			db.close();
		}

	}

	public static void storeAll(Context context, Collection<?> objects) {
		Orm orm = getOrm(context);
		SQLiteDatabase db = orm.getWritableDatabase();

		try {
			db.beginTransaction();
			for (Object object: objects) {
				orm.storeObject(object, db);
			}
			db.setTransactionSuccessful();
		} catch (Exception ex) {
			Config.LOG.error(FAILED_TO_PERSIST_OBJECT, ex);
		} finally {
			db.endTransaction();
			db.close();
		}
	}

	public static void delete(Context context, Object obj) {
		Orm orm = getOrm(context);
		SQLiteDatabase db = orm.getWritableDatabase();

		try {
			db.beginTransaction();
			orm.deleteObject(obj, db);
			db.setTransactionSuccessful();
		} catch (Exception ex) {
			Config.LOG.error(FAILED_TO_PERSIST_OBJECT, ex);
		} finally {
			db.endTransaction();
			db.close();
		}
	}

	public static <T> void deleteObjects(Context context, Class<T> clazz, Object... conditions) {
		Orm orm = getOrm(context);
		SQLiteDatabase db = orm.getWritableDatabase();

		try {
			db.beginTransaction();
			orm.deleteObjects(clazz, db, conditions);
			db.setTransactionSuccessful();
		} catch (Exception ex) {
			Config.LOG.error(FAILED_TO_PERSIST_OBJECT, ex);
		} finally {
			db.endTransaction();
			db.close();
		}
	}
}
