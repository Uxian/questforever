/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data;

import android.content.Context;
import info.truewatch.android.questforever.core.exception.GameNotSupportedException;
import info.truewatch.android.questforever.core.persistence.Guild;
import info.truewatch.android.questforever.core.persistence.Toon;
import info.truewatch.android.questforever.core.data.eq2.*;

import static info.truewatch.android.questforever.core.info.GameConstants.EVERQUEST2;

public class ParserFactory {

	public static info.truewatch.android.questforever.core.data.CharacterSearcher getCharacterSearcher(Context context, int game, String searchString) throws GameNotSupportedException {
		switch (game) {
		case EVERQUEST2:
			return new Eq2CharacterSearcher(context, searchString);
		}
		throw new GameNotSupportedException(game);
	}

	public static GuildSearcher getGuildSearcher(Context context, int game, String searchString) throws GameNotSupportedException {
		switch (game) {
		case EVERQUEST2:
			return new Eq2GuildSearcher(context, searchString);
		}
        throw new GameNotSupportedException(game);
	}

	public static ItemSearcher getItemSearcher(Context context, int game, String searchString) throws GameNotSupportedException {
		switch (game) {
		case EVERQUEST2:
			return new Eq2ItemSearcher(context, searchString);
		}
        throw new GameNotSupportedException(game);
	}

	public static CharacterLoader getCharacterLoader(Context context, Toon theCharacter) throws GameNotSupportedException {
		info.truewatch.android.questforever.core.Config.LOG.debug("Game = " + theCharacter.getGame());
		int game = theCharacter.getGame();
		switch (game) {
		case EVERQUEST2:
			return new Eq2CharacterLoader(context, theCharacter);
		}
        throw new GameNotSupportedException(game);
	}

	public static GuildRosterParser getRosterParser(Context ctx, Guild theGuild) throws GameNotSupportedException {
		int game = theGuild.getGame();
		switch (game) {
		case EVERQUEST2:
			return new Eq2GuildCsvParser(ctx, theGuild.getId());
		}
        throw new GameNotSupportedException(game);
	}

    public static ServerSearcher getServerSearcher(Context ctx, int game, String searchString) throws GameNotSupportedException {
        switch (game) {
            case EVERQUEST2:
                return new Eq2ServerSearcher(ctx, searchString);
        }
        throw new GameNotSupportedException(game);
    }
}
