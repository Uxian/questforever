/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import android.content.Context;
import android.graphics.Bitmap;
import info.truewatch.android.questforever.core.BitmapLoader;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.ProgressNotification;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.persistence.Item;
import info.truewatch.android.questforever.core.data.EffectItem;
import info.truewatch.android.questforever.core.data.ValueField;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: krupagj
 * Date: 16/12/11
 * Time: 23:40
 * To change this template use File | Settings | File Templates.
 */
public class Eq2ItemLoader implements Runnable {

    private static Properties modOrdering = null;

    private String name;
    private Bitmap icon;
    private String description;
    private List<ValueField> modifiers = new ArrayList<ValueField>();
    private List<EffectItem> effects = new LinkedList<EffectItem>();
    private Set<String> slots = new HashSet<String>();
    private Set<String> flags = new HashSet<String>();
    private Map<String, Integer> usableClasses = new TreeMap<String, Integer>();
    private Set<String> adornmentSlots = new HashSet<String>();
    private Set<String> recipes = new HashSet<String>();

    private final Context context;
    private final Item item;
    private ProgressNotification notifier;

    public Eq2ItemLoader(final Context context, final Item item) {
        this.item = item;
        this.context = context;
    }

    @Override
    public void run() {
        try {
            loadItemDetails();
        } catch (final Exception e) {
            Config.LOG.error("Error parsing", e);
            notifyError(e);
        }
    }

    /* (non-Javadoc)
    * @see info.truewatch.android.questforever.core.xml.eq2.CharacterLoader#setNotifier(info.truewatch.android.questforever.core.ProgressNotification)
    */
    public void setNotifier(final info.truewatch.android.questforever.core.ProgressNotification notifier) {
        this.notifier = notifier;
    }

    private void notifyError(final Throwable problem) {
        if (notifier != null) {
            notifier.notifyError(problem, this);
        }
    }

    private void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText) {
        if (notifier != null) {
            notifier.notifyProgress(completed, currentProgress, maxProgress,
                    progressText, this);
        }
    }

    public void loadItemDetails() throws IOException, JSONException {
        try {
            final HttpSession session = new HttpSession(context);
            notifyProgress(false, 0, 3, "Connecting");
            String itemUrl = JSONUrlBuilder.itemUrl(item.getId(), 1);
            Config.LOG.debug(itemUrl);
            JSONArray matchingItems = session.loadJSonDocument(itemUrl).getJSONArray("item_list");

            notifyProgress(false, 1, 3, "Processing");

            JSONObject item = matchingItems.getJSONObject(0);

            name = item.getString("displayname");
            StringBuilder descBuilder = new StringBuilder();

            if (!item.isNull("flags")) {
                JSONObject flagsObject = item.getJSONObject("flags");
                JSONArray names = flagsObject.names();
                for (int n=0; n<names.length(); ++n) {
                    String name = names.getString(n);
                    if (flagsObject.getJSONObject(name).getInt("value") == 1) {
                        flags.add(StatTransformer.translateStat(name));
                    }
                }
            }

            JSONObject typeinfo = item.isNull("typeinfo") ? null : item.getJSONObject("typeinfo");
            if (!item.isNull("tier")) {
                descBuilder.append(item.getString("tier")).append(' ');
            }
            if (typeinfo != null && !typeinfo.isNull("wieldstyle")) {
                descBuilder.append(typeinfo.getString("wieldstyle")).append(' ');
            }
            descBuilder.append(item.getString("type")).append(' ');
            if (typeinfo != null && !typeinfo.isNull("knowledgedesc")) {
                String knowledge = typeinfo.getString("knowledgedesc");
                if (!"".equals(knowledge)) {
                    descBuilder.append(" (").append(knowledge).append(") ");
                }
            }
            if (typeinfo != null && !typeinfo.isNull("skill")) {
                descBuilder.append(" (").append(typeinfo.getString("skill")).append(")");
            }
            description = descBuilder.toString();

            if (!item.isNull("slot_list") && item.get("slot_list") instanceof JSONArray) {
                JSONArray jsonSlots = item.getJSONArray("slot_list");
                for (int n = 0; n < jsonSlots.length(); ++n) {
                    String slotName = jsonSlots.getJSONObject(n).getString("name");
                    slots.add(slotName);
                }
            }

            addModifierIfPresent(typeinfo, "damagerating");
            addModifierIfPresent(typeinfo, "damagetype");
            addModifierIfPresent(typeinfo, "maxdamage");
            addModifierIfPresent(typeinfo, "mindamage");
            addModifierIfPresent(typeinfo, "delay");
            addModifierIfPresent(typeinfo, "physicaldamageabsorption");
            addModifierIfPresent(item, "maxstacksize");

            if (!item.isNull("modifiers")) {
                Object modifiers = item.get("modifiers");
                if (modifiers instanceof JSONArray) {
                    handleModifiersArray(this.modifiers, (JSONArray) modifiers);
                } else if (modifiers instanceof JSONObject) {
                    handleModifiersObject(this.modifiers, (JSONObject) modifiers);
                }
            }

            if (!item.isNull("effect_list")) {
                JSONArray jsonEffects = item.getJSONArray("effect_list");
                for (int n = 0; n < jsonEffects.length(); ++n) {
                    String effect = jsonEffects.getJSONObject(n).getString("description");
                    int indentation = Integer.parseInt(jsonEffects.getJSONObject(n).getString("indentation"));
                    effects.add(new EffectItem(indentation, effect));
                }
            }

            if (typeinfo != null && !typeinfo.isNull("classes")) {
                Object classesAnon = typeinfo.get("classes");
                if (classesAnon instanceof JSONObject) {
                    JSONObject classes = (JSONObject)classesAnon;
                    JSONArray allClasses = classes.names();
                    for (int n = 0; n < allClasses.length(); ++n) {
                        String className = allClasses.getString(n);
                        JSONObject details = classes.getJSONObject(className);
                        usableClasses.put(className, Integer.parseInt(details.getString("level")));
                    }
                } else if (classesAnon instanceof JSONArray) {
                    JSONArray allClasses = (JSONArray) classesAnon;
                    for (int n = 0; n < allClasses.length(); ++n) {
                        JSONObject details = allClasses.getJSONObject(n);
                        usableClasses.put(details.getString("displayname"), Integer.parseInt(details.getString("level")));
                    }
                }

                removeDuplicateClasses();
            }

            if (!item.isNull(Eq2ItemLoaderHtml.ADORNMENTS_SLOTS)) {
                Object slots = item.get(Eq2ItemLoaderHtml.ADORNMENTS_SLOTS);
                if (slots instanceof JSONArray) {
                    JSONArray adornmentsslots = (JSONArray) slots;
                    for (int n = 0; n < adornmentsslots.length(); ++n) {
                        JSONObject adornmentslot = adornmentsslots.getJSONObject(n);
                        adornmentSlots.add(adornmentslot.getString("color"));
                    }
                }
            }

            if (typeinfo != null && !typeinfo.isNull("recipe_list")) {
                JSONArray jsonRecipes = typeinfo.getJSONArray("recipe_list");
                for (int n = 0; n < jsonRecipes.length(); ++n) {
                    recipes.add(jsonRecipes.getJSONObject(n).getString("name"));
                }
            }

            notifyProgress(true, 2, 3, "Loading Icon");

            String iconUrl = JSONUrlBuilder.itemImageUrl(item.getString("iconid"));
            icon = BitmapLoader.load(context, iconUrl);

            notifyProgress(true, 3, 3, "Finished");
        } catch (JSONException ex) {
            Config.LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    private void addModifierIfPresent(JSONObject baseObject, String attributeName) throws JSONException {
        if (baseObject != null && !baseObject.isNull(attributeName)) {
            addModifier(attributeName, baseObject.getString(attributeName), null, modifiers);
        }
    }

    private void removeDuplicateClasses() {
        if (usableClasses.size() == 25 && usableClasses.containsKey("paladin")) {
            int level = usableClasses.get("paladin");
            usableClasses.clear();
            usableClasses.put("All Adventure", level);
        } else if (usableClasses.size() == 12 && usableClasses.containsKey("tailor")) {
            int level = usableClasses.get("tailor");
            usableClasses.clear();
            usableClasses.put("All Tradeskill", level);
        }
    }

    private static void handleModifiersObject(Collection<ValueField> modMap, JSONObject modifiers) throws JSONException {
        JSONArray modnames = modifiers.names();
        if (modnames != null) {
            for (int n = 0; n < modnames.length(); ++n) {
                Object modifierObject = modifiers.get(modnames.getString(n));
                if (modifierObject instanceof JSONObject) {
                    JSONObject modifier = ((JSONObject) modifiers).getJSONObject(modnames.getString(n));
                    String modType = modifier.getString("type");
                    if ("modifyproperty".equals(modType)) {
                        String description;
                        if (modifier.has("desc")) {
                            description = modifier.getString("desc");
                        } else if (modifier.has("displayname")) {
                            description = modifier.getString("displayname");
                        } else {
                            description = "???";
                        }
                        addModifier(modnames.getString(n), modifier.getString("value"), description, modMap);
                    } else {
                        addModifier(modnames.getString(n), modifier.getString("value"), null, modMap);
                    }
                } else if (modifierObject instanceof JSONArray) {
                    JSONArray modSubArray = (JSONArray) modifierObject;
                    for (int m = 0; m < modSubArray.length(); ++m) {
                        JSONObject modItem = modSubArray.getJSONObject(m);
                        addModifier(modItem.getString("displayname"), modItem.getString("value"), null, modMap);
                    }
                }
            }
        }
    }

    private static void handleModifiersArray(Collection<ValueField> modMap, JSONArray modifiers) throws JSONException {
        if (modifiers.length() > 0) {
            for (int m = 0; m < modifiers.length(); ++m) {
                JSONObject modItem = modifiers.getJSONObject(m);
                addModifier(modItem.getString("displayname"), modItem.getString("value"), null, modMap);
            }
        }
    }
    
    private static void addModifier(String name, String value, String displayName, Collection<ValueField> collection) {
        if (modOrdering == null) {
            try {
                info.truewatch.android.questforever.core.Config.LOG.debug("Loading statistics mapper");
                modOrdering = new Properties();
                modOrdering.load(StatTransformer.class.getResourceAsStream("/modifiers.properties"));
            } catch (IOException ex) {
                info.truewatch.android.questforever.core.Config.LOG.error("Error reading modifiers map", ex);
            }
        }
        
        Config.LOG.debug("Dealing with stat " + name + " with value " + value + " and display " + displayName);
        
        String info = modOrdering.getProperty(name, "999,Misc");
        if (info != null) {
            String[] mods = info.split(",");
            if (mods.length == 2) {
                int sectionId = Integer.parseInt(mods[0]);
                String sectionName = mods[1];
                if (displayName == null) {
                    displayName = StatTransformer.translateStat(name);
                }
                collection.add(new ValueField(displayName, value, sectionId, sectionName));
            }
        }
    }

    public String getName() {
        return name;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public String getDescription() {
        return description;
    }

    public Collection<ValueField> getModifiers() {
        Collections.sort(modifiers);
        return modifiers;
    }

    public List<EffectItem> getEffects() {
        return effects;
    }

    public Set<String> getSlots() {
        return slots;
    }

    public Set<String> getFlags() {
        return flags;
    }

    public Map<String, Integer> getUsableClasses() {
        return usableClasses;
    }

    public Set<String> getAdornmentSlots() {
        return adornmentSlots;
    }

    public Set<String> getRecipes() {
        return recipes;
    }
}
