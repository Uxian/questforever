/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.persistence;

import info.truewatch.android.questforever.core.orm.annotation.Column;
import info.truewatch.android.questforever.core.orm.annotation.Entity;

@Entity(tableName="member", primaryKey={"game", "id"})
public class GuildMember implements Comparable<GuildMember> {

	private long id;
	private int game;
	private long guildId;
	private String name;
	private String rank;
	private String adventureClass;
	private int adventureLevel;
	private String artisanClass;
	private int artisanLevel;

	@Column(name="id", type="LONG", notNull=true, version=1)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@Column(name="game", type="INTEGER", notNull=true, version=1)
	public int getGame() {
		return game;
	}
	public void setGame(int game) {
		this.game = game;
	}

	@Column(name="guildId", type="LONG", notNull=true, version=1)
	public long getGuildId() {
		return guildId;
	}
	public void setGuildId(long guildId) {
		this.guildId = guildId;
	}

	@Column(name="name", notNull=true, version=1)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="adventureClass", notNull=true, version=1)
	public String getAdventureClass() {
		return adventureClass;
	}
	public void setAdventureClass(String adventureClass) {
		this.adventureClass = adventureClass;
	}

	@Column(name="adventureLevel", notNull=true, version=1, type="INTEGER")
	public int getAdventureLevel() {
		return adventureLevel;
	}
	public void setAdventureLevel(int adventureLevel) {
		this.adventureLevel = adventureLevel;
	}

	@Column(name="artisanClass", version=1)
	public String getArtisanClass() {
		return artisanClass;
	}
	public void setArtisanClass(String artisanClass) {
		this.artisanClass = artisanClass;
	}

	@Column(name="artisanLevel", version=1, type="INTEGER")
	public int getArtisanLevel() {
		return artisanLevel;
	}
	public void setArtisanLevel(int artisanLevel) {
		this.artisanLevel = artisanLevel;
	}

	@Column(name="rank", version=1)
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getDetails() {
		StringBuffer buf = new StringBuffer(rank);

		if (adventureClass != null) {
			buf.append(", ").append(adventureLevel).append(" ").append(adventureClass);
		}

		if (artisanClass != null) {
			buf.append(", ").append(artisanLevel).append(" ").append(artisanClass);
		}
		return buf.toString();
	}

	public Toon asToon(Guild owningGuild) {
		Toon toon = new Toon();
		toon.setId(id);
		toon.setGame(game);
		toon.setName(name);
		toon.setServer(owningGuild.getServer());
		return toon;
	}

	@Override
	public int compareTo(GuildMember another) {
		return name.compareTo(another.name);
	}

}
