/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data.eq2;

import android.content.Context;
import info.truewatch.android.questforever.core.Config;
import info.truewatch.android.questforever.core.http.HttpSession;
import info.truewatch.android.questforever.core.info.GameConstants;
import info.truewatch.android.questforever.core.persistence.Guild;
import info.truewatch.android.questforever.core.data.GuildSearcher;
import info.truewatch.android.questforever.core.data.SearchUtilities;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Eq2GuildSearcher implements Runnable, GuildSearcher {

    private info.truewatch.android.questforever.core.ProgressNotification notifier;
    private final String searchString;
    private List<Guild> results;

    private final Context context;

    public Eq2GuildSearcher(final Context context, final String searchString) {
        this.searchString = searchString;
        this.context = context;
    }

    @Override
    public void run() {
        try {
            results = findGuilds();
            notifyProgress(true, 3, 3, "Finished");
        } catch (final Exception e) {
            notifyError(e);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.GuildSearcher#setNotifier(info.truewatch.android.questforever.core.ProgressNotification)
      */
    @Override
    public void setNotifier(final info.truewatch.android.questforever.core.ProgressNotification notifier) {
        this.notifier = notifier;
    }

    private void notifyError(final Throwable problem) {
        if (notifier != null) {
            notifier.notifyError(problem, this);
        }
    }

    private void notifyProgress(final boolean completed, final int currentProgress, final int maxProgress, final String progressText) {
        if (notifier != null) {
            notifier.notifyProgress(completed, currentProgress, maxProgress,
                    progressText, this);
        }
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.GuildSearcher#findGuilds()
      */
    @Override
    public List<Guild> findGuilds() throws IOException, URISyntaxException, JSONException {

        Config.LOG.info("Starting search");

        final LinkedList<Guild> retval = new LinkedList<Guild>();
        String searchUrl = JSONUrlBuilder.guildUrl(null, 100,
                JSONUrlBuilder.PARAM_SHOW, "name,id,world",
                "name", "i/" + SearchUtilities.capitalise(searchString) + "/");

        final HttpSession session = new HttpSession(context);

        notifyProgress(false, 1, 2, "Reading data");
        JSONArray jsonObject = session.loadJSonDocument(searchUrl).getJSONArray("guild_list");

        // TODO - error responses
        try {
            for (int n = 0; n < jsonObject.length(); ++n) {
                JSONObject guild = jsonObject.getJSONObject(n);
                info.truewatch.android.questforever.core.Config.LOG.debug(guild.getString("name"));

                final long guildId = Long.parseLong(guild.getString("id"));
                final String name = guild.getString("name");
                final String serverName = guild.getString("world");
                final Guild entity = new Guild();
                entity.setId(guildId);
                entity.setName(name);
                entity.setServer(serverName);
                entity.setGame(GameConstants.EVERQUEST2);
                retval.add(entity);
            }
        } catch (JSONException e) {
            throw new IOException(e.getMessage());
        }

        Collections.sort(retval, new Comparator<Guild>() {
            @Override
            public int compare(Guild guild, Guild guild1) {
                int comparison = guild.getName().compareToIgnoreCase(guild1.getName());
                if (comparison == 0) {
                    comparison = guild.getServer().compareToIgnoreCase(guild1.getServer());
                }
                return comparison;
            }
        });

        return retval;
    }

    /* (non-Javadoc)
      * @see info.truewatch.android.questforever.core.xml.eq2.GuildSearcher#getResults()
      */
    @Override
    public List<Guild> getResults() {
        return results;
    }

}
