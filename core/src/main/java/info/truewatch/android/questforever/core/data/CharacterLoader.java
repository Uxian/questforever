/*
 * QuestForever Everquest and Everquest II (TM) data browser for Android
 * Smartphones.
 * Copyright (C) 2010-2011 Gerard Krupa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package info.truewatch.android.questforever.core.data;

import android.graphics.Bitmap;
import info.truewatch.android.questforever.core.persistence.Toon;

import java.util.Collection;
import java.util.List;

public interface CharacterLoader extends Runnable {

    int INTERMEDIATE_UPDATE = -1;
    int ATTRIBUTES_AVAILABLE = 1;
    int STATS_AVAILABLE = 2;
    int FACTIONS_AVAILABLE = 3;
    int BIO_AVAILABLE = 4;
    int PAPERDOLL_AVAILABLE = 5;
    int ALTS_AVAILABLE = 6;
    int EQUIPMENT_AVAILABLE = 7;
    int COMPLETED = 8;

    void setNotifier(info.truewatch.android.questforever.core.ProgressNotification notifier);

    int getEquipmentCount();

    Bitmap getEquipmentImage(int index);

    String getEquipmentName(int slot);

    long getEquipmentId(int slot);

    List<Toon> getAlts();

    Collection<ValueField> getAttributes();

    Collection<ValueField> getStats();

    Collection<ValueField> getFactions();

    String getBiography();

    Bitmap getPaperDoll();
    
    Bitmap getPortrait();
}
